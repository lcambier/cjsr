function A = adjacencyMatrix(edges)
% ADJACENCYMATRIX Builds the adjacency matrix
%
%   A = ADJACENCYMATRIX(EDGES) creates a matrix A such that A(i,j) is equal
%   to the number of [i j] rows in EDGES. 0 means that the (i,j) edge does
%   not exist in EDGES.

nNodes = max(edges(:)) ;
A = sparse(edges(:,1),edges(:,2),ones(size(edges,1),1),nNodes,nNodes) ;

end