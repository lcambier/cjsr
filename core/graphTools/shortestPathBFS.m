function [lengthShortestPath, shortestPaths, isReachableAnywhere] = shortestPathBFS(M,origins)
% SHORTESTPATHBFS Shortest Path
%
%   [LENGTHSHORTTESTPATH, SHORTTESTPATH, ISREACHABLEANYWHERE] = SHORTESTPATHBFS(M,origins)
%   With a BFS algorithm, find the shortest path from every node *to* (not from !) set of
%   nodes origins using adjacency matrix M. If M(i,j) is non-zero, it means
%   that there is an edge from node i to node j.
%   LENGTHSHORTTESTPATH is the length from each node to the closest node in
%   origins.
%   SHORTTESTPATH are the shortest paths themselves.
%   ISREACHABLEANYWHERE is true if each node can be connected to at least
%   one node in origins.
%
%   Originally written by Damien Scieur

% Mem allocation
nNodes = size(M,1);
lengthShortestPath = inf*ones(nNodes,1) ;
shortestPaths = cell(nNodes,1) ;
isReachableAnywhere = false ;
lastLabelised = zeros(nNodes,1);

% Initialisation : length from origin to origin is zero.
nVisited = 0;
nNodeLeft = nNodes;
origins = origins(:)';
for originNode = origins
    lengthShortestPath(originNode) = 0;
    nNodeLeft = nNodeLeft -1;
    nVisited = nVisited+1;
    lastLabelised(nVisited) = originNode;
    shortestPaths{originNode} = originNode;
end

while(true)
    NVisitedThisTime = 0;
    oldLastLabelised = lastLabelised(1:nVisited) ;
    for idxNode = 1:nVisited
        actualNode = oldLastLabelised(idxNode);
        % We check all neightboors of last labelised
        for i = 1:nNodes
            if(M(i,actualNode) ~= 0) % we work with transposed matrix!
                % We keep unlabelised neightboors
                if(lengthShortestPath(i) == inf)
                    lengthShortestPath(i) = lengthShortestPath(actualNode) + 1;
                    shortestPaths{i} = [i, shortestPaths{actualNode}];
                    NVisitedThisTime = NVisitedThisTime +1;
                    lastLabelised(NVisitedThisTime) = i;
                end
            end
        end
    end    
    nVisited = NVisitedThisTime;
    nNodeLeft = nNodeLeft - nVisited;
    
    if(nNodeLeft == 0)
        isReachableAnywhere = true;
        break ;
    end
    
    if(nVisited == 0)
        isReachableAnywhere = false;
        break ;
    end
end