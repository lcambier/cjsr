function connected = isStronglyConnected(edges)
% ISSTRONGLYCONNECTED Check Strong Connectivity
%
%   CONNECTED = ISSTRONGLYCONNECTED(EDGES) check wether the graph
%   discribed by the set of edges EDGES is strognly connected (CONNECTED is true)
%   or not.

if size(edges,2) ~= 2
    error('edges should be a [n x 2] matrix') ;
end

% Check in one direction, up to node 1
M = adjacencyMatrix(edges) ;
[~, ~, isReachableAnywhere] = shortestPathBFS(M,1) ;
if ~ isReachableAnywhere
    connected = false ;    
    return ;
end
% Check in reversed direction, i.e. from node 1
[~, ~, isReachableAnywhere] = shortestPathBFS(M',1) ;
if ~ isReachableAnywhere    
    connected = false ;
    return ;
end

connected = true ;
