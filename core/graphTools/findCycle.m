function cycle = findCycle(edges, maxLength)
% FINDCYCLE Find any cycle in the digraph expressed by edges.
%
%   CYCLE = FINDCYCLE(EDGES, MAXLENGTH) returns a cycle in EDGES. EDGES should be a [n
%   x 2] matrix of edges (i, j). CYCLE is a edge-indexed cycle, i.e.
%      edges(cycle(1),2) == edges(cycle(2),1)
%      edges(cycle(2),2) == edges(cycle(3),1)
%      ...
%      edges(cycle(end-1),2) == edges(cycle(end),1)
%      edges(cycle(end),2) == edges(cycle(1),1)
%   MAXLENGTH is the maximal length of the cycle. If no cycle of length at 
%   most maxLength are found, returns [].
%
%   This function returns *one* arbitrary cycle in the graph.

if size(edges, 2) ~= 2
    error('edges should be n x 2') ;
end

nEdges = size(edges, 1) ;

% Start from an edge, explore the graph until back home ...
paths = cell(nEdges, 1) ;
for e = 1:nEdges
    paths{e} = e ;
    if edges(e,1) == edges(e,2)
        cycle = e ;
        return ;
    end
end
% Augment path
for ll = 2:maxLength
    pathToAdd = cell(0) ;
    for p = 1:numel(paths)
        path = paths{p} ;
        edgesToAdd = find(edges(path(end),2) == edges(:,1)) ;
        for ee = 1:numel(edgesToAdd)
            pathToAdd{end+1,1} = [path edgesToAdd(ee)] ;
            if (edges(path(1),1) == edges(edgesToAdd(ee),2))
                cycle = pathToAdd{end} ;
                return ;
            end
        end                
    end
    for l = 1:numel(pathToAdd)
        paths{end+1,1} = pathToAdd{l,1} ;
    end
end

cycle = [] ;

end