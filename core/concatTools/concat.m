function [edges, oldLabels] = concat(edges1, edges2)
% CONCATFAST Concatenates sets of edges
%
%   [EDGES, OLDLABELS] = concat(EDGES1, EDGES2) concatenates the edges in
%   EDGES1 and EDGES2 such that there is an edge in EDGES if there is a path of length 2
%   with a first edge in EDGES1 and a second in EDGES2.
%   OLDLABELS is the concatenations of the labels in EDGES1 and EDGES2.

% Count # of edges in resulting graph
nEdges1 = size(edges1, 1) ;
nEdges2 = size(edges2, 1) ;
nEdges = 0 ;
for e1 = 1:nEdges1
    nEdges = nEdges + sum(edges2(:, 1) == edges1(e1, 2)) ;
end
% Prepare output matrix
edges = zeros(nEdges, 3) ;
oldLabels = zeros(nEdges, 2) ;
% Fill matrix
e = 1 ;
for e1 = 1:nEdges1
    for e2 = 1:nEdges2
        if edges1(e1, 2) == edges2(e2, 1) % Match on the inner dimension
            edges(e, :) = [edges1(e1, 1) edges2(e2, 2) 0] ;
            oldLabels(e, :) = [edges1(e1, 3) edges2(e2, 3)] ;
            e = e+1 ;
        end
    end
end

end