/** CONCATFAST Concatenates sets of edges
 *
 *   [EDGES, OLDLABELS] = concat(EDGES1, EDGES2) concatenates the edges in
 *   EDGES1 and EDGES2 such that there is an edge in EDGES if there is a path of length 2
 *   with a first edge in EDGES1 and a second in EDGES2.
 *   OLDLABELS is the concatenations of the labels in EDGES1 and EDGES2.
 *
 * Equivalent to 
 * ------------------------------------------------------------------------
 * function [edges, oldLabels] = concatSlow(edges1, edges2)
 * % Count # of edges in resulting graph
 * nEdges1 = size(edges1, 1) ;
 * nEdges2 = size(edges2, 1) ;
 * nEdges = 0 ;
 * for e1 = 1:nEdges1
 *    nEdges = nEdges + sum(edges2(:, 1) == edges1(e1, 2)) ;
 * end
 * % Prepare output matrix
 * edges = zeros(nEdges, 3) ;
 * oldLabels = zeros(nEdges, 2) ;
 * % Fill matrix
 * e = 1 ;
 * for e1 = 1:nEdges1
 *    for e2 = 1:nEdges2
 *         if edges1(e1, 2) == edges2(e2, 1) % Match on the inner dimension
 *             edges(e, :) = [edges1(e1, 1) edges2(e2, 2) 0] ;
 *             oldLabels(e, :) = [edges1(e1, 3) edges2(e2, 3)] ;
 *             e = e+1 ;
 *         end
 *     end
 * end
 * end
 * ------------------------------------------------------------------------ 
 *
 * The execution time is about 20-30 times faster for this kind of problems
 * edges1 = randi(100, [100 3]) ;
 * edges2 = randi(100, [100 3]) ;    
 *
 * Compile with 
 * mex concatFast.c
 */

#include <math.h>
#include <matrix.h>
#include <mex.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{           
    mwSize nEdges1, nEdges2, nEdges ;
    mwIndex e1, e2, e ;
    double* edges1 ;
    double* edges2 ;
    double* edges ;
    double* oldLabels ;
    
    
    /* Some checks */
    if (nlhs != 2 || nrhs != 2)
        mexErrMsgTxt("Needs 2 inputs and 2 outputs") ;    
    if (!mxIsDouble(prhs[0]) || !mxIsDouble(prhs[1]))
        mexErrMsgTxt("Inputs need to be double type") ;    
    if (mxGetN(prhs[0]) != 3 || mxGetN(prhs[1]) != 3)
        mexErrMsgTxt("Inputs need to be nEdges x 3 matrices") ;
    if (mxGetNumberOfDimensions(prhs[0]) != 2 || mxGetNumberOfDimensions(prhs[1]) != 2)
        mexErrMsgTxt("Inputs need to be 2-D matrices") ;
    
    /* Get Data */
    nEdges1 = mxGetM(prhs[0]) ;
    nEdges2 = mxGetM(prhs[1]) ;
    edges1 = mxGetPr(prhs[0]) ;
    edges2 = mxGetPr(prhs[1]) ;
    
    /**
     * nEdges1 = size(edges1, 1) ;
     * nEdges2 = size(edges2, 1) ;
     * nEdges = 0 ;
     * for e1 = 1:nEdges1
     *    nEdges = nEdges + sum(edges2(:, 1) == edges1(e1, 2)) ;
     * end
     **/
    
    /* Count number of edges */
    nEdges = 0 ;
    for(e1 = 0 ; e1 < nEdges1 ; e1++) {
        for(e2 = 0 ; e2 < nEdges2 ; e2++) {
            if (edges1[e1 + (2-1)*nEdges1] == edges2[e2 + (1-1)*nEdges2]) {
                nEdges++ ;             
            }
        }                    
    }    
    
    /* Allocate outputs */
    plhs[0] = mxCreateDoubleMatrix(nEdges, 3, mxREAL) ;
    plhs[1] = mxCreateDoubleMatrix(nEdges, 2, mxREAL) ;
    edges = mxGetPr(plhs[0]) ;
    oldLabels = mxGetPr(plhs[1]) ;
    
    /**
     * % Fill matrix
     * e = 1 ;
     * for e1 = 1:nEdges1
     *    for e2 = 1:nEdges2
     *         if edges1(e1, 2) == edges2(e2, 1) % Match on the inner dimension
     *             edges(e, :) = [edges1(e1, 1) edges2(e2, 2) 0] ;
     *             oldLabels(e, :) = [edges1(e1, 3) edges2(e2, 3)] ;
     *             e = e+1 ;
     *         end
     *     end
     * end
     */
    
    /* Fill outputs */
    e = 0 ;         
    for(e1 = 0 ; e1 < nEdges1 ; e1++) {
        for(e2 = 0 ; e2 < nEdges2 ; e2++) {
            if (edges1[e1 + (2-1)*nEdges1] == edges2[e2 + (1-1)*nEdges2]) {
                edges[e + (1-1)*nEdges] = edges1[e1 + (1-1)*nEdges1] ;
                edges[e + (2-1)*nEdges] = edges2[e2 + (2-1)*nEdges2] ;
                edges[e + (3-1)*nEdges] = 0 ;
                oldLabels[e + (1-1)*nEdges] = edges1[e1 + (3-1)*nEdges1] ;
                oldLabels[e + (2-1)*nEdges] = edges2[e2 + (3-1)*nEdges2] ;
                e++ ;
            }
        }                    
    }  
}

