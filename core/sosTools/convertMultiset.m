function [multisetConv, mu] = convertMultiset(multiset)
% CONVERTMULTISET From position-based to content-based indexing
%
%   [MULTISETCONV, MU] = convertMultiset(MULTISET) convert the position-based
%   multiset MULTISET to content-based index set MULTISETCONV. MU is the
%   product of the factorial of the elements in MULTISET.
%   Since an example is often better :
%      multiset = [0 1 0 2] ;
%      [multisetConv, mu] = convertMultiset(multiset) ;
%   gives
%      multisetConv = [2 4 4] % Because one element in second position and
%      two in fourth position
%      mu = (1! * 2!)
%
%   This is an internal function that should not be used to other purposes.
multisetConv = [] ;
mu = 1 ;
nnz = find(multiset ~= 0) ;
for i = nnz
    mu = mu * factorial(multiset(i)) ;
    multisetConv = [multisetConv ones(1, multiset(i))*i] ;
end
end

