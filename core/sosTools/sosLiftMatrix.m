function [Ad, monomials] = sosLiftMatrix(A, d)
% SOSLIFTMATRIX SOS Lift of a matrix A
% 
%   [AD, MONOMIALS] = SOSLIFTMATRIX(A, D) computes the SOS-Lift AD of the 
%   matrix A of degree D. Note that the sos lift is not uniquely defined 
%   since it depends on the indexing of the monomials. MONOMIALS are the
%   monomials degree : each row describes the power of each monomials
%   involved in the nth basis element of Ad. For instance, if row i is [0 2
%   1], it means it corresponds to x1^0 * x2^2 * x3^1.
%
%   The SOS-lift of a matrix A (in R^(n x n)), denoted A^[d] (in R^(N x N))
%   is built as follow :
%
%   A_{alpha, beta} = permanent(A(alpha, beta)) / sqrt( mu(alpha) * mu(beta) )
%
%   where :
%       - alpha, beta are multi-index in {1 ... N} that sum to d. For
%         instance, if N = 3 and d = 2, these are valid multi-indices :
%         (0, 2, 0), (1, 1, 0), (1, 0, 1), etc.
%       - N = (n-d+1, d), the number of combinaisons of d elements among 
%         n-d+1 elements.
%       - In the notation A_{alpha, beta} it is assumed that each
%         multi-index is mapped in the [1 ... N] interval. Assuming index
%         of alpha is a and beta is b, A_{alpha, beta} = A(a, b).
%       - permanent(A) is the permanent of A.
%       - mu(alpha) is the product of the factorials of the elements in the
%         multi-index.
%
%   See PERMANENT

% nchoosek(5,2) = binomial coef

if ~ (size(A,1) == size(A,2))
    error('A should be square') ;
end
if (d <= 0)
    error('d should be >= 1') ;
end

n = size(A,1) ;
N = nchoosek(n+d-1,d) ;

Ad = zeros(N,N) ; % Full here

% 1. Get all multiset of dimension d from 1 ... n
monomials = genPerms(n-1, d, 1) ; % 1 for the equality
% fprintf('n = %d (=?= %d), d = %d -> N = (n+d-1, d) = %d (=?= %d)\n', n, size(multisets, 2), d, N, size(multisets, 1)) ;

% 2. Convert all multisets once
multiset = cell(N, 1) ;
mus        = zeros(N, 1) ;
for i = 1:N
    [multiset{i}, mus(i)] = convertMultiset(monomials(i, :)) ;
end

% 3. For each pair of multiset, we build one entry of Ad
allPerms = perms(1:d) ;
for a = 1:N % The double-for loop would probably be much faster using a compiled mex file
    for b = 1:N
        alpha = multiset{a} ; muAlpha = mus(a) ;
        beta  = multiset{b} ; muBeta  = mus(b) ;
        Ad(a, b) = permanent(A(alpha, beta), allPerms) / sqrt(muAlpha * muBeta) ;
    end
end

end