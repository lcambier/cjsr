/** PERMANENT Permanent of a matrix
 *
 * P = permanent(A, allPerms) computes the permanent of a matrix A where the
 *   indexing is dictated by the allPerms arrays
 *   A should be a square matrix, while allperms should be a matrix giving the 
 *   exponent of each variables
 *
 * This code is equivalent to
 * ----------------------------------------------
 * function P = permanent(A, allPerms)
 *    P = 0 ;
 *    for p = 1:size(allPerms, 1)
 *        Ploc = 1 ;
 *        for i = 1:size(allPerms, 2)
 *            Ploc = Ploc * A(i, allPerms(p, i)) ;
 *        end
 *        P = P + Ploc ;
 *    end
 * end
 * ----------------------------------------------
 *
 * The speedup is approximately 2 compared  with the plain matlab implementation
 *
 * Compile with 
 * mex permanent.c
 */

#include <math.h>
#include <matrix.h>
#include <mex.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    mwSize fact_n, n, fact_n_check ;
    mwIndex i, p ;
    double * perm ;
    double * A ;
    double * P ;
    double ploc ;
            
    /* Some checks */
    if (nlhs != 1 || nrhs != 2)
        mexErrMsgTxt("Needs 2 inputs and 1 output") ;
    
    if (!mxIsDouble(prhs[0]) || !mxIsDouble(prhs[1]))
        mexErrMsgTxt("Input 1 and 2 need to be double type") ;
            
    if (mxGetNumberOfDimensions(prhs[0]) != 2 || mxGetM(prhs[0]) != mxGetN(prhs[1]))
        mexErrMsgTxt("Input 1 needs to be a square matrix and needs to have the same number of rows as the number of column of the second argument") ;
    
    if ( ! (mxGetN(prhs[1]) == mxGetN(prhs[0])))
        mexErrMsgTxt("Input 2 should have the same number of column as Input 1 (# of rows not checked)") ;

    fact_n = mxGetM(prhs[1]) ;
    n = mxGetN(prhs[1]) ;    
        
    fact_n_check = 1 ;
    for(i = 1 ; i <= n ; i++)
        fact_n_check *= i ;
    if (fact_n_check != fact_n)
        mexErrMsgTxt("Input 2 should have fact(n) rows") ;
    
    /* Prepare output */
    plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL) ;
    perm = mxGetPr(plhs[0]) ;
    *perm = 0 ;
        
    /* Compute permanent */
    A = mxGetPr(prhs[0]) ;
    P = mxGetPr(prhs[1]) ;

    for(p = 0 ; p < mxGetM(prhs[1]) ; p++) {
        ploc = 1 ;
        for(i = 0 ; i < mxGetN(prhs[1]) ; i++) {        
            ploc *= A[i + n * ((mwIndex)(P[p + i * fact_n]-1))] ; /* -1 because Matlab <-> indexing */
        }                
        *perm += ploc ;
    }            
}