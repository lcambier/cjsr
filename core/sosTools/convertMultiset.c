/** CONVERTMULTISET From position-based to content-based indexing
 *
 * function [multisetConv, mu] = convertMultiset(multiset) : since an example is
 *   often better ...
 *      multiset = [0 1 0 2] ;
 *      [multisetConv, mu] = convertMultiset(multiset) ;
 *      multisetConv = [2 4 4]
 *      mu = (1! * 2!)
 *
 * Equivalent to
 * -------------------------------------------------
 *   function [multisetConv, mu] = convert(multiset)
 *       multisetConv = [] ;
 *       mu = 1 ;
 *       nnz = find(multiset ~= 0) ;
 *       for i = nnz
 *             time = tic ;        
 *             mu = mu * factorial(multiset(i)) ; % Product of the factorial of the multiplicity
 *             fprintf('Factorial of %d. %f sec.\n', multiset(i), toc(time)) ;
 *             for j = 1:multiset(i)
 *                multisetConv = [multisetConv i] ; 
 *             end
 *           mu = mu * factorial(multiset(i)) ;
 *           multisetConv = [multisetConv ones(1, multiset(i))*i] ;
 *       end
 *   end
 * -------------------------------------------------
 *
 * The speedup is approximately 10 compared to the full matlab implementation.
 *
 * Compile with 
 * mex convertMultiset.c
 */

#include <math.h>
#include <matrix.h>
#include <mex.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    
    double * multiset ;
    mwSize numel, size ;
    mwIndex i, j, k ;
    double * data ;
    double * factorial ;
    
    /* Some checks */
    if (nlhs != 2 || nrhs != 1)
        mexErrMsgTxt("Needs 1 inputs and 2 outputs") ;
    
    if (! mxIsDouble(prhs[0]))
        mexErrMsgTxt("Inputs needs to be double type") ;
    
    multiset = mxGetPr(prhs[0]) ;
    numel = mxGetNumberOfElements(prhs[0]) ;
    
    /* Allocate outputs */
    /* First, get the size */
    size = 0 ;
    for(i = 0 ; i < numel ; i++)
        size += (mwSize) multiset[i] ;
    
    /* Create output */
    plhs[0] = mxCreateDoubleMatrix(1, size, mxREAL) ;
    plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL) ;
    data = mxGetPr(plhs[0]) ;
    factorial = mxGetPr(plhs[1]) ;
   
    /* Fill output and compute factorial */
    k = 0 ;
    *factorial = 1 ;
    for(i = 0 ; i < numel ; i++) {
        for(j = 0 ; j < (mwSize) multiset[i] ; j++) { /* New multiset */
            data[k++] = i+1 ;
        }
        for(j = 1 ; j <= (mwSize) multiset[i] ; j++) { /* Factorial */
            (*factorial) *= j ;
        }
    }
}