function P = permanent(A, allPerms)
% PERMANENT Permanent of a matrix
%
%   P = PERMANENT(A, ALLPERMS) computes the permanent of a matrix A where the
%   indexing is dictated by the ALLPERMS array
%   A should be a square matrix, while ALLPERMS should be a matrix giving the 
%   exponent of each variables
%
%   This is an internal function that should not be used to other purposes.
P = 0 ;
for p = 1:size(allPerms, 1)
    Ploc = 1 ;
    for i = 1:size(allPerms, 2)
        Ploc = Ploc * A(i, allPerms(p, i)) ;
    end
    P = P + Ploc ;
end
end