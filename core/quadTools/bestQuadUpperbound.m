function gamma = bestQuadUpperbound(S, Q)
% BESTQUADUPPERBOUND Computes the best CJSR upper bound a given quadratic multinorm has to offer.
%
%   GAMMA = BESTQUADUPPERBOUND(S, Q) finds the best upper-bound on the CJSR
%   for a given set of matrices describing the quadratic multinorm. S is a
%   CSSystem and Q a cell array of square symmetric real matrices. GAMMA is
%   the upper-bound on the CJSR.
%
%   It thus find the smallest gamma such that
%             gamma^(2*|s|) Qi>= As' Qj As for all (i, j, s) in Edges.
%   This can be done simply by
%       gamma = max_{(i,j,s) in Edges} lev(Qi^(-1)*As'*Qj*As)^(1/2/|s|)
%   where lev(X) returns the leading eigenvalue of X (i.e. the largest one,
%   where X only has real positive eigenvalues).
%   Note that Qi^(-1/2)*As'*Qj*As*Qi^(-1/2) is psd and thus only
%   has real positive eigenvalues. So is Qi^(-1)*As'*Qj*As since it is equal to 
%   Qi^(-1/2)*As'*Qj*As*Qi^(-1/2) by similarity.
%
%   See also CJSRQUADMULTINORM

gamma = - inf ;
for e = 1:S.getNEdges()
    edge = S.getEdges(e) ; % edge = (i, j)
    Qi = real(Q{edge(1)}) ; % To make sure there are no numerical errors
    Qj = real(Q{edge(2)}) ;
    As = S.getMatProdEdge(e) ; 
    gammaNew = eigs(Qi\(As'*Qj*As),1)^(1/2/S.getNLabels(e)) ;             
    gamma = max(gamma, gammaNew) ;
end

end

