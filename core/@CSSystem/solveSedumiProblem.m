function [Qprimal, Qdual, feas] = solveSedumiProblem(Sys, opti, gamma, opts)
% SOLVESEDUMIPROBLEM Solve the Sedumi/Matrix formulation
%
%   [Qprimal, Qdual, feas] = S.SOLVESEDUMIPROBLEM(opti, gamma, opts) solve the problem
%   opti on system S with a given gamme and opts.
%   opti should come from the output of getSedumiProblem
%   gamme is the candidate CJSR
%   opts is an option structure
%
%   This function returns Qprimal, Qdual and feas = true if the primal is feasible
%   with gamma, or feas = false otherwise.
%   
%   This is an internal private function that cannot be used outside of
%   this class (private).
     
    % Acntr = opti.A + (opti.gammaCoef(1/gamma) * opti.Agamma - opti.Agamma) ;
    Acntr = opti.A + opti.Agamma * opti.gammaCoef(1/gamma) - opti.Agamma ;
    % Acntr = opti.A + ((1/gamma)^(2*uniformLabel)-1) * opti.Agamma ;
    % [~, Y, info] = sedumi(Acntr, opti.b, opti.c, opti.K, params) ;
    if strcmp(opts.quad.solver,'sedumi')
        solOpts = opts.sedumi ;
    elseif strcmp(opts.quad.solver,'sdpt3')
        solOpts = opts.sdpt3 ;
    end
    options.solver = opts.quad.solver ;
    options.solverOptions = solOpts ;
    reltol = opts.quad.reltol ;
    
    % We can ignore warnings. If warning, the function returns infeasible
    % We will improve gamma in the quad function
    warning('off','all') ;
    [X,Y,feas,~,~] = solveSemiDefiniteProgram(Acntr,opti.b,opti.c,opti.K,reltol,options) ;
    warning('on','all') ;

    Qdual = cell(1,Sys.getNEdges()) ;
    sumTr = 0 ;
    NEntries = Sys.getMatSize() * Sys.getMatSize();
    for e = 1:Sys.getNEdges()
        Qdual{e} = reshape(X((NEntries*(e-1)+1):(NEntries*e)),Sys.getMatSize(),Sys.getMatSize()) ;
        sumTr = sumTr + trace(Qdual{e}) ;
    end
    for e = 1:Sys.getNEdges()
        Qdual{e} = Qdual{e} / sumTr ;
    end
    
    Qprimal = convertSolutionToCell(Y, Sys.getNNodes(), Sys.getMatSize()) ;
    
    function Pcell = convertSolutionToCell(DualVec,n_nodes,sizeMatrix)
    % Copied - as it - from the JSR toolbox
        mapIdx = zeros(sizeMatrix);
        counter = 0;
        % We create the same map than get_standard_optimisation_form
        for i=1:sizeMatrix;
            counter = counter + 1;
            mapIdx(i,i) = counter;
            for j=(i+1):sizeMatrix;
                counter = counter + 1;
                mapIdx(i,j) = counter;
                mapIdx(j,i) = counter;
            end
        end
        nElemMatrix = sizeMatrix*(sizeMatrix+1)/2;
        Pvec = DualVec(1:n_nodes*nElemMatrix);
        n_P = n_nodes;
        Pcell = cell(n_P,1);
        for i=1:n_P
            Pcell{i} = zeros(sizeMatrix);
            for j=1:sizeMatrix
                for k=1:sizeMatrix
                    Pcell{i}(j,k) = Pvec(nElemMatrix*(i-1)+mapIdx(j,k));
                end
            end
            Pcell{i} = 0.5*Pcell{i} + 0.5*Pcell{i}'; % Symetric part.
        end
    end


end
