function writeDot(this, file, edgeNumbering, nodeNumbering)
% WRITEDOT Build dot-format file
%
% S.WRITEDOT(FILE, EDGENUMBERING, NODENUMBERING) will create, in file
% FILE, the .dot representation of the graph in S.
% If EDGENUMBERING is true, it will add the edge number at each edge.
% If NODENUMBERING is true, it will add the node number at each node.
%
% To use this function, the "dot" software needs to be installed. One way
% to do it is to install graphviz (http://www.graphviz.org).

fid = fopen(file,'w') ;
fprintf(fid, 'digraph G {\n') ;
fprintf(fid, 'overlap=scale;\nspline=true;\nsize="200,150";\nrankdir=LR;\nK=1;\n') ;
for n = 1:this.getNNodes()
    if nodeNumbering
        fprintf(fid, sprintf('%d [height=0.3, width=0.3];\n', n)) ;
    else
        fprintf(fid, sprintf('%d [height=0.3, width=0.3, label = ""];\n', n)) ;
    end
end
for e = 1:this.getNEdges()       
    label = num2str(this.labels{e}) ;
    if edgeNumbering
      edgeNum = sprintf('%d:',e);
    else
      edgeNum = '';
    end
    fprintf(fid, sprintf('%d -> %d [label="%s%s",fontsize=10];\n', this.edges(e, 1), ...
        this.edges(e, 2), ...
        edgeNum, ...
        label)) ;
end
fprintf(fid, '}') ;
fclose(fid) ;

end
