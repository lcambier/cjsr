function Sout = TProductLift(Sin, T)
% TPRODUCTLIFT T-product lift
%
%   SOUT = SIN.TPRODUCTLIFT(T) computes the lifted system SOUT such that
%   (i, j, s) is a edge in SOUT if and only if there exist a path p of length T
%   in SIN, beginning at node i and ending at node j. The label s is the
%   concatenation of the labels of all the edges on path p
%   The matrices are unchanged (only the labels are adapted).
%   T should be greater or equal to 1. If T == 1, SOUT = SIN.
%
%   References
%   [1] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015). 
%       Stability of discrete-time switching systems with constrained switching 
%       sequences. arXiv preprint arXiv:1503.06984.
%
%   See also MDEPENDANTLIFT, ARBITRARYLIFT, SOSLIFT

if ~isscalar(T) || T <= 0 || mod(T, 1) ~= 0
    error(sprintf('T (%d) should be a scalar integer >= 0', T)) ;
end

Sout = Sin ;
for t = 2:T
    Sout = CSSystem.concat(Sout, Sin) ;
end
Sout.degree = Sin.degree;
end
