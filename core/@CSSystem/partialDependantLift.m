function Sout = partialDependantLift(Sys, nodesToSplit, direction)
% PARTIALDEPENDANTLIFT Partial path or horizon-dependant lift
%
%   SOUT = S.PARTIALDEPENDANTLIFT(NODES) or
%   SOUT = S.PARTIALDEPENDANTLIFT(NODES, 'past') splits the nodes in
%   NODES according to their entering edges, and connect the graph
%   such that the language is preserved.
%   If NODES = [1 ... S.getNNodes], this is equivalent to MDependantLift(1,
%   'path').
%   SOUT = S.PARTIALDEPENDANTLIFT(NODES, 'future') does the same
%   but according to the leaving edges of each nodes in nodesToSplit.
%   If NODES = [1 ... S.getNNodes], this is equivalent to MDependantLift(1,
%   'horizon').
%
%   Experimental, research oriented code.
%
%   See also TPRODUCTLIFT, MDEPENDANTLIFT, SOSLIFT, ARBITRARYLIFT,
%   PARTIALPRODUCTLIFT

% Input checks
nodesToSplit = nodesToSplit(:) ;
if nargin == 2
    direction = 'past' ;
end
if ~ (strcmp(direction, 'past') || strcmp(direction, 'future'))
    error('Direction should be either past or future') 
end
if any(unique(nodesToSplit) ~= nodesToSplit)
    error('nodesToSplit should contain at most one occurence of each node') ;
end
if max(nodesToSplit) > Sys.getNNodes() || min(nodesToSplit) < 1
    error('One of the node in nodesToSplit is greater than the number of nodes in Sys or less than 1') ;
end

past = strcmp(direction, 'past') ;

edges = Sys.edges ;
labels = Sys.labels ;
matrices = Sys.matrices ;
nNodes = Sys.getNNodes() ;
nEdges = Sys.getNEdges() ;

% Build new clusters
cluster = cell(nNodes, 1);
nodeCount = 0;
for v = 1:nNodes
    if sum(v == nodesToSplit) >= 1 
        if past
            indexes = find(edges(:, 2) == v) ;
        else
            indexes = find(edges(:, 1) == v) ;
        end
        cluster{v} = nNodes + indexes ; 
        nodeCount = nodeCount + length(indexes);
    else
        cluster{v} = v ;
        nodeCount = nodeCount + 1;
    end
end
% Build new edges
newEdges = [] ;
newLabels = {} ;
id = 0 ;
for e = 1:nEdges
    edge = edges(e, :) ;
    label = labels{e, 1} ;
    if past
        if sum(edge(2) == nodesToSplit) >= 1 
            newDest = nNodes + e ;
        else
            newDest = edge(2) ;
        end 
        clusterLength = length(cluster{edge(1)}) ;   
        newEdges(id+1:id+clusterLength,:) = [cluster{edge(1)} newDest*ones(clusterLength,1)] ;
    else
        if sum(edge(1) == nodesToSplit) >= 1 
            newOrig = nNodes + e ;
        else
            newOrig = edge(1) ;
        end 
        clusterLength = length(cluster{edge(2)}) ;           
        newEdges(id+1:id+clusterLength,:) = [newOrig*ones(clusterLength,1) cluster{edge(2)} ] ;
    end        
    for l = id+1:id+clusterLength
        newLabels{l,1} = label ;
    end
    id = id+clusterLength ;
end
% Remap indexes
allNodes = unique(newEdges(:)) ;
newEdges2 = zeros(size(newEdges)) ;
for k = 1:numel(allNodes)
    newEdges2(newEdges == allNodes(k)) = k ;
end
% Output system
Sout = CSSystem(newEdges2, matrices, newLabels) ;


