function [gamma, Qprimal, Qdual, info] = cjsrQuadMultinorm(Sys, opts)
% CJSRQUADMULTINORM Bissection-LMI's algorithm.
%
%   [gamma, Qprimal, Qdual, info] = Sys.CJSRQUADMULTINORM()
%   [gamma, Qprimal, Qdual, info] = Sys.CJSRQUADMULTINORM(opts) approximates the extremal
%   multinorm with a quadratic multinorm, leading to an upperbound on the
%   CJSR.
%
%   In practice, it finds gamma such that
%
%   min gamma
%       - As ' Qprimalj As + gamma^(2*|s|) Qprimali >= 0 (SDP)    for all (i, j, s) in Edges
%                                          Qprimali >= I (SDP)    for all (i) in Nodes
%
%   where the Edges and Nodes are contained in the CSSystem sys.
%
%   The structure opts (optionnal) can contain several fields.
%     -  opts.verbose (1) the verbose level. 0 means silent, 1 means some
%        informations printed.
%     -  opts.quad.bounds ([]), a vector containing the initial lower and
%        upper bounds for the bissection. If it is empty, the following
%        methods are used to create initial upper and lower bounds :
%        Lower bound: we use the maximum spectral radius of all self loops,
%           or 0 if there are none.
%        Upper bound: we use the maximum 2-norm of all matrices
%        If it only contains one element, this value is used as the
%        upperbound.
%     -  opts.quad.tol (1e-5), the tolerance for the bissection.
%     -  opts.quad.modeling ('sedumi'), a string describing the modeling type, either
%        yalmip ('yalmip') or full matrices ('sedumi') in Sedumi format.
%        If you use the 'yalmip' modeling, the options to yalmip need to be
%        passed using opts.quad.yalmip.
%        If you use the 'sedumi' modeling, the solver can be choosen using
%        opts.quad.solver, and the corresponding options to the solver can be
%        passed using either opts.sedumi (for sedumi) or opts.sdpt3 (for
%        sdpt3)
%     -  opts.quad.solver ('sedumi') the solver used if the modeling is set
%        on 'sedumi'. The solver can either be 'sedumi' or 'sdpt3'. The
%        corresponding solver field in opts (opts.sedumi or opts.sdpt3) can
%        be used to pass parameters to the solver.
%     -  opts.quad.yalmip ([]). If opts.modeling ==
%        'yalmip', it is used in the Yalmip modeling and solver. 
%     -  opts.quad.reltol (1e-7) : the relative tolerance of the SDP solver when
%        opts.quad.modeling is set on 'sedumi'
%
%   The info structure contains informations about the algorithm.
%     - time : a *vector* containing the timing of each iteration. time(1)
%       contains the time spent in the initializations and in the building of
%       the algorithm
%     - ub : a *vector* containing the estimates of the upperbound
%       corresponding to the timing in time
%     - timeChecks : the time spent in all initial checks
%     - timeBounds : the time spent in computing the lower and upper bounds
%     - timeBuild : the time spent to build the problems
%     - timeSolve : the time used to solve the problems
%
%   Experiments show that :
%   - Using Yalmip, the time spent in building the problem is *much higher*
%     (2 to 3 times on problem buit using Sys = randomSystem(10, 5, 5, 5))
%   - and the time spent in the resoltion is also quite higher with the Yalmip
%     formulation (from 20 to 50% higher) with a tolerance of 1e-5.
%
%   Reference:
%   [1] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015). 
%       Stability of discrete-time switching systems with constrained switching 
%       sequences. arXiv preprint arXiv:1503.06984.
%
%   See also CJSRCPLXPOLYTOPEMULTINORM.

initChecks = tic ;

if nargin == 1
    opts = cjsrSettings() ;
end

bounds = opts.quad.bounds ;
tol = opts.quad.tol ;

% Check tol
if tol <= 0 || ~isscalar(tol)
    error('tol should be a > 0 scalar') ;
end

timeChecks = toc(initChecks) ;
initBounds = tic ;

% Check bounds
if isempty(bounds) || numel(bounds) == 1
    down = 0 ;
    % Lowerbound     
    % Let's check all the self loops to get an LB
    loops = find(Sys.edges(:, 1) == Sys.edges(:, 2)) ;
    for i = 1:numel(loops)
        e = loops(i) ;
        spectralRadius = abs(eigs(Sys.getMatProdEdge(e), 1)) ;
        down = max(down, spectralRadius^(1/Sys.getNLabels(e))) ;
    end    
    if isempty(bounds)
        up = 0 ;
        % Upperbound
        % An upperbound is the norm of all matrices
        % rho(S) <= ||A_sigma||^(1/matLength)   
        for e = 1:Sys.getNEdges()
            up = max(up, norm(Sys.getMatProdEdge(e))^(1/Sys.getNLabels(e))) ;
        end
    else % Using the value in bounds
        up = bounds(1) ;
    end
end
if numel(bounds) > 1
    if numel(bounds) ~= 2 || bounds(1) >= bounds(2)
        error('Bounds should be a vector with two elements in increasing order') ;
    end
    down = bounds(1) ;
    up = bounds(2) ;   
end

timeBounds = toc(initBounds) ;
initModeling = tic ;

% Modeling
if strcmp(opts.quad.modeling, 'yalmip')
    [primal, dual] = Sys.getOptimizer(opts) ;
elseif strcmp(opts.quad.modeling, 'sedumi')
    opti = Sys.getSedumiProblem(opts) ;
else
    error('Wrong modeling parameter in opts structure') ;
end

timeModeling = toc(initModeling) ;
initSolve = tic ;

iter = 0 ;

info = struct() ;
info.time(1) = toc(initChecks) ;
info.ub(1) = up ;

while up - down > tol        
    
    initIter = tic ;
    iter = iter+1 ;
    newGamma = (up + down)/2 ;
    if opts.verbose > 0
        fprintf('%2d - Trying gamma %f <= %f <= %f\n', iter, down, newGamma, up) ;
    end
    
    % Optimizer
    if strcmp(opts.quad.modeling, 'yalmip')
        [Qprimalcur, feasible] = Sys.solveOptimizer(primal, newGamma, opts) ; %ndm: No dual in yalmip?
    elseif strcmp(opts.quad.modeling, 'sedumi')
        [Qprimalcur, Qdualcur, feasible] = Sys.solveSedumiProblem(opti, newGamma, opts) ;
    else
        error('Wrong modeling parameter in opts structure') ;
    end    
    if opts.verbose > 0
        fprintf('     Feasible ? %d\n', feasible) ;
    end
    
    if feasible
        up = newGamma ;
        % betterGamma = bestQuadUpperbound(Sys, Q) ;        
        Qprimal = Qprimalcur ;
    else
        down = newGamma ;
        if exist('Qdualcur','var')
            Qdual = Qdualcur ;
        end
    end
    
    % Update timings
    info.time(iter+1) = info.time(iter) + toc(initIter) ;
    info.ub(iter+1) = up ;
end


if ~ exist('Qdual','var')
    if strcmp(opts.quad.modeling, 'yalmip')
        [Qdual, feasible] = Sys.solveOptimizer(dual, down, opts) ;
    elseif strcmp(opts.quad.modeling, 'sedumi')
        [~, Qdual, feasible] = Sys.solveSedumiProblem(opti, down, opts) ;
    else
        error('Wrong modeling parameter in opts structure') ;
    end
    if ~ feasible
        % The only way to be infeasible even at the end is that the lb was
        % wrong
        warning('The lowerbound provided seems to be infeasible. If you did not provide any value using the opts field, please report this bug.') ;
    end
end
if ~ iscell(Qdual)
    Qdual = {Qdual} ;
end

if ~ exist('Qprimal','var')
    if strcmp(opts.quad.modeling, 'yalmip')
        [Qprimal, feasible] = Sys.solveOptimizer(primal, up, opts) ;
    elseif strcmp(opts.quad.modeling, 'sedumi')
        [Qprimal, ~, feasible] = Sys.solveSedumiProblem(primal, up, opts) ;
    else
        error('Wrong modeling parameter in opts structure') ;
    end  
    if ~ feasible
        % The only way to be infeasible even at the end is that the ub was
        % wrong
        warning('The upperbound provided seems to be infeasible. If you did not provided any value using the opts field, please report this bug.') ;        
    end
end
if ~ iscell(Qprimal)
    Qprimal = {Qprimal} ;
end

gamma = up ;

timeSolve = toc(initSolve) ;

info.timeChecks = timeChecks ;
info.timeBounds = timeBounds ;
info.timeModeling = timeModeling ;
info.timeSolve = timeSolve ;

end
