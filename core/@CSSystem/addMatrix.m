function [Sout, label] = addMatrix(Sin, M)
% ADDMATRIX Adds a matrix to the CSSystem
%
%   [Sout, label] = S.ADDMATRIX(M) adds a matrix M, and the corresponding label, to
%   the system S.
%   Sout is the new system, and 'label' is such that
%   Sout.getMatProdLabel(label) == M;
%   By default, label = S.getNLabels + 1;
%
%   See also ADDEDGE

if (any(size(M) ~= [Sin.getMatSize() Sin.getMatSize()]))
    error('Size of M is not valid.') ;
end

Sout = Sin ;

label = numel(Sout.matrices)+1;

Sout.matrices{label} = M ;

end