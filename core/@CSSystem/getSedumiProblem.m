function opti = getSedumiProblem(Sys, opts)
% GETSEDUMIPROBLEM Return CJSR-Sedumi formulation
%
% Return the following Sedumi formulation
%   min_x c' x
%       (A + (1/gamma)^2 Agamma) x = b
%                        x in K
%
% Corresponding to this problem
%
% P(gamma) = min 0
%             Qi > 0                               for all i         in Nodes (SDP sense)
%            s.t. - Ms' Qj Ms + gamma^2 Qi >= 0    for all (i, j, k) in Edges (SDP sense)  
%
% opt is currently useless
% Using this formulation, we can then solve the problem using Sedumi.
% Relevant when using cjsrQuadMultinorn with option 'quad.modeling' =
% 'sedumi'.

if nargin == 1
    opts = cjsrSettings() ;
end

[graph, M, nLabels] = Sys.getGraphMonolabels() ;

% The output of generate_pathcomplete_sdp corresponds to
%             max_P 0
%       s.t.    gamma^2( Mk' Pj Mk ) - Pi <=   0 forall edges i,j,k (SDP sense)
%               Pi                        >    0 forall nodes i     (SP sense)
% leading to
%             min_x c'x
%       s.t.    [ A + (gamma^2-1)Agamma ] x = b
%               x in K

%[A,Agamma,b,c,K] = generate_pathcomplete_sdp(M,graph) ;
%opti.A = A ;
%opti.Agamma = Agamma ;
%opti.b = b ;
%opti.K = K ;
%opti.c = c ;

[A,Agamma,gammaCoef,b,c,K] = generatePathcompleteSdpNonHomogeneous(M,graph,nLabels) ;
opti.A = A ;
opti.Agamma = Agamma ;
opti.gammaCoef = gammaCoef ;
opti.b = b ;
opti.K = K ;
opti.c = c ;


end