function h = viewMultinorm(Sys, type, in1)
% VIEWMULTINORM Displays the quadratic, sos or polytope multinorm.
%
%   S.VIEWMULTINORM('quad', Q) displays the multinorm built on system S where
%   Q is a cell arrays of matrices describing the quadratic norm at each
%   node.
%   S.VIEWMULTINORM('sos', Q) displays the multinorm built on the sos-lifted
%   system S where Q contains the (lifted) quadratic forms
%   The Q should come from the output of the cjsrQuadMultinorm method and have
%   the appropriate dimensions.
%   S.VIEWMULTINORM('poly', P) displays the complex-polytope multinorm coming
%   from the output of cjsrCplxPolytopeMultinorm.
%   H = S.VIEWMULTINORM(...) also returns the handle H to the figure.
%
%   See also VIEW, CJSRQUADMULTINORM, CJSRCPLXPOLYTOPEMULTINORM.

quad = strcmp(type, 'quad') ;
sos  = strcmp(type, 'sos') ;
poly = strcmp(type, 'poly') ;

if nargin ~= 3
    error('Needs 3 input arguments') ;
end
if ~ ischar(type)
    error('Type should be a string') ;
end

if ~ (quad || sos || poly)
    error('Type should be quad, sos or poly')
end
if quad
    Q = in1 ;
    d = 1 ;
    if ~ iscell(Q) || numel(Q) ~= Sys.getNNodes()
        error('Q should be a cell array with as many entries as the number of nodes') ;
    end
elseif sos
    Q = in1 ;
    d = Sys.degree ;
    if ~ iscell(Q) || numel(Q) ~= Sys.getNNodes()
        error('Q should be a cell array with as many entries as the number of nodes') ;
    end
    if d <= 0 || ~ isscalar(d)
        error('degree needs to be a >= 1 scalar') ;
    end
elseif poly
    P = in1 ;
    d = 1 ;
    if ~ iscell(P) || numel(P) ~= Sys.getNNodes()
        error('P should be a cell array with as many entries as the number of nodes') ;
    end
else
    error('Wrong type') ;
end

h = figure ;
nNodes = Sys.getNNodes() ;
n = ceil(sqrt(nNodes)) ;
m = ceil(nNodes/n) ;
if quad || sos
    dim = size(Q{1},1) ;
else
    dim = size(P{1},1) ;
end

% Preprocess
if poly
    nn = 100 ;
elseif quad || sos
    nn = 250 ;
end
theta = linspace(0, 2*pi, nn) ;
r = ones(1, nn) ;
x = r.*cos(theta) ;
y = r.*sin(theta) ;

if quad || sos
    [~, monomials] = sosLiftMatrix(eye(2), d) ;
    N = size(monomials, 1) ;
    Xd = zeros(N, nn) ;
    for i = 1:N
        coef = factorial(d) / factorial(monomials(i, 1)) / factorial(monomials(i, 2)) ;
        Xd(i, :) = sqrt(coef) * x.^(monomials(i, 1)) .* y.^(monomials(i, 2)) ;
    end
elseif poly
    X = [x ; y] ;
end

opts = cjsrSettings() ;
runits = zeros(nNodes, nn) ;
ok = zeros(nNodes, 1) ;
for node = 1:nNodes
    if nchoosek(2+d-1,d) == dim
        ok(node) = 1;
        if quad || sos
            Qq = Q{node} ;
            rnorm = zeros(1, nn) ;
            for i = 1:nn
                rnorm(i) = sqrt(Xd(:,i)' * Qq * Xd(:,i))^(1/d) ;
            end
        elseif poly
            Pp = P{node} ;
            % rnorm = polyNorm(Pp, X) ;
            for i = 1:nn
                rnormi = polyNorm(Pp, X(:,i), opts) ;
                if isempty(rnormi) % Rank-deficient polytope
                    error('Cannot plot rank-deficient polytope') ;
                end
                rnorm(i) = rnormi ;
            end
        end
        runits(node,:) = r ./ rnorm ;
    end
end
maxr = max(max(runits));
if maxr > 0.5
  % In this case we can the sublevel sets of our norms will still be
  % seen if we use the unit ball.
  maxr = 1;
end
for node = 1:nNodes
    curf = subplot(m, n, node) ;
    if ok(node)
        euclid = polar(theta, ones(size(theta)), '--k') ;
        hold on ;
        p = polar(theta, runits(node,:) ./ maxr) ;

        tit = title(sprintf('Node %d', node)) ;

        % We remove the lines and texts added by polar.

        % remove all lines
        lines = findall(curf,'type','line') ;
        % except the lines of our two sublevel sets
        lines(lines == euclid) = [];
        lines(lines == p) = [] ;
        delete(lines) ;

        % remove all texts
        text = findall(curf,'type','text') ;
        % except the titles
        text(text == tit) = [];
        delete(text) ;

        axis equal ;
    else
        error('The matrices should be 2x2 or come from a 2x2 lifted matrix.') ;
    end
end
fprintf('The dashed curve is the Euclidean unit ball');
if r == 1
    fprintf('.\n');
else
    fprintf('multiplied by %e.\n', maxr) ;
end
end
