function h = view(this, opts)
% VIEW Displays system
%
%   Sys.VIEW() displays the system stored in system Sys.
%   Sys.VIEW(OPTS) uses the optional structure opts.
% 
%   OPTS can contain
%     opts.dot.edgeNumbering (false) : add the edge number when set to true
%     opts.dot.nodeNumbering (false) : add the node number when set to true
%     opts.dot.displayLevel (0) : displays, next to the plot
%       - Nothing if displayLevel = 0
%       - The "atomistic" matrices if displayLevel = 1, i.e. the matrices
%         corresponding to the mono-labels
%       - All the matrix-product corresponding to each edge. For instance, if
%         at edge e the label is [1 2 3], it will display, along with the graph,
%         the matrix A3*A2*A1 next to the label [1 2 3].
%     opts.dot.dotFile ('') : when set to a string, save the intermediate .dot to
%       this file. If set to '', the temporary dot file will be deleted at the end.
%
%   To use this function, the "dot" software needs to be installed. One way
%   to do it is to install graphviz (http://www.graphviz.org).
% 
%   See also EXPORT

if nargin == 1
    opts = cjsrSettings() ;
end

if strcmp(opts.dot.dotFile,'')
    file = 'graph_tempfile.dot' ;
    keep = false ;
else
    file = opts.dot.dotFile ;
    keep = true ;
end
dispMatrices = opts.dot.displayLevel ;
edgeNumbering = opts.dot.edgeNumbering ;
nodeNumbering = opts.dot.nodeNumbering ;

% Let's write neato .dot file
this.writeDot(file, edgeNumbering, nodeNumbering) ;

% Let's run neato to build the graph
err = runDot(file, 'graph_tempfile', {'png'}) ;

% Plot the resulting png picture
if ~ err
    h = figure ;
    if dispMatrices == 0
            imshow(imread('graph_tempfile.png')) ;    
    elseif dispMatrices == 1
        subplot(1, 2, 1) ;
        imshow(imread('graph_tempfile.png')) ;
        subplot(1, 2, 2) ;
        nMat = this.getNMats() ;
        text(0.4, 1, 'Atomistic matrices') ;
        for l = nMat:-1:1
            text(0.15, 1/(nMat+1)*l, sprintf('Matrix %d', l)) ;
            text(0.5, 1/(nMat+1)*l, num2str(full(this.getMat(l)))) ;
        end
        axis off ;
    elseif dispMatrices == 2
        subplot(1, 2, 1) ;
        imshow(imread('graph_tempfile.png')) ;
        subplot(1, 2, 2) ;    
        nEdges = this.getNEdges() ;
        text(0.4, 1, 'Product matrices') ;
        for e = nEdges:-1:1
            text(0.1, 1/(nEdges+1)*e, sprintf('Edge Product %s', num2str(this.labels{e}))) ;
            text(0.7, 1/(nEdges+1)*e, num2str(full(this.getMatProdEdge(e)))) ;
        end
        axis off ;    
    end
   delete('graph_tempfile.png') ;
end
if ~ keep
    delete(file) ;
end

end
