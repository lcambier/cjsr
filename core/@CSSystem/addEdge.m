function Sout = addEdge(Sin, in1, in2)
% ADDEDGE Add an edge
%
%   SOUT = SIN.ADDEDGE(EDGE) adds an edge EDGE = [i j s] with label s. In this
%   case, s should be a scalar, 1 <= s <= S.getNMats.
%   SOUT = SIN.ADDEDGE(EDGE, LABEL) add an edge [i j s] where EDGE
%   = [i j] and s = LABEL. In this case, label can be a vector (multilabel).
%   Moreover, 1 <= min(LABEL), max(LABEL) <= S.getNMats;
%
%   If i or j > S.getNNodes, then one (or two) new nodes are added.
%   One node added if min(i,j) <= S.getNNodes < max(i,j) = S.getNNodes + 1.
%   Two nodes, and an edge disconnected from S, 
%       if min(i,j) = S.getNNodes + 1 and max(i,j) = S.getNNodes + 2.
%
%   See also ADDMATRIX

if nargin == 2
    edge = in1(1:2) ;
    label = in1(3) ;
elseif nargin == 3
    edge = in1 ;
    label = in2 ;
else
    error('Needs 1 or 2 input arguments.') ;
end

if numel(edge) ~= 2
    error('edge should be a length-2 vector.') ;
end
% Check the nodes and the label
if any(edge <= 0) || any(edge > Sin.getNNodes()+2)
    error('Nodes out of range.') ;
end
if any(label <= 0) || any(label > Sin.getNMats())
    error('Labels out of range.') ;
end

% Check wether edge already belongs to the edges
[~,indx] = ismember(Sin.edges,edge,'rows') ;
idx = find(indx) ;
inLabels = Sin.getLabels() ;
for i = 1:numel(idx)
    if numel(inLabels{idx(i)}) == numel(label) && all(label == inLabels{idx(i)})
        warning('Edge already present, equals to edge number %d. Continuing', idx(i)) ;
    end    
end

Sout = Sin ;
% Check if both edges are greater
if edge(1) == Sin.getNNodes()+1 && edge(2) <= Sin.getNNodes() % One greater and == to NNodes+1 and one lower
    Sout.nNodes = Sout.nNodes + 1 ;
elseif edge(2) == Sin.getNNodes()+1 && edge(1) <= Sin.getNNodes() % Same
    Sout.nNodes = Sout.nNodes + 1 ;
elseif all(edge > Sin.getNNodes()) % Either both are greater
    Sout.nNodes = Sout.nNodes + 2 ;
elseif all(edge <= Sin.getNNodes()) % Or both lower 
    Sout.nNodes = Sout.nNodes ;
else
    error('Node out of range (at least one node index should be max S.getNNodes+1).')
end
Sout.edges = [Sout.edges ; edge] ;
Sout.labels{numel(Sout.labels)+1} = label ;


end