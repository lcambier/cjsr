function [Sout, monomials] = sosLift(Sin, d)
% SOSLIFT Sum-of-square lift
%
%   Sout = Sin.SOSLIFT(d) lifts the system Sin, where each matrix is replaced
%   by its SOS2d-lift. The underlying graph is unchanged. d should be greater
%   or equal to 1. If d == 1, Sout = Sin.
%   In the case where Sin contains multilabels, only the "atomistic" matrices
%   are changed
%   It stays consistent, since the lift of A*B is equal to the product of the
%   lifts of A and B.
%
%   Note: when using cjsrQuadMultinorm on this lifted system, don''t forget
%   to scale the result by 1/d (i.e. to raise the result to the power 1/d)
%   to get the CJSR of the original system or to use the getbounds method.
%
%   References: 
%   [1] P. A. Parrilo and A. Jadbabaie, "Approximation of the joint
%       spectral radius using sum of squares", Linear Algebra and its
%       Applications, vol. 428, no. 10, pp. 2385-2402, 2008.
%   [2] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015). 
%       Stability of discrete-time switching systems with constrained switching 
%       sequences. arXiv preprint arXiv:1503.06984.
%
%   See also TPRODUCTLIFT, MDEPENDANTPATH, ARBITRARYLIFT,
%   PARTIALPRODUCTLIFT, PARTIALDEPENDANTLIFT, CJSRQUADMULTINORM


if d <= 0 || mod(d, 1) ~= 0 || ~isscalar(d)
    error('d (%d) should be a scalar integer >= 1', d) ;
end

for l = 1:Sin.getNMats()
    [Sin.matrices{l}, monomials] = sosLiftMatrix(Sin.matrices{l}, d) ;
end
Sout = Sin ;
Sout.degree = Sin.degree * d;

end
