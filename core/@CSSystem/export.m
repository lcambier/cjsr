function export(this, file, types, opts)
% EXPORT Export system to various file types
%
%   S.EXPORT('graph',TYPES) will use the dot software to create a series of
%   files with the extensions in TYPES that represents the graph
%   S.EXPORT('graph',TYPES,OPTS) uses the optional structure OPTS.
%
%   OPTS can contain
%     opts.dot.nodeNumbering (false) : add the node number when set to true
%
%   Example : S.export('graph', {'dot', 'png', 'svg'}) wil create graph.dot,
%     graph.png and graph.svg, all representing the graph in different format.
%
%   Warning : the string in types should be accepted types by dot. See 
%   http://www.graphviz.org/doc/info/output.html for all the supported
%   extensions.
%
%   To use this function, the "dot" software needs to be installed. One way
%   to do it is to install graphviz (http://www.graphviz.org).
%
%   See also VIEW

if nargin == 3
    opts = cjsrSettings ;
end
this.writeDot(file, opts.dot.nodeNumbering) ;
runDot(file, file, types) ;
delete(file) ;
end