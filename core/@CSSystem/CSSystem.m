classdef CSSystem
    % CSSYSTEM Constrainted Switching System
    %
    % We briefly describe Constrained Switching System below. Please see
    % [1] for complete definitions.
    %
    % A CSS is a dynamical system of the form x(t+1) = A(s(t))*x(t), where 
    % s(t) is a discrete parameter in a range {1 ... N}, and A(s(t)) is an 
    % n x n matrix in a set M = {A(1), ..., A(N)}. The sequences s(0), s(1), ...
    % are non-deterministic, but must satisfy a set of rules given by an 
    % automaton G describing a regular language on the alphabet {1 ... N}.
    %
    % The automaton G(V,E) is represented as a graph, on a set of nodes V and 
    % directed labeled edges E. Edges are tuples of the form (i, j, s) where 
    % i is the origin, j the destination and s in {1,...,N} is a label
    % representing a matrix or, possible, a multi-label of some length T>1:
    % s = {s(1),...,s(T)} where each s(i) is in {1,...,N}.
    %
    % The trajectories of the system are then computed from paths in G. 
    % For the path (i(0), i(1), S(0)), ..., (i(k-1), i(k), S(k-1)), at time 
    % t = |S(0)| + ... + |S(k-1)| we have 
    % x(|S(0)| + ... + |S(k-1)|) = A(S(k-1))...A(S(0)) x(0), where for 
    % S = {s(1), ..., s(T)}, A(S) = A(s(T))...A(s(1)).
    %
    % This class is thus used to describe both the automaton G(V,E) and the
    % set of matrices M.
    % Nodes are labeled 1, 2, ..., nNodes where nNodes is the number of nodes
    % in the automaton. The graph can be disconnected, but there should always 
    % be an incoming or outgoing edge at each node. 
    %
    % To see all the methods of this class, type 
    % doc CSSystem   
    % The help of the constructor can be accessed through
    % help CSSystem.CSSystem    
    % 
    % References:
    %   [1] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015). 
    %       Stability of discrete-time switching systems with constrained switching 
    %       sequences. arXiv preprint arXiv:1503.06984.
    
    properties (Access = private)
        edges ;    % [nEdges x 2]
        % edges(e, 1) = origin (1 ... nNodes)
        % edges(e, 2) = destination (1 ... nNodes)
        labels ;   % {[1 x nLabels(l)]}
        nNodes ;   % The # of nodes
        matrices ; % {nMatrices x 1}
    end

    properties (Access = public)
        degree ; % Matrices are in a SOS lift of degree d
                 % take the dth root when computing the bound
                 % It is automatically adjusted by sosLift but
                 % it can also be manually tweaked.
    end
    
    methods
        
        function this = CSSystem(varargin)
            % CSSYSTEM Builds a CSSystem object
            %
            %   S = CSSYSTEM(MATRICES) builds a constrained switching system
            %   representing an *unconstrained* system with on the set of
            %   matrices MATRICES. MATRICES can be a single square matrix or 
            %   a cell array of square matrices.  
            %
            %   S = CSSYSTEM(EDGES, MATRICES) builds a system with the edges
            %   described in EDGES and the matrices in MATRICES.
            %   EDGES is a [nEdges x 3] matrix where each row is (i, j, s)
            %   with i the origin of the edge, j the destination and the
            %   label s. In this case, the label can only be an integer.
            %   MATRICES is a cell array of square matrices of the same size.  
            %
            %   S = CSSYSTEM(EDGES, MATRICES, LABELS) builds a system.
            %   EDGES is a [nEdges x 2] matrix where each row is (i, j), 
            %   where i is the source of the edge, and j its destination.
            %   MATRICES is a cell array of N square matrices of the same
            %   sizes.
            %   LABELS is a cell array with nEdges elements. 
            %   For  1 <= e <= nEdges, LABELS{e} is an integer array,
            %   with each element in 1 ... N.  
            %
            %   For instance, if LABELS{e} is [1 2 3], it means that from
            %   node EDGES(e, 1) to node EDGES(e, 2), the encountered
            %   matrices are MATRICES{1}, then MATRICES{2} and then
            %   MATRICES{3}., and the resulting product is
            %   MATRICES{3}*MATRICES{2}*MATRICES{1}.
            %
            %   see also GETNEDGES, GETNNODES, GETNLABELS, GETNMATS,
            %   GETMATSIZE, GETEDGES, GETLABELS, GETMATPRODEDGE,
            %   GETMATPRODLABEL, GETMATRICES, ARBITRARYLIFT, TPRODUCTLIFT,
            %   MDEPENDANTLIFT, SOSLIFT, PARTIALDEPENDANTLIFT,
            %   PARTIALPRODUCTLIFT, CJSRQUADMULTINORM,
            %   CJSRCPLXPOLYTOPEMULTINORM, EXPORT, GETGRAPHMONOLABELS, VIEW,
            %   VIEWMULTINORM, ADDEDGE, ADDMATRIX
                                    
            % Inputs
            if nargin == 0
                error('CSSystem needs at least one argument') ;
            elseif nargin == 1
                if iscell(varargin{1})
                    matrices = varargin{1} ;
                elseif ismatrix(varargin{1})
                    matrices = {varargin{1}} ;
                end
                nMats = numel(matrices) ;
                edges = ones(nMats,2) ;
                labels = num2cell((1:nMats)') ;
            elseif nargin >= 2
                edges = varargin{1} ;
                matrices = varargin{2} ;
                if nargin == 2
                    if ~ ismatrix(edges) || size(edges, 2) ~= 3
                        error('edges should be [nEdges x 3]') ;
                    end
                    labels = num2cell(edges(:, 3)) ;
                    edges = edges(:, 1:2) ;
                elseif nargin == 3
                    if ~ ismatrix(edges) || size(edges, 2) ~= 2
                        error('edges should be [nEdges x 2]') ;
                    end
                    labels = varargin{3} ;
                else
                    error('Wrong number of inputs') ;
                end
            end
            % Matrices
            if ~ iscell(matrices)
                error('matrices should be a cell array') ;
            end
            if (numel(matrices) ~= 0) && ~ isvector(matrices)
                error('matrices should be a cell-vector (row or column)') ;
            end
            matrices = reshape(matrices, [numel(matrices), 1]) ;
            % We need all nodes to be there ? 
            E = edges(:) ;
            allNodesSorted = sort(unique(E)) ;
            nNodes = max(allNodesSorted) ;
            errStr = ('Nodes should have labels (1 ... nNodes)') ;
            if numel(allNodesSorted) ~= nNodes
                error(errStr) ;
            elseif any(allNodesSorted ~= (1:nNodes)')
                error(errStr) ;
            end
            % Check strong connectivity
            if ~ isStronglyConnected(edges)
                warning('Your graph is not strongly connected. This is fine, but this might lead to strange results at some point.') ;   
                warning('We strongly advice splitting the graph in connected components.') ;   
            end
            % We need all labels to be there
            L = flattenAndConcat(labels) ;
            LSorted = sort(unique(L)) ;
            errStr = ('All labels should be present, from 1 to nLabel') ;
            if numel(LSorted) ~= max(LSorted)
                error(errStr) ;
            elseif any(LSorted ~= (1:max(LSorted))')
                error(errStr) ;
            end
            % We need all matrices with the same size
            for l = 1:numel(matrices)
                if size(matrices{l},1) ~= size(matrices{l},2) || any(size(matrices{l}) ~= size(matrices{1}))
                    error('All matrices should be square and with the same size.') ;
                end
            end
            % We need as many matrices as the max label
            Lmax = max(L) ;
            if numel(matrices) ~= Lmax
                error('Some matrices are missing/redundant ! %d in matrices but max(labels) = %d.', numel(matrices), Lmax) ;
            end
            % Done
            this.edges = edges ;
            this.matrices = matrices ;
            this.nNodes = nNodes ;
            this.labels = labels ;
            this.degree = 1 ;
        end
        
        %% Getter
        
        function out = getNEdges(this)
            % GETNEDGES Number of edges.
            %
            %   N = S.GETNEDGES() returns the number of edges in system S.
            %
            %   See also GETNNODES, GETNMATS, GETNLABELS, GETMATSIZE.
            out = size(this.edges, 1) ;
        end
        
        function out = getNNodes(this)
            % GETNNODES Number of nodes.
            %
            %   N = S.GETNNODES() returns the number of nodes in system S.
            %
            %   See also GETNEDGES, GETNLABELS, GETNMATS, GETMATSIZE.
            out = this.nNodes ;
        end
        
        function out = getLabels(this, e)
            % GETLABELS (Multi-)label(s).
            %
            %   L = S.GETLABELS() returns all the (multi-) labels in a cell
            %   array of arrays.            
            %   L = S.GETLABELS(E) return the multilabel of the edges in e
            %   as a cell array of vectors.
            %
            %   See also GETEDGES, GETMATPRODEDGE, GETMATPRODLABEL,
            %   GETMATRICES.
            if nargin == 1
                e = 1:this.getNEdges() ;
            end              
            if any(e < 1) || any(e > this.getNEdges())
                error('All entries in e should be >= 1 and <= S.getNEdges()') ;
            end
            if ~ isvector(e)
                error('e should be a vector') ;
            end
            out = cell(numel(e),1) ;
            for i = 1:numel(e)
                out{i} = this.labels{e(i)} ;
            end        
        end
        
        function out = getMatSize(this)
            % GETMATSIZE Matrices size.
            %
            %   s = S.GETMATSIZE() returns the dimension of all the matrices in
            %   the CSSystem (or, equivalently, the dimension of the state
            %   vector).
            %
            %   See also GETNEDGES, GETNNODES, GETNLABELS, GETNMATS
            if numel(this.matrices) == 0
                error('Not matrices defined. No size can be returned') ;
            end
            out = size(this.matrices{1}, 1) ;
        end       
        
        function out = getMatProdLabel(this, label)
            % GETMATPRODLABEL Matrix corresponding to (multi-)label.
            %
            %   M = S.GETMATPRODLABEL(L) returns the matrix product
            %   corresponding to label L
            %
            %   Example : if label L = [1 2 3], returns A3*A2*A1
            %
            %   See also GETMATRICES, GETMATPRODEDGE
            out = eye(this.getMatSize()) ;
            if any(label < 1) || any(label > this.getNMats)
                error('One of the labels is either <= 0 or greater than the number of ''atomistic'' matrices') ;
            end
            for i = 1:numel(label)
                out = this.getMat(label(i)) * out ;
            end
        end
        
        function out = getMatProdEdge(this, e)
            % GETMATPRODEDGE Matrix product for the sequence of edges E.
            % 
            %   M = S.GETMATPRODEDGE(E) 
            %   If numel(E) = 1, returns the matrix product on edge E.
            %   If E is a sequence of edges, returns the corresponding
            %   matrix-products.
            %
            %
            %   See also GETMATRICES, GETMATPRODLABEL.
            if any(e < 1) || any(e > this.getNEdges())
                error('e should be >= 1 and <= S.getNEdges()') ;
            end
            out = 1;
            for k = 1:numel(e);
                lab = this.labels{e(k)} ;
                out = this.getMatProdLabel(lab) * out ;
            end
        end
        
        function out = getNMats(this)
            % GETNMATS The number of matrices
            %
            %   N = S.GETNMATS() returns the number of "atomistic" matrices,
            %   i.e. matrices corresponding to labels of length 1
            %
            %   See also GETNEDGES, GETNLABELS, GETNNODES
            out = numel(this.matrices) ;
        end
                      
        function out = getMatrices(this, l)
            % GETMATRICES The matrices
            %
            %   Ms = S.GETMATRICES() returns all the "atomistic" matrices in
            %   a cell array, corresponding to all the mono-labels
            %   Ms = S.GETMATRICES(L) returns all the "atomistic" matrices
            %   with the mono-labels in L
            %
            %   See also GETMATPRODEDGE, GETMATPRODLABEL, GETMAT
            if nargin == 1
                out = this.matrices ;
            else
                out = cell(numel(l), 1) ;
                for i = 1:numel(l)
                    out{i} = this.matrices{l(i)} ;
                end                
            end
        end
        
        function out = getMat(this, l)
            % GETMAT Matrix
            %
            %   M = S.GETMAT(L) returns the lth matrix. It corresponds to the
            %   matrix associated with the (mono-) label L
            %
            %   See also GETMATRICES
            if ~ isscalar(l) || l <= 0 || l > this.getNMats()
                error('The given label is either <= 0 or greater than the number of ''atomistic'' matrices. It should also be a scalar.') ;
            end
            out = this.matrices{l} ;
        end

        
        function out = getEdges(this, e)
            % GETEDGES The edges.
            %
            %   E = S.GETEDGES() returns all the edges in a [nEdges x 2]
            %   matrix. The edges in this case only contain the extremities
            %   of the edges, *not* the labels.
            %   E = S.GETEDGES(INDEX) returns the edges in INDEX only. 
            %   INDEX should be a subset of 1:S.getNEdges().
            %
            %   If you want the labels as well, use getLabels in
            %   combination with getEdges.
            %
            %   See also GETGRAPHMONOLABELS, GETLABELS, GETMATRICES
            if nargin == 1
                e = 1:this.getNEdges() ;
            end
            if any(e < 1) || any(e > this.getNEdges())
                error('Index should be >= 1 and <= S.getNEdges()') ;
            end
            out = this.edges(e,:) ;
        end
        
        function out = getNLabels(this, e)
            % GETNLABELS Size of multilabel.
            %
            %   N = S.GETNLABELS(E) returns the size of the (multi-)label at
            %   edge E.
            %
            %   See also GETNEDGES, GETNMATS, GETNNODES
            if ~ isscalar(e) || e <= 0 || e > this.getNEdges()
                error('e should be a scalar >= 1 and <= S.getNEdges()') ;
            end
            out = numel(this.labels{e}) ;
        end

        function out = getAdjacencyMat(this)
            % GETADJACENCYMAT The adjacency matrix of the automaton.
            %
            %   A = S.GETADJACENCYMAT() returns the adjacency matrix.
            nNodes = this.getNNodes() ;
            nEdges = this.getNEdges() ;
            edges = this.edges ;
            out = zeros(nNodes, nNodes) ;
            for e = 1:this.getNEdges()
                tail = edges(e,1) ;
                head = edges(e,2) ;
                out(head, tail) = out(head, tail) + 1 ;
            end
        end

        function [lb, ub] = getbounds(this, gamma)
            d = this.degree ;
            ub = gamma^(1/d) ;
            lb = ub / max(this.getMatSize(), eigs(this.getAdjacencyMat(),1))^(1/(2*d)) ;
        end
        
        function [graph, newMats, nLabels] = getGraphMonolabels(this)
            % GETGRAPHMONOLABELS Returns corresponding graph
            %
            %   [graph, mats, nLabels] = S.GETGRAPHMONOLABELS() returns a representation of the
            %   CSSystem S using a directed graph with labelled edges
            %   In this graph, all edges are labelled using mono-label, i.e.
            %   integers from 1 to N and the corresponding matrices are
            %   stored in mats, a Nx1 cell array. There are no multi-label
            %   anymore.
            %   graph is a structure with fields :
            %     graph.edges : a [nEdges x 3] matrix, where is row is (i, j, s)
            %   where i is the origin and j the destination of the edge,
            %   while s in an integer from 1 to N giving the corresponding
            %   matrix.
            %     graph.nNodes : the number of nodes in the graph
            %   nLabels is a vector giving the *original* number of atomistic
            %   matrices at each edge
            %
            %   If multilabels are present in system S, they are mapped to
            %   a mono-label (say I) and the corresponding matrix product can
            %   be found in mats{I}.
            %
            %   This function can be useful to interface the CSSystem class
            %   with, for instance, the JSR Toolbox
            %   (http://www.mathworks.com/matlabcentral/fileexchange/33202-the-jsr-toolbox)
            %
            %   See also GETEDGES, GETMATRICES
            
            
            % Get max label
            labMax = 1 ;
            for e = 1:this.getNEdges()
                labMax = max(this.getNLabels(e)) ;
            end
            % Prealloc
            nLabels = zeros(this.getNEdges(), 1) ;
            labelsMat = zeros(this.getNEdges(), labMax) ;
            % Fill
            for e = 1:this.getNEdges()
                lab = this.getLabels(e) ;
                labelsMat(e, 1:numel(lab{1})) = lab{1} ;
                nLabels(e) = this.getNLabels(e) ;
            end
            % Find unique
            [~,edgeMap,newLab] = unique(labelsMat, 'rows') ;
            
            newEdges = [this.edges newLab] ;
            newMats = cell(numel(edgeMap), 1) ;
            for i = 1:numel(edgeMap)
                newMats{i} = this.getMatProdEdge(edgeMap(i)) ;
            end
            
            graph = struct() ;
            graph.edges = newEdges ;
            graph.nNodes = this.nNodes ;
        end
        
        
        %% Modifier
        
        % Add stuff
        Sout = addEdge(Sin, in1, in2)
        [Sout, label] = addMatrix(Sin, M)
        
        %% Lifts
        
        Sout = TProductLift(Sin, T)
        Sout = MDependantLift(Sin, M, direction)
        Sout = arbitraryLift(Sin)
        [Sout, monomials] = sosLift(Sin, d)
        
        %% Partial lifts
        
        Sout = partialDependantLift(Sys, nodes, direction)
        Sout = partialProductLift(Sys, edges, direction)
        
        %% Solvers

        [lb, smp, seq, info] = cjsrBuildSequence(Sys, l, Qdual, opts)
        [gamma, Qprimal, Qdual, info] = cjsrQuadMultinorm(Sys, opts)
        [W, rho, smp, info] = cjsrCplxPolytopeMultinorm(Sys, smp, opts)
        
        %% Auxiliary
        
        export(this, file, types,opts) % Export the CSSystem to .dot, .png, .svg, .ps, etc
        h = view(this, opts)
        h = viewMultinorm(this, type, in1, in2)
        
        function disp(this)
            % DISP Displays the CSSystem
            %
            %   disp(S) display the number of edges, nodes and matrices of
            %   the CSSystem
            fprintf('CSSystem with %d edges, %d nodes and %d matrices.\n', ...
                this.getNEdges(), this.getNNodes(), this.getNMats()) ;
        end
        
    end
    
    methods (Access = private, Static)
        
        S = concat(S1, S2)
        
    end
    
    methods (Access = private)       
        
        [primal, dual] = getOptimizer(Sys, opt)
        opti = getSedumiProblem(Sys, opt)
        
        [Q, feasible] = solveOptimizer(Sys, opti, gamma, opts)
        [Qprimal, Qdual, feasible] = solveSedumiProblem(Sys, opti, gamma, opts)
        
        writeDot(Sys, file, edgeNumbering, nodeNumbering)
        
    end
    
end
