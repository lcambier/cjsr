function S = concat(S1, S2)
% CONCAT Concatenates two systems
%
%   S = CONCAT(S1, S2) returns a system S where there is an edge between i
%   and j if there is a path of length 2, starting at node i in system S1 and
%   ending at node j in system S2.
%   The matrices are assumed to be the same in system S1 and S2.
%
% This is an internal private function that cannot be used outside this
% class

if S1.nNodes ~= S2.nNodes || S1.getMatSize() ~= S2.getMatSize() || S1.getNMats() ~= S2.getNMats()
    error('Both system should have the same number of nodes and the same matrices size') ;
end
for m = 1:S1.getNMats()
    if any(S1.getMat(m) ~= S2.getMat(m))
        error('Matrices should be the same in S1 and S2') ;
    end
end

% Concat arrays
% edges(:, 3) is filled with zero.
edges1 = [S1.edges (1:S1.getNEdges())'] ;
edges2 = [S2.edges (1:S2.getNEdges())'] ;
[edges, oldLabels] = concat(edges1, edges2) ;

% Append labels
edges = edges(:, 1:2) ;
labels = cell(size(edges, 1), 1) ;
for l = 1:size(edges, 1)
    l1 = S1.getLabels(oldLabels(l,1)) ;    
    l2 = S2.getLabels(oldLabels(l,2)) ;
    labels{l} = [l1{1} l2{1}] ;
end

% Create output
matrices = S1.getMatrices() ;
S = CSSystem(edges, matrices, labels) ;
end