function Sout = MDependantLift(Sys, M, direction)
% MDEPENDANTLIFT M-path or horizon-dependant lift
%   
%   SOUT = SIN.MDEPENDANTLIFT(M) or
%   SOUT = SIN.MDEPENDANTLIFT(M, 'path') or
%   SOUT = SIN.MDEPENDANTLIFT(M, 'past') returns the M-path-dependant lift 
%   of SIN. Allows to check for the existence of path-dependant Lyapunov
%   functions (see [1],[2]).
%   the M-path-dependant lift  is built as follow : let S(G(V,E),Sigma) be
%   the system SIN and S(G(V',E'),Sigma') be the system SOUT.
%   For each path p of length M, create a node vp in V'. Then, there is an
%   edge (vp1, vp2, s) in E' between the nodes vp1 and vp2 if there is a
%   path of length M+1 in G(V,E) with edges (e1 ... eM+1) sucht that
%   p1 = (e1 ... eM) and p2 = (e2 ... eM+1), and such that the label on
%   eM+1 is s [1].
%   In short, it splits each node according to the number of edges/path that
%   *enter* the node and builds the graph such that the "language" is
%   preserved.
%
%   SOUT = S.MDEPENDANTLIFT(M, 'horizon') or
%   SOUT = S.MDEPENDANTLIFT(M, 'future') does the same but in the "future", 
%   i.e. according to the edges leaving each node. 
%   Variant of the path-dependant Lyapunov function method, where a
%   quadratic form is associated to each sequence of a given lenght in the
%   future ([2], with no memory).
%
%   M should be greater or equal to 1. If M == 0, SOUT = SIN.
%   direction should be a string equal to path (or past) or horizon (or
%   future).
%   
%   References:
%   [1] J.-W. Lee and G. E. Dullerud, �Uniform stabilization of
%       discrete-time switched and markovian jump linear systems,�
%       Automatica, 42(2), 205-218, 2006.
%   [2] R. Essick, J.-W. Lee, and G. E. Dullerud, �Control of linear
%       switched systems with receding horizon modal information,�
%       IEEE Transactions on Automatic Control, to appear.
%   [3] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015). 
%       Stability of discrete-time switching systems with constrained switching 
%       sequences. arXiv preprint arXiv:1503.06984.
%
%   See also TPRODUCTLIFT, SOSLIFT, ARBITRARYLIFT, PARTIALPRODUCTLIFT,
%   PARTIALDEPENDANTLIFT

if ~isscalar(M) || M < 0 || mod(M, 1) ~= 0
    error(sprintf('M (%d) should be a scalar integer >= 0', M)) ;
end
if nargin == 2
    direction = 'past';
end
if ~  (strcmp(direction,'past') || strcmp(direction,'future') || ...
       strcmp(direction,'path') || strcmp(direction,'horizon'))
    error('direction (%s) should be past or future.',direction) ;
end

Sout = Sys ;
for m = 1:M
    Sout = M1DependantLift(Sout, direction) ; % +1 level, in the past or in the future
end

end

function Sout = M1DependantLift(Sys, direction)

matrices = Sys.matrices ;
past = (strcmp(direction,'past') || strcmp(direction,'path')) ;

% Count number of dep. path to prealloc
% Yes : eventhough we do things twice, it is actually useful
nEdges = 0 ;
for e = 1:Sys.getNEdges()
    if past
        nNewEdges = sum(Sys.edges(:, 2) == Sys.edges(e, 1)) ; % '1Path' dependant, in the past
    else
        nNewEdges = sum(Sys.edges(e, 2) == Sys.edges(:, 1)) ; % '1Horizon' dependant, in the future
    end
    nEdges = nEdges + nNewEdges ;
end
edges = zeros(nEdges, 2) ;
labels = cell(nEdges, 1) ;

% Actually do it
ee = 1 ;
for e = 1:Sys.getNEdges()
    if past
        OnePathDep = find(Sys.edges(:, 2) == Sys.edges(e, 1)) ; % '1Path' dependant, in the past
        nNewEdges = numel(OnePathDep) ;
        edges(ee:ee+nNewEdges-1,:) = [OnePathDep(:, 1)    e*ones(nNewEdges,1)] ;
        
    else
        OneHorizonDep = find(Sys.edges(e, 2) == Sys.edges(:, 1)) ; % '1Horizon' dependant, in the future
        nNewEdges = numel(OneHorizonDep) ;
        edges(ee:ee+nNewEdges-1,:) = [e*ones(nNewEdges,1) OneHorizonDep(:, 1)] ;
    end
    labelsOld = Sys.labels{e} ;
    for l = ee:ee+nNewEdges-1
        labels{l} = labelsOld ;
    end
    ee = ee+nNewEdges ;
end

Sout = CSSystem(edges, matrices, labels) ;
Sout.degree = Sys.degree ;
end
