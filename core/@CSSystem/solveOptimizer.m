function [Q, feasible] = solveOptimizer(Sys, opti, gamma, opts)
% SOLVEOPTIMIZER Solve the Yalmip formulation
%
%   [Q, FEAS] = S.SOLVEOPTIMIZER(OPTI, GAMMA, OPTS) solves the problem
%   OPTI on system S with a given GAMMA and OPTS.
%   OPTI should come from the output of getOptimizer
%   GAMMA is the candidate CJSR
%   OPTS is an option structure
%
%   This function returns Q and FEAS = true is GAMMA is feasible, or FEAS =
%   false otherwise.
%   
%   This is an internal private function that cannot be used outside of
%   this class (private).
%   See also GETOPTIMIZER

[Q, flag] = opti{gamma} ;
feasible = isThisFeasible(flag) ;
end

function feasible = isThisFeasible(flag)

switch flag
    case 0 % Successfully solved (http://users.isy.liu.se/johanl/yalmip/pmwiki.php?n=Commands.yalmiperror)
        feasible = true ;
    case 1
        feasible = false ;
    otherwise
        feasible = false ;
        warning('Error while solving problem with Yalmip (probably numerical)') ;
end
end