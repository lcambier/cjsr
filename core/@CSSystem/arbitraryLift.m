function Sout = arbitraryLift(Sin)
% ARBITRARYLIFT Arbitrary Lift
%
%   Sout = Sin.ARBITRARYLIFT() lifts the constraint system to a unconstrained
%   system Sout build as follows.
%   Sout contains only one node, and onde edge per edge in Sin. The matrix
%   associated to each edge (i, j, s) is
%   A = (e(j) * e(i)') [x] A_s
%   where [x] is the Kronecker product and where e(i) is a vector in R^(Sin.getNNodes())
%   where only the ith entry is non-zero and is equal to 1.
%
%   References: 
%   [1] V. Kozyakin, "The Berger-Wang formula for the markovian
%       joint spectral radius", Linear Algebra and its Applications,
%       vol. 448, pp. 315-328, 2014,
%   [2] Y. Wang, N. Roohi, G. E. Dullerud, and M. Viswanathan
%       "Stability of linear autonomous systems under regular
%       switching sequences", in Proceedings of the 53rd Conference
%       on Decision and Control. IEEE, 2014, pp. 5445-5450.
%   [3] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015)
%       "Stability of discrete-time switching systems with constrained
%       switching sequences" arXiv preprint arXiv:1503.06984.
%
%   See also TPRODUCTLIFT, MDEPENDANTLIFT, SOSLIFT, PARTIALPRODUCTLIFT,
%   PARTIALDEPENDANTLIFT

    nEdges = Sin.getNEdges() ;
    nNodes = Sin.getNNodes() ;
    matrices = cell(nEdges, 1) ;
    matSize = Sin.getMatSize() ;
    for e = 1:nEdges
        edge = Sin.getEdges(e) ;
        i = edge(1) ; j = edge(2) ;
        % A(i, j, sigma) = (e(j) * e(i)^T) x A_sigma (x = kron product)
        I = (j-1)*matSize+1:j*matSize ; % Yes , j and not i. Because of the previous equation
        J = (i-1)*matSize+1:i*matSize ;
        A = Sin.getMatProdEdge(e) ;
        
        V = A(:) ;
        [I, J] = ndgrid(I, J) ;
        I = I(:) ;
        J = J(:) ;
        
        matrices{e,1} = sparse(I, J, V, ...
            matSize*nNodes, ...
            matSize*nNodes, ...
            matSize*matSize) ;        
    end
    
    edges = [ones(nEdges, 2) (1:nEdges)'] ;
    
    Sout = CSSystem(edges, matrices) ;
end