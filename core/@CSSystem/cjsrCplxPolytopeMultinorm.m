function [W, bounds, smp, info] = cjsrCplxPolytopeMultinorm(Sys, in1, in2)
% CJSRCPLXPOLYTOPEMULTINORM Complex Polytope Norm of a CSSystem
%
%   [W, BOUNDS, SMP] = SYS.CJSRCPLXPOLYTOPEMULTINORM() or
%   [W, BOUNDS, SMP] = SYS.CJSRCPLXPOLYTOPEMULTINORM(OPTS) or
%   [W, BOUNDS, SMP] = SYS.CJSRCPLXPOLYTOPEMULTINORM(SMP)  or
%   [W, BOUNDS, SMP] = SYS.CJSRCPLXPOLYTOPEMULTINORM(SMP, OPTS)
%   [W, BOUNDS, SMP, INFO] = SYS.CJSRCPLXPOLYTOPEMULTINORM(SMP, OPTS)
%   returns the extremal complex polytope norm of a CSSystem SYS
%   SMP should be a candidate Spectrum Maximizing Product, represented as a
%   sequence of edges. 
%   If no SMP is provided, the algorithm will first pick an arbitrary
%   cycle of maximal length OPTS.cpoly.maxPathsLength.
%   OPTS is an optional option structure. It can contain
%   - OPTS.verbose (1) : the level of verbose. 0 prints nothing, 1 prints
%     some log informations
%   - OPTS.cpoly.checkNewCycle (true) : if set to false, does not check for
%     better SMP while the algorithm runs
%   - OPTS.cpoly.maxPathsLength (10) : the maximum length of the path used
%     to improve the candidate S.M.P.
%   - OPTS.cpoly.plot (false) : if set to true, plot the complex polytope during
%     the algorithm execution.
%   - OPTS.cpoly.solver ('sedumi') : the solver name, either 'sedumi' or
%     'sdpt3'. Use opts.sedumi or opts.sdpt3 to pass options to the solver.
%     The default value should work fine in most cases.
%   - OPTS.cpoly.maxiter (25) : the total maximum number of iterations. Note that 
%     this includes all iterations, including the restarts. If the algorithm restarts
%     after 5 iterations, they will still be accoutned for in this maximum number
%     of iterations.
%   - OPTS.cpoly.reltol (1e-7) : the relative tolerance of the SDP solver
%   W is the essential set of vertices of the resulting complex polyhedron
%   BOUNDS is a length-2 vector of upper and lower bounds on the CJSR
%   If the algorithm successfully terminates,
%   bounds(1) == bounds(2) ==  cjsr   
%   SMP is the cycle with the highest averaged spectral radius found. 
%   INFO is a structure with several fields containing informations on the
%   algorithm.
%   - time : a vector containing the (absolute) time of the end of each
%     iteration.
%   - rho : contains the estimates of the CJSR corresponding to each timing
%     in time
%
%   For constrained switching systems, we are interested in finding a set of
%   (complex) polytopes P1 ... PN, where N is the number of nodes in the graph, 
%   such that for a system with CJSR = 1, for any node n1, 
%   there is a node n2 and an edge (n2, n1, s) such that As*P2 is on the
%   border of P1. 
%
%   The algorithm tries to build such a set of polytopes by
%   successively applying (scaled using the current spectral radius estimate)
%   matrices to the current vertices of the complex polytopes.
%   This builds from [1] where this is applied to arbitrary products.
%
%   The algorithm starts with the leading eigenvectors of the smp product
%   (and his conjugate) and then apply the product on the edges.
%
%
%   For any cycle c, the averaged spectral radius of c, 
%   abs(eigs(S.getMatProdEdge(c),1))^(1/length(c)) is a lower bound on the
%   cjsr.
%   The best cycle found so far is the one achieving the best lower bound.
%
%   If the algorithm find a better cycle while exploring the graph, it
%   replaces the estimate of cjsr with the leading eigenvalue of this cycle
%   (this was not present in [1]).
%
%   Note that this method is fully experimental, as no theory seems to
%   support his convergence.
%   Still, when the right smp is provided, it is observed that the method
%   succesfully recover what seems to be a correct complex polytope-norm.
%   When a non extremal SMP is provided, the method usually returns the right
%   one after a few iterations.
%
%   Note that the opts.cpoly.maxPathsLength should be kept low enough to
%   avoid an increasingly high number of cycle. Still, below 10, the time
%   spent in the new cycle generation is almost insignificant compared to the
%   time spent in the solver.
%
%  References :
%  [1] Guglielmi, Nicola, and Marino Zennaro. "An algorithm for finding extremal
%      polytope norms of matrix families." Linear Algebra and its Applications
%      428.10 (2008): 2265-2282.
%
% See also CJSRQUADMULTINORM

% Sys.EXTREMALCOMPLEXPOLYTOPENORM()
if nargin == 1
    opts = cjsrSettings() ;
    smp = findCycle(Sys.getEdges(), opts.cpoly.maxPathsLength) ;
    if isempty(smp)
        error('No cycle found of length at most %d. Please increase opts.cpoly.maxPathsLength', opts.cpoly.maxPathsLength) ;
    end
elseif nargin == 2
    % Sys.EXTREMALCOMPLEXPOLYTOPENORM(opts)
    if isstruct(in1) 
        opts = in1 ;
        smp = findCycle(Sys.getEdges(), opts.cpoly.maxPathsLength) ;
        if isempty(smp)
            error('No cycle found of length at most %d. Please increase opts.cpoly.maxPathsLength', opts.cpoly.maxPathsLength) ;
        end
    % Sys.EXTREMALCOMPLEXPOLYTOPENORM(smp)    
    else
        smp = in1 ;
        opts = cjsrSettings() ;
    end
% Sys.EXTREMALCOMPLEXPOLYTOPENORM(smp, opts)          
elseif nargin == 3
    smp = in1 ;
    opts = in2 ;
else
    error('Wrong number of inputs arguments') ;
end

smp = smp(:)' ;

nNodes = Sys.getNNodes() ;
nEdges = Sys.getNEdges() ;
edges = Sys.edges ;

s_tot = 0 ;
info = struct() ;
info.time = [] ;
info.rho = [] ;



%% MAIN LOOP
while true %% Loop over the S.M.P.
    
    initOuter = tic() ;
    
    % Check the cycle property
    for i = 2:numel(smp)
        if edges(smp(i), 1) ~= edges(smp(i-1), 2)
            error('The SMP should be a cycle') ;
        end
    end
    if edges(smp(1), 1) ~= edges(smp(end), 2)
        error('The SMP should be a cycle') ;
    end
        
    % Prealloc cells
    W = cell(nNodes, 1) ;
    X = cell(nNodes, 1) ;
    V = cell(nNodes, 1) ;
    normAw = cell(nNodes, 1) ; % A vector with the norm of each Medge*w using W{n}
    
    % (0) Pick an arbitrary node
    % Let's pick the first one
    % Compute the product associated with the cycle
    P = eye(Sys.getMatSize()) ;
    k = 0 ;
    for e = smp
        k = k + Sys.getNLabels(e) ;
        P = Sys.getMatProdEdge(e) * P ;
    end
    n = edges(smp(1), 1) ;
    [u,lambda] = eigs(P,1) ;
    if abs(u(end)) > 1e-6
        u = u/u(end) ;
    end
    % Prepare the data
    if isreal(u)
        W{n} = u ; % Avoid duplicates if u is real
        X{n} = u ;
    else
        W{n} = [u conj(u)] ;
        X{n} = [u conj(u)] ;
    end
    rho = abs(lambda)^(1/k) ; % Compute the estimation of the CJSR
        
    % Dispay a few things ...
    if opts.verbose > 0
        fprintf('Algorithm (re-)started with cycle (edges) : %s\n',num2str(smp)) ;
    end
    if opts.verbose > 0
        fprintf('Initial vector : %s\n',num2str(u.'))        
    end
    
    % Check node-irreducibility and rank-completeness at each node
    [diag] = trajectoryCollapses(Sys, W, rho) ;
    if diag
        fprintf('Abort: we won''t be able to find a proper complex polytope.\n')
        fprintf('This is either because the graph is disconnected,\n')
        fprintf('or because it appears that trajectories collapse into a non-trivial subspace. \n')
        W = [];
        bounds = [] ;        
        return ;
    end
    
    % Store the paths of length 1, i.e., the edges
    paths = cell(nEdges, 1) ;
    nodeEndOfPaths = zeros(nEdges, 1) ;
    for e = 1:nEdges
        newPath = struct() ;
        newPath.path = e ;
        newPath.prod = Sys.getMatProdEdge(e) ;
        newPath.eig = [] ;
        paths{e} = newPath ;
        if length(newPath.path) < opts.cpoly.maxPathsLength
            nodeEndOfPaths(e) = edges(e, 2) ;
        else
            nodeEndOfPaths(e) = 0 ;
        end 
    end
    
    % Prepare figure for plot
    if opts.verbose > 0
        nColsPlot = ceil(sqrt(nNodes)) ;
        nRowsPlot = ceil(nNodes/nColsPlot) ;
        fprintf('Candidate CJSR is %f\n', rho) ;
    end
    s = 0 ;
    if opts.cpoly.plot > 0
        figure() ;
    end
    
    % Update info struct
    if numel(info.time) >= 1
        info.time(numel(info.time)+1) = info.time(end) + toc(initOuter) ;
    else
        info.time(numel(info.time)+1) = toc(initOuter) ;
    end
    info.rho(numel(info.rho)+1) = rho ;
    
    while true % Polytope construction
        stop = zeros(nNodes, 1) ;
        restart = false ;
        s = s+1 ;
        s_tot = s_tot+1 ;
        initInner = tic() ;
        
        % (0) Stop or not
        if s_tot > opts.cpoly.maxiter
            fprintf('Maximum number of iteration reached. Aborting.\n') ;
            restart = false ;
            break ;
        end
        if opts.verbose > 0
            fprintf('It. %d\n', s) ;            
        end
        
        % (1) Cycle detection
        % Create new paths and propagate
        for e = 1:nEdges
            edge = edges(e, :) ;
            Medge = Sys.getMatProdEdge(e) ;
            % Add new paths
            if opts.cpoly.checkNewCycle
                % if edges(paths{p}.path(end),2) == edge(1) && length(paths{p}.path) < opts.cpoly.maxPathsLength
                pathsToAddTo = find(nodeEndOfPaths == edge(1)) ; 
                for p = pathsToAddTo'
                    newPath = struct() ;
                    newPath.path = [paths{p}.path e] ;
                    newPath.prod = Medge * paths{p}.prod ;
                    newPath.eig = [] ;
                    paths{end+1,1} = newPath ;
                    if length(newPath.path) < opts.cpoly.maxPathsLength % Will automatically skip path that would be too long
                        nodeEndOfPaths(end+1,1) = edges(e, 2) ;
                    else
                        nodeEndOfPaths(end+1,1) = 0 ;
                    end                      
                end                
            end
        end
        V = propagateVertices(Sys, X, rho) ;       
        
        % (2) Better smp detection
        if opts.cpoly.checkNewCycle
            [paths, nodeEndOfPaths] = trimpaths(paths, nodeEndOfPaths) ; % Remove duplicates
            oldRho = rho ;
            if opts.verbose > 0
                fprintf('Looking through %d paths for better S.M.P.\n', numel(paths)) ;
            end
            for p = 1:numel(paths)
                if edges(paths{p}.path(1),1) == edges(paths{p}.path(end),2) % If the first and last edges match on their extremities
                    P = paths{p}.prod ;                    
                    k = 0 ;
                    for e = paths{p}.path
                        k = k + Sys.getNLabels(e) ; % To account for multilabels
                    end
                    if isempty(paths{p}.eig)
                        paths{p}.eig = eigs(P,1) ;                    
                    end
                    lambda = paths{p}.eig ;
                    newRho = abs(lambda)^(1/k) ;
                    if newRho > rho * (1 + sqrt(eps))
                        rho = newRho ;
                        smp = paths{p}.path ;
                        restart = true ;
                        if opts.verbose > 0
                            fprintf('Better S.M.P. detected with rho = %f > %f\n', newRho, oldRho) ;
                            fprintf('Path (edges) : %s\n', num2str(smp)) ;                            
                        end
                    end
                end
            end
            if restart
                break ;
            end
        end
        
        % (3) At each node, check wheter V belongs to absco Wold or not
        ub = 0 ;
        for n = 1:nNodes
            
            % Plot
            if opts.cpoly.plot
                plotPolytope(Sys, V, W, X, n, nColsPlot, nRowsPlot)
            end
            
            % Update stuff
            if ~ isempty(V{n}) && isempty(W{n})
                W{n} = V{n} ;
                X{n} = V{n} ;
                stop(n) = false ;                
                ub = inf ;
            elseif ~ isempty(V{n}) && ~ isempty(W{n})
                [W{n}, X{n}, stop(n), normAw] = checkAndAugmentAbsco(V{n}, W{n}, opts) ;
                W{n} = unique(W{n}', 'rows')' ;
                ub = max(ub, max(normAw)) ;
            elseif isempty(V{n}) && ~ isempty(W{n})
                stop(n) = true ;
                W{n} = W{n} ;
                X{n} = [] ;
            elseif isempty(V{n}) && isempty(W{n})
                stop(n) = true ;
                W{n} = [] ;
                X{n} = [] ;
            end
            
        end
        ub = ub * rho ; % It was used to scale the V points. Need to be corrected here.
        ub = max(ub, rho) ; % Ub can drop below rho if the V are strictly inside Wold (i.e. at the last iteration)
        
        if opts.verbose > 0            
            fprintf('Bounds on CJSR : [%f %f]\n', rho, ub) ;
        end
        
        % Update info
        info.time(numel(info.time)+1) = info.time(end) + toc(initInner) ;
        info.rho(numel(info.rho)+1)   = rho ;
        
        % (4) Stop or not ?
        if all(stop)
            if opts.verbose > 0
                fprintf('Stopped at iteration %d\n', s) ;
                fprintf('The JSR is %f\n', rho) ;
            end
            restart = false ;
            break ;
        end
    end
    
    % Re-init or not ?
    if restart
        % if opts.verbose > 0
        %     fprintf('Re-initialising algorithm with better S.M.P.\n') ;
        % end
    else
        break ;
    end
    
end

% Last iteration upper bound
% Compute the final upper bound
% We can easily compute an upper-bound with the norm in W
% To do so, we compute, for each vertex v at node i, and edge (i,j)
% |Ax|_j/|x|_i where |x|_i = 1 (by definition)

% ub < rho : can happen at last iteration if all points are stricly inside polyhedron.
% In that case, re-compute ub
ub = 0 ;
for n = 1:nNodes    
    for i = 1:size(W{n},2) % # of vertices
        w = W{n}(:,i) ;
        for e = find(edges(:,1) == n)' % all (i, j, s) with i == n
            Aw = Sys.getMatProdEdge(e) * w ; % w at node i, Aw = As*w
            k = Sys.getNLabels(e) ;
            % gamma = max |Ax|_j / |x|_i using the polytope W
            nAw = polyNorm(W{edges(e,2)}, Aw, opts) ;
            nw =  1 ;
            if isempty(nAw)
                ub = inf ;
            else
                ub = max(ub, (nAw/nw)^(1/k)) ;
            end                             
        end                        
    end       
end

% Output bounds
bounds = [rho ub] ;
if opts.verbose > 0
    fprintf('Final bounds : [%f %f]\n', rho, ub) ;
end

end

function plotPolytope(Sys, V, W, X, n, nColsPlot, nRowsPlot)
warning('off','all');
subplot(nColsPlot, nRowsPlot, n) ;
if Sys.getMatSize() == 2
    if ~ isempty(W{n})
        plot(W{n}(1, :), W{n}(2, :), 'or') ;
        hold on ;
        plot(-W{n}(1, :), -W{n}(2, :), 'ok') ;
    end
    hold on ;
    if ~ isempty(X{n})
        plot(X{n}(1, :), X{n}(2, :), 'ob') ;
    end
    if ~ isempty(V{n})
        plot(V{n}(1, :), V{n}(2, :), 'og') ;
    end
    plot(0, 0, 'o', 'Color', [1 1 1]) ;
elseif Sys.getMatSize() == 3
    if ~ isempty(W{n})
        plot3(W{n}(1, :), W{n}(2, :), W{n}(3, :), 'or', 'MarkerSize', 10) ;
    end
    hold on ;
    if ~ isempty(X{n})
        plot3(X{n}(1, :), X{n}(2, :), X{n}(3, :), 'ob', 'MarkerSize', 10) ;
    end
    if ~ isempty(V{n})
        plot3(V{n}(1, :), V{n}(2, :), V{n}(3, :), 'og', 'MarkerSize', 10) ;
    end
    plot3(0, 0, 0, 'o', 'Color', [1 1 1]) ;
end
axis equal ;
title(sprintf('Node %d | W = red, X = blue, V = green', n)) ;
hold off ;
pause(0.1) ;
warning('on','all');
end

% paths{p}.path (in edge space)
% paths{p}.prod
function [paths, nodeEndOfPaths] = trimpaths(paths, nodeEndOfPaths)
npaths = numel(paths) ;
maxLength = -1 ;
for p = 1:npaths
    maxLength = max(maxLength, numel(paths{p}.path)) ;
end
P = zeros(npaths, maxLength) ;
for p = 1:npaths
    P(p,1:numel(paths{p}.path)) = paths{p}.path ;
end
[~, I] = unique(P, 'rows') ;
newpaths = cell(numel(I), 1) ;
nodeEndOfPaths = nodeEndOfPaths(I,:) ;
for i = 1:numel(I)
    newpaths{i} = paths{I(i)} ;
end
paths = newpaths ;
end