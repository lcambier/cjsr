function [lb, smp, seq, info] = cjsrBuildSequence(Sys, opts, Qdual, Qprimal)
% CJSRBUILDSEQUENCE Producing lower bounds and s.m.p. candidates
%
%   [lb, smp, seq, info] = Sys.BUILDSEQUENCE()
%   [lb, smp, seq, info] = Sys.BUILDSEQUENCE(opts)
%   [lb, smp, seq, info] = Sys.BUILDSEQUENCE(opts, Qdual)
%   [lb, smp, seq, info] = Sys.BUILDSEQUENCE(opts, Qdual, Qprimal)
%   returns
%   - a lower bound lb on the CJSR,
%   - a path of length k such that the kth root of the product
%     of the matrices of the path is equal to the lower bound lb
%   - a truncated sequence from which the path is extracted
%
%   There is a guarantee on the asymptotic growth rate of the
%   sequence. If the sequence is periodic and at least one period
%   is contained on its truncation, the lower bound is equal to
%   this asymptotic growth rate and hence it has the same guarantees.
%   The sequence is built using the dual of the LMI used by
%   cjsrQuadMultinorm. See [1] for more details.
%
%   Note: it currently only works if each edge has the same number of
%   labels.
%
%   If Qdual is provided, it is used to generate the sequence.
%   Otherwise it is recomputed by cjsrQuadMultinorm using the options
%   of the structure opts..
%   The structure opts (optionnal) can contain several fields.
%     -  opts.verbose (1) the verbose level. 0 means silent, 1 means
%        some informations printed.
%     -  opts.seq.v_0 (-1) last node of the sequence. The sequence is
%        built in the reverse order so the initial node is the last
%        node of the sequence. By default it is picked at random.
%     -  opts.seq.p_0 ('random') initial polynomial. By default it is
%        picked at random in the interior of the SOS cone. If it is
%        equal to 'primal' then the primal variable of the initial
%        node is used.
%     -  opts.seq.trunc (42) length of the truncated sequence.
%     -  opts.seq.l (1) the horizon of the algorithm building the
%        sequence; see [1].
%
%   The info structure contains informations about the algorithm.
%     - timeBuilding : the time spent building the truncated sequence
%     - timeExtracting : the time spent extracting a s.m.p. candidate
%       and the corresponding lower bound from the truncated sequence
%
%   References :
%   [1] Legat, B., Jungers, R. M., Parrilo, P. A. (2015)
%       Generating unstable trajectories for Switched Systems via Dual
%       Sum-Of-Squares techniques. Submitted Hybrid Systems: Computation and Control.
%
%   See also CJSRQUADMULTINORM, CJSRCPLXPOLYTOPEMULTINORM.


if nargin < 2
    opts = cjsrSettings() ;
end
if nargin < 3 || (nargin < 4 && strcmp(opts.seq.p_0, 'primal'))
    [~, Qprimal, Qdual, ~] = cjsrQuadMultinorm(Sys, opts) ;
end

nNodes = Sys.getNNodes() ;
nEdges = Sys.getNEdges() ;
edges = Sys.edges ;

initBuilding = tic ;

l = opts.seq.l ;
niter = opts.seq.trunc - mod(opts.seq.trunc, l) ;

if opts.seq.v_0 > 0
  curnode = opts.seq.v_0 ;
else
  curnode = randi(nNodes) ;
end
if opts.verbose > 0
  fprintf('Initial node : %d\n', curnode) ;
end


if strcmp(opts.seq.p_0, 'primal')
  p_0 = Qprimal{curnode}
else
  p_0 = random_psd(Sys.getMatSize()) ;
end

p_k = p_0 ;
prod = eye(Sys.getMatSize()) ;
seq = zeros(1,niter) ;

for iter = 1:l:(niter-l+1)
    best = 0 ;
    best_e = -1 ;
    npaths = 0 ;

    i = l ;
    pathnodes = zeros(1,l+1) ;
    pathnodes(l+1) = curnode ;
    pathedges = zeros(1,l) ;
    pathprods = cell(1,l+1) ;
    pathprods{l+1} = eye(Sys.getMatSize()) ;
    while i <= l
        if i == 0
            npaths = npaths + 1 ;
            pathprod = pathprods{1} ;
            cur = sum(sum((pathprod * Qdual{pathedges(1)} * (pathprod')) .* p_k)) ;
            if cur > best
                best = cur ;
                best_e = pathedges ;
            end
            i = i + 1 ;
        else
            pathedges(i) = pathedges(i) + 1 ;
            if pathedges(i) > nEdges
                pathedges(i) = 0 ;
                i = i + 1 ;
            elseif edges(pathedges(i),2) == pathnodes(i+1)
                pathnodes(i) = edges(pathedges(i),1) ;
                pathprods{i} = pathprods{i+1} * Sys.getMatProdEdge(pathedges(i)) ;
                i = i - 1 ;
            end
        end
    end
    if npaths == 0
        error('%d does not have any incoming path of length %d.', curnode, l) ;
    end
    if best_e == -1
        error('Oops, this should not happend, please report this bug.') ;
    end
    curnode = edges(best_e(1), 1) ;
    seq((niter-iter-l+2):(niter-iter+1)) = best_e ;
    Medges = Sys.getMatProdEdge(best_e) ;
    p_k = (Medges') * p_k * Medges ;
    prod = prod * Medges ;
end

timeBuilding = toc(initBuilding) ;

initExtracting = tic ;

lbd = 0 ;
for i = 1:numel(seq)
    P = eye(Sys.getMatSize()) ;
    startNode = edges(seq(i),1) ;
    k = 0 ;
    for j = i:numel(seq)
        e = seq(j) ;
        k = k + Sys.getNLabels(e) ;
        P = Sys.getMatProdEdge(e) * P ;
        if edges(e,2) == startNode
            lambda = eigs(P,1) ;
            newLbd = abs(lambda)^(1/k) ;
            if newLbd > lbd * (1 + sqrt(eps))
                smp = seq(i:j) ;
                if opts.verbose > 0
                    fprintf('Better S.M.P. detected with lb = %f > %f\n', newLbd^(1/Sys.degree), lbd^(1/Sys.degree)) ;
                    fprintf('Path (edges) : %s\n', num2str(smp)) ;
                end
                lbd = newLbd ;
            end
        end
    end
end

lb = lbd^(1/Sys.degree) ;

timeExtracting = toc(initExtracting) ;

info = struct() ;
info.timeBuilding = timeBuilding ;
info.timeExtracting = timeExtracting ;

end
function [A] = random_psd(n)

Q = randn(n,n) ;

eps = 0.1 ;

A = Q' * diag(eps + abs(randn(n,1))) * Q ;

end
