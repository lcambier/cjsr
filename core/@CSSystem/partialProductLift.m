function Sout = partialProductLift(Sys, edgesToLift, direction)
% PARTIALPRODUCTLIFT Partial product lift
%
%   Sout = Sys.PARTIALPRODUCTLIFT(EDGES, 'past') returns Sout such that all the edges
%   in EDGES are replaced by all length-2 paths ending in an edge in EDGES. The
%   labels are the concatenation of the labels on the path.
%   If EDGES = [1 ... S.getNEdges], this is equivalent to TProductLift.
%
%   Sout = Sys.PARTIALPRODUCTLIFT(EDGES, 'future') does the same for path
%   beginning in an edge in edges
%   If EDGES = [1 ... S.getNEdges], this is equivalent to TProductLift.
%
%   (Very) Experimental, research oriented code.
%
%   See also TPRODUCTLIFT, MDEPENDANTLIFT, SOSLIFT, ARBITRARYLIFT,
%   PARTIALDEPENDANTLIFT


% Input checks
edgesToLift = edgesToLift(:) ;
if max(edgesToLift) > Sys.getNEdges() || min(edgesToLift) < 1
    error('One of the element of edges if greater than the number of nodes or less than 1') ;
end
if ~ (strcmp(direction,'past') || strcmp(direction,'future'))
    error('direction should be past or future') ;
end

past = strcmp(direction, 'past') ;

edges = Sys.edges ;
labels = Sys.labels ;
matrices = Sys.matrices ;
nNodes = Sys.getNNodes() ;
nEdges = Sys.getNEdges() ;

newEdges = [] ;
newLabels = {} ;
id = 0 ;

for e = edgesToLift'
    edge = edges(e, :) ;
    label = labels{e} ;
    % Look for 2-path
    if past
        idBefore   = edges(:, 2) == edge(1) ;
        nodeBefore = edges(idBefore, 1) ;
        idBefore   = find(idBefore) ;
        nBefore    = numel(idBefore) ;
        newEdges(id+1:id+nBefore,:) = [nodeBefore edge(2)*ones(size(nodeBefore))] ;
        for l = 1:nBefore
            newLabels{id+l,1} = [labels{idBefore(l)} label] ;
        end
        id = id + nBefore ;
    else        
        idAfter    = edge(2) == edges(:, 1) ;
        nodeAfter  = edges(idAfter, 2) ;
        idAfter    = find(idAfter) ;
        nAfter     = numel(idAfter) ;
        newEdges(id+1:id+nAfter,:) = [edge(1)*ones(size(nodeAfter))  nodeAfter] ;
        for l = 1:nAfter
            newLabels{id+l,1} = [label labels{idAfter(l)}] ;
        end
        id = id + nAfter ;
    end
end

edges(edgesToLift,:) = [] ;
newEdges = [edges ; newEdges] ;
labels(edgesToLift,:) = [] ;
nLabels = numel(labels) ;
for l = 1:numel(newLabels)
    labels{nLabels+l,1} = newLabels{l,1} ;
end
newLabels = labels ;

Sout = CSSystem(newEdges, matrices, newLabels) ;
