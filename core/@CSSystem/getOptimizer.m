function [primal, dual] = getOptimizer(Sys, opts)
% GETOPTIMIZER LMI CJSR problem
%
% Builds the LMI's (as Yalmip's optimizer object) used to compute the
% Lyapunov quadratic norm.
%
% The primal is the following problem
% 
% P(gamma) = min 0
%             Qi > 0                                        for all i in Nodes (SDP sense)
%            s.t. Ms' Qj Ms <= gamma^(2 nLabels(k)) Qi      for all (i, j, k) in Edges (SDP sense). 
%
% This is an internal private function
% Relevant when using cjsrQuadMultinorn with option 'quad.modeling' =
% 'yalmip'.

if nargin == 1
    opts = cjsrSettings() ;
end

cntrprimal = [] ;
Qprimal = cell(Sys.getNNodes(), 1) ;
s = Sys.getMatSize() ;
gamma = sdpvar(1, 1, 'full') ;

% Primal Strict positivity constraint
for n = 1:Sys.getNNodes()
    Qprimal{n} = sdpvar(s, s) ;
    cntrprimal = [cntrprimal ; Qprimal{n} >= eye(s)] ;
end
    
% Norm constraint
for e = 1:Sys.getNEdges()
    edge = Sys.getEdges(e) ;
    cntr_ijs = - Sys.getMatProdEdge(e)' * Qprimal{edge(2)} * Sys.getMatProdEdge(e) + (gamma^(2*Sys.getNLabels(e))) * Qprimal{edge(1)} >= 0 ;
    cntrprimal = [cntrprimal ; cntr_ijs] ;
end

% Build primal
primal = optimizer(cntrprimal, [], opts.quad.yalmip, gamma, Qprimal) ;


mbcntr_lhs = cell(Sys.getNNodes(),1) ;
cntrdual   = [] ;
Qdual      = cell(Sys.getNEdges(), 1) ;

% Dual positivity constraint
for e = 1:Sys.getNEdges()
    Qdual{e} = sdpvar(s, s) ;
    cntrdual = [cntrdual ; Qdual{e} >= 0] ;
    edge = Sys.getEdges(e) ;
    if isempty(mbcntr_lhs{edge(1)})
        mbcntr_lhs{edge(1)} = -gamma^(2*Sys.getNLabels(e)) * Qdual{e} ;
    else
        mbcntr_lhs{edge(1)} = mbcntr_lhs{edge(1)} - gamma^(2*Sys.getNLabels(e)) * Qdual{e} ;
    end
    if isempty(mbcntr_lhs{edge(2)})
        mbcntr_lhs{edge(2)} = Sys.getMatProdEdge(e) * Qdual{e} * Sys.getMatProdEdge(e)' ;
    else
        mbcntr_lhs{edge(2)} = mbcntr_lhs{edge(2)} + Sys.getMatProdEdge(e) * Qdual{e} * Sys.getMatProdEdge(e)' ;
    end
    if e == 1
        sumtraces = trace(Qdual{e}) ;
    else
        sumtraces = sumtraces + trace(Qdual{e}) ;
    end
end

% Mass Balance constraint
for n = 1:Sys.getNNodes()
    cntrdual = [cntrdual ; mbcntr_lhs{n} >= 0] ;
end

cntrdual = [cntrdual ; sumtraces == 1] ;

% Build dual
dual = optimizer(cntrdual, [], opts.quad.yalmip, gamma, Qdual) ;

end
