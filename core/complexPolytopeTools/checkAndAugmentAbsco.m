function [W, X, stop, VnormWold] = checkAndAugmentAbsco(V, Wold, opts)
% CHECKANDAUGMENTABSCO Absolute convex hull construction
%
%   [W, X, STOP, VNORMWOLD] = checkAndAugmentAbsco(V, WOLD) check wether each column
%   vector of V belong to the absolute convex hull (absco) of the set of
%   column vector in WOLD and return the answer in STOP. STOP is then true
%   if all columns of V belong to the absco(WOLD) and false otherwise.
%   VNORMWOLD is the norm of each vector of V using WOLD as the complex
%   polytope.
%   [W, X, STOP, VNORMWOLD] = checkAndAugmentAbsco(V, WOLD, OPTS) uses the opts
%   structure to pass options to the solver. Type
%   help checkBelongingRobust 
%   for the options' meaning.
% 
%   For each column i that belong to absco(WOLD), STOP(i) is set to true
%   Otherwise, STOP(i) is set to false.
% 
%   A vector x belongs to the absco of an essential set of vectors 
%   Wold = {w1,...,wn} if there exist lambda1...lambdan complex such that
%       sum_i lambdai wi = x
%       sum_i |lambdai| <= 1
%       lambdai is complex.
%
%   W is the new essential set of vector of absco(WOLD Union V). It is the
%   minimal set of vectors that is used to describe absco(WOLD Union V).
%   X is the intersection of W and Wold.

% Checks
d = size(V, 1) ;
nPts = size(V, 2) ;
if size(Wold, 1) ~= d
    error('Dimensions don''t match') ;
end

%% Check and solve
W = [] ;
stop = true ;
VnormWold = zeros(nPts, 1) ;
% Add the points
for i = 1:nPts                    
    v = V(:, i) ; 
    [belong, normWv] = checkBelonging(Wold, v, opts) ;
    if isempty(normWv)
        VnormWold(i) = inf ;
    else
        VnormWold(i) = normWv ;
    end
    if belong
        % Do nothing : point is in polyhedron
    else 
        % Point is not : add it
        stop = false ;
        W = [W v] ;
    end    
end
% We merge W and Wold
W = [W Wold] ;
% We trim W
i = 1 ;
while i <= size(W, 2) % While there are points in W that haven't been checked
    Wtemp = W(:,[1:i-1 i+1:end]) ;
    wi = W(:,i) ;
    if checkBelonging(Wtemp, wi, opts) 
        % The point can be expressed as a convex combi of the others
        % Remove it
        W = Wtemp ;
        % Do not update i since W was "column-shrinked"
    else
        % Move forward
        i = i+1 ;
    end            
end

% We return X, the intersection of W (new basis) and V (old points)
% And W
X = intersect(W', V', 'rows')' ;

end

function [belongs, normWv] = checkBelonging(W, v, opts)
    normWv = polyNorm(W, v, opts) ;
    belongs = ~ (isempty(normWv) || normWv > 1+opts.cpoly.reltol) ;   
end










