function diag = trajectoryCollapses(Sys, W, rho)
% TRAJECTORYCOLLAPSES Checks if trajectories collapse to a non-trivial
% subspace at a node.
%
%   DIAG = TRAJECTORYCOLLAPSES(S, W, RHO) check wether system S,
%   given an initial set of vertices W and a scaling factor RHO, 
%   has a node where all trajectories eventually collapse to an invariant
%   non-trivial subspace. This is very important for
%   cjsrCplxPolytopeMultinorm, since this would mean that the polytope lives
%   in a subspace of R^n, and thus its volume (in R^n) is 0. Checking if a
%   point belongs to it then becomes difficult.
%   DIAG is true if there is a problem, and flase otherwise.

nNodes = Sys.getNNodes() ;
matSize = Sys.getMatSize() ;
rankW = zeros(nNodes,1) ;
for n = 1:nNodes
    rankW(n) = rank(W{n}) ;
end

for iter = 1:(nNodes*matSize)
    Wnew = propagateVertices(Sys, W, rho) ;
    % Add vectors that increase the rank
    rankIncreased = zeros(nNodes,1) ;    
    for n = 1:nNodes        
        for i = 1:size(Wnew{n},2) 
            Wnewnew = [W{n} Wnew{n}(:,i)] ;
            if rank(Wnewnew) > rankW(n)
                % it increases the rank 
                W{n} = Wnewnew ;
                rankW(n) = rankW(n)+1 ;
                rankIncreased(n) = true ;
            else
                % skip vector
            end              
        end
    end
    % If at least one mat did not increased : returns false
    if ~ any(rankIncreased)        
        diag = true ; % We have a problem        
        return ;
    end
    if all(rankW == matSize)
        diag = false ; % We are good!
        return ;
    end
end

diag = true; % The algorithm might not a have reached every node.

end