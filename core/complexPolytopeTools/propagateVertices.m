function V = propagateVertices(Sys, X, rho)
% PROPAGATEVERTICES Propagate a set of vertices through graph
%
%   V = PROPAGATEVERTICES(S, X, RHO) propagates the set of vertices in X (a
%   cell array of matrices) using the CSSystem S, whose matrices are scaled by RHO.
%
%   For each edge, we have in Medge*X{edge(1)}/rho as part of the columns
%   of V{edge(2)}, with Medge the matrix on the edge.

edges = Sys.getEdges() ;
nEdges = Sys.getNEdges() ;
V = cell(Sys.getNNodes(), 1) ;
for e = 1:nEdges
    edge = edges(e,:) ;
    Medge = Sys.getMatProdEdge(e) ;
    % Propagate vertices
    if ~ isempty(X{edge(1)})
        if rho == 0 % No scaling
            V{edge(2)} = [V{edge(2)} Medge*X{edge(1)}] ;
        else
            k = Sys.getNLabels(e) ;
            V{edge(2)} = [V{edge(2)} Medge*X{edge(1)}/(rho^k)] ;
        end
    end
end
end