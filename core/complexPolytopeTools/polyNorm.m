function normv = polyNorm(W, v, opts)
% POLYNORM Complex Polytope Norm of a vector
%
%   NORMV = POLYNORM(W, V) returns the norm of vector V, NORMV, defined as
%       norm_W(v) = inf(rho > 0 | v \in absco(rho * W))
%   where absco(W) is defined as the absolute convex hull of the set of
%   vectors W (each column of W is a vector).
%       absco(W) = { w | there exist lambda_1 ... lambda_n complex such
%       that \sum lambda_i W(:,i) = w and \sum |lambda_i| <= 1 }.
%   with n the number of vectors in W (i.e. the number of columns)
%   NORMV = POLYNORM(W, V, OPTS) does the same, but solves the optimization
%   problem using the options in OPTS :
%     - opts.cpoly.solver ('sedumi') : either 'sedumi' or 'sdpt3', used to
%       solve the SOCP in the norm computation.
%     - opts.sedumi and opts.sdpt3 are used to pass options to the solver.
%
%   See also CHECKANDAUGMENTABSCO


    if nargin == 2
        opts = cjsrSettings ;
    end

    if size(v, 2) ~= 1        
        error('v should be a vector') ;
    end
    if size(v, 1) ~= size(W, 1)
        error('W and v should have the same (1st) dimension') ;
    end    
    
    %% Tolerance for preprocess
    tol = opts.cpoly.reltol ;

    %% Modeling
    % This formulation is the formulation of the alternative problem
    %
    %   min_(rho, z) rho >= |w_i* z|
    %                1 = Re(v* z)
    %
    %   where z is a dx1 complex vector
    %         rho is a positive scalar
    %
    %   And the resulting norm of v is 1/rho
    
    d = size(W, 1) ;
    n = size(W, 2) ;
    
    if n == 0
        normv = [] ;
        return ;
    end
    
    A = zeros(2*n+1+n,2*d+1+3*n) ;
    b = zeros(2*n+1+n,1) ;
    c = zeros(2*d+1+3*n,1) ;    
    
    A(1:n           ,1:d)     =   real(W)' ;
    A(n+1:2*n       ,1:d)     = - imag(W)' ;
    A(1:n           ,d+1:2*d) =   imag(W)' ;
    A(n+1:2*n       ,d+1:2*d) =   real(W)' ;
    A(2*n+1         ,1:2*d)   = - [real(v)' imag(v)'] ;
    A(2*n+2:2*n+1+n ,2*d+1)   = 1 ;
    for i = 1:n
        col = 2*d+1+(i-1)*3 ;        
        A(i       ,col+2) = -1 ;
        A(n+i     ,col+3) = -1 ;
        A(2*n+1+i ,col+1) = -1 ;
    end    
    b(2*n+1,1) = -1 ;
    c(2*d+1,1) =  1 ;
    K.f = 2*d ;
    K.l = 1   ;
    if n > 0
        K.q = 3*ones(n,1) ;
    end
                
    % Call solver
    if strcmp(opts.cpoly.solver,'sedumi')
        solOpts = opts.sedumi ;            
    elseif strcmp(opts.cpoly.solver,'sdpt3')
        solOpts = opts.sdpt3 ;
    end
    options.solver = opts.cpoly.solver ;
    options.solverOptions = solOpts ;
    reltol = tol ;
    
    [X,Y,feas,stopFlag,obj] = solveSemiDefiniteProgram(A,b,c,K,reltol,options) ;    
    if ~ feas || obj < tol
        normv = [] ;
    else
        if isempty(obj) 
            normv = [] ;        
        else        
            normv = (obj)^(-1) ;        
        end
    end
end