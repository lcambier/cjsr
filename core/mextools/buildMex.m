% This script builds the mex files
% 1) First, move to this directory
% 2) If not done yet, you should call
% mex -setup to setup-up the mex compiler
% If you don't have one, you will need to download and install a supported
% compiler. See http://nl.mathworks.com/support/compilers/R2015a/index.html
% for more informations.
% 3) Then, run the following

fprintf('Compiling mex files...\n') ;
cd ..
% FlattendAndConcat
cd utils 
mex flattenAndConcat.c
cd ..

% Sos function 
cd sosTools
mex permanent.c
mex convertMultiset.c
cd ..

% Concat 
cd concatTools
mex concat.c
cd ..

% Return
cd mextools
fprintf('Compiling mex files : done\n') ;

