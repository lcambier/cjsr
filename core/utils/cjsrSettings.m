function opts = cjsrSettings(varargin)
% CJSRSETTINGS Constrainted system options
%
%   OPTS = CJSRSETTINGS() returns default option structure for the algorithms. 
%   OPTS = CJSRSETTINGS(key1, value1, key2, value2, ...) returns the
%   default.
%   option structure where the field described by key have been overriden 
%   with value.
%
%   The following keys are available (default values) :
%
%   verbose (1) : 0 or 1, the verbose level.
%   sedumi (see in-code for default value) : the options for the sedumi
%   solver.
%   sdpt3 (see in-code for default value) : the options for the sdpt3
%   solver.
%
%   --- Options related to CSSystem.cjsrQuadMultinorm ---
%
%   quad.tol (1e-5) : the tolerance for the bissection tolerance.
%   quad.bounds ([]) : the initial upper and lower bounds for the
%   bissection algorithm.
%   quad.modeling ('sedumi') : the modeling (matrix format) to be used in the bissection.
%     Can be either 'sedumi' or 'yalmip'. 
%     If sedumi is used, matrix representing the problem in sedumi format are 
%     built once and for all. 
%     If yalmip is used, the optimizer function is used to compile the
%     problem once and for all at the beginning.
%   quad.solver ('sedumi') : the solver to be used in the bissection if
%     modeling is set on 'sedumi'.
%   quad.yalmip (sdpsettings) : the options to be used when the 'yalmip'
%     modeling is used.
%   quad.reltol (1e-7) : the relative tolerance of the SDP solver when
%     quad.modeling is set on 'sedumi'.
%
%   --- Options related to CSSystem.cjsrCplxPolytopeMultinorm ---
%
%   cpoly.checkNewCycle (true) : if true, new cycles are checked in the
%     complex polytope algorithm.
%   cpoly.maxPathsLength (10) : the maximum length of the path stored in 
%     order to find better S.M.P. in the complex polytope algorithm.
%   cpoly.plot (false) : if true, the complex polytope algorithm plots the
%     polytope (in 2D, and only the real part) from iteration to iteration
%     while the algorithm is running.
%   cpoly.solver ('sedumi') : the solver to be used in the complex polytope
%     method, either 'sedumi' or 'sdpt3'.
%   cpoly.maxiter (25) : the max number of iterations of the complex
%     polytope algorithm.
%   cpoly.reltol (1e-7) : the relative tolerance of the solver.
%
%   --- Options related to CSSystem.cjsrBuildSequence ---
%
%     some informations printed.
%   opts.seq.v_0 (-1) last node of the sequence. The sequence is
%     built in the reverse order so the initial node is the last
%     node of the sequence. By default it is picked at random.
%   opts.seq.p_0 ('random') initial polynomial. By default it is
%     picked at random in the interior of the SOS cone. If it is
%     equal to 'primal' then the primal variable of the initial
%     node is used.
%   opts.seq.trunc (42) length of the truncated sequence.
%   opts.seq.l (1) the horizon of the algorithm building the
%     sequence.
%
%   --- Options related to the drawing of graphs---
%
%   dot.edgeNumbering (false) : add the edge number when exporting to dot
%   dot.nodeNumbering (false) : add the node number when exporting to dot
%   dot.displayLevel (0) : when set to 1, display the atomistic matrices
%     next to the graph. When set to 2, display all the products authorized
%     by the labels.
%   dot.dotFile ('') : when set to a string, save the intermediate .dot to
%     this file. If set to '', the dot file will be deleted at the end.
%
%   See also CSSYSTEM

% (Ripped from sdpsettings.m by Johan L�fberg and jsrsettings.m from the JSR toolbox)

% Print out possible values of properties.
if (nargin == 0) && (nargout == 0)
    help cjsrSettings
    return;
end


Names = {
    % General    
    'verbose'
    'sedumi'
    'sdpt3'
    
    % quad
    'quad.tol'
    'quad.bounds' 
    'quad.solver'
    'quad.modeling'
    'quad.yalmip'
    'quad.reltol'
    
    % cpoly
    'cpoly.checkNewCycle'
    'cpoly.maxPathsLength'
    'cpoly.plot'
    'cpoly.solver'
    'cpoly.maxiter' 
    'cpoly.reltol'
    
    % seq
    'seq.v_0'
    'seq.p_0'
    'seq.trunc'
    'seq.l'

    % dot
    'dot.edgeNumbering'
    'dot.nodeNumbering'
    'dot.displayLevel' 
    'dot.dotFile' 
};


obsoletenames ={ % Use when options have become obsolete
};

[m,n] = size(Names);
names = lower(Names);

if (nargin>0) & isstruct(varargin{1})
    opts = varargin{1};
    paramstart = 2;
else
    paramstart = 1;
    
    % General    
    opts.verbose = 1 ;
    
    % Yalmip default options
    opts.sedumi = sedumiDefaultParams() ;      
    opts.sdpt3 = sdpt3DefaultParams() ;
    
    % Quad 
    opts.quad.tol = 1e-5 ;
    opts.quad.bounds = [] ; 
    opts.quad.solver = 'sedumi' ;
    opts.quad.modeling = 'sedumi' ;
    opts.quad.yalmip = [] ; % sdpsettings('verbose',1,'solver','sdpt3') ; for standard yalmip opts stucture
    opts.quad.reltol = 1e-7 ;
    
    % Cpoly
    opts.cpoly.checkNewCycle = true ;
    opts.cpoly.maxPathsLength = 10 ;
    opts.cpoly.plot = false ;
    opts.cpoly.solver = 'sedumi' ;    
    opts.cpoly.maxiter = 25 ;
    opts.cpoly.reltol = 1e-7 ;

    % Seq
    opts.seq.v_0 = -1 ;
    opts.seq.p_0 = 'random' ;
    opts.seq.trunc = 42 ;
    opts.seq.l = 1 ;

    
    % Dot
    opts.dot.edgeNumbering = false ;
    opts.dot.nodeNumbering = false ;
    opts.dot.displayLevel = 0 ;
    opts.dot.dotFile = '' ;
    
end

i = paramstart;
% A finite state machine to parse name-value pairs.
if rem(nargin-i+1,2) ~= 0
    error('Arguments must occur in name-value pairs.');
end
expectval = 0;                          % start expecting a name, not a value
while i <= nargin
    arg = varargin{i};
    
    if ~expectval
        if ~ischar(arg)
            error(sprintf('Expected argument %d to be a string property name.', i));
        end
        
        lowArg = lower(arg);
        
        j_old = strmatch(lowArg,obsoletenames);
        if ~isempty(j_old)
            % For compability... No need yet
        end
        
        j = strmatch(lowArg,names);
        if isempty(j)                       % if no matches
            error(sprintf('Unrecognized property name ''%s''.', arg));
        elseif length(j) > 1                % if more than one match
            % Check for any exact matches (in case any names are subsets of others)
            k = strmatch(lowArg,names,'exact');
            if length(k) == 1
                j = k;
            else
                msg = sprintf('Ambiguous property name ''%s'' ', arg);
                msg = [msg '(' deblank(Names{j(1)})];
                for k = j(2:length(j))'
                    msg = [msg ', ' deblank(Names{k})];
                end
                msg = sprintf('%s).', msg);
                error(msg);
            end
        end
        expectval = 1;                      % we expect a value next
    else
        eval(['opts.' Names{j} '= arg;']);
        expectval = 0;
    end
    i = i + 1;
end

if expectval
    error(sprintf('Expected value for property ''%s''.', arg));
end

if strcmp(opts.quad.modeling,'yalmip')        
    if isempty(opts.quad.yalmip)
        warning('You used opts.quad.modeling = ''yalmip'' without setting the opts.quad.yalmip field. A default value has been set.') ;    
        opts.quad.yalmip = sdpsettings('verbose',opts.verbose,'solver','sedumi') ;
    end
end

end

% Copied from the sdpsettings() function of the Yalmip toolbox
function sedumi = sedumiDefaultParams() 
sedumi.alg    = 2;
sedumi.beta   = 0.5;
sedumi.theta  = 0.25;
sedumi.free   = 1;
sedumi.sdp    = 0;
sedumi.stepdif= 0;
sedumi.w      = [1 1];
sedumi.mu     = 1.0;
sedumi.eps    = 1e-9;
sedumi.bigeps = 1e-3;
sedumi.maxiter= 150;
sedumi.vplot  = 0;
sedumi.stopat     = -1;
sedumi.denq   = 0.75;
sedumi.denf   = 10;
sedumi.numtol = 5e-7;
sedumi.bignumtol = 0.9;
sedumi.numlvlv = 0;
sedumi.chol.skip = 1;
sedumi.chol.canceltol = 1e-12;
sedumi.chol.maxu   = 5e5;
sedumi.chol.abstol = 1e-20;
sedumi.chol.maxuden= 5e2;
sedumi.cg.maxiter = 25;
sedumi.cg.restol  = 5e-3;
sedumi.cg.refine  = 1;
sedumi.cg.stagtol = 5e-14;
sedumi.cg.qprec   = 0;
sedumi.maxradius = inf;
end

function sdpt3 = sdpt3DefaultParams() 
sdpt3.vers     = 1;
sdpt3.gam      = 0;
sdpt3.predcorr = 1;
sdpt3.expon    = 1;
sdpt3.gaptol   = 1e-7;
sdpt3.inftol   = 1e-7;
sdpt3.steptol  = 1e-6;
sdpt3.maxit    = 50;
sdpt3.stoplevel= 1;
sdpt3.sw2PC_tol  = inf;
sdpt3.use_corrprim  = 0;
sdpt3.printyes   = 1;
sdpt3.scale_data = 0;
sdpt3.schurfun   = [];
sdpt3.schurfun_parms = [];
sdpt3.randnstate = 0;
sdpt3.spdensity   = 0.5;
sdpt3.rmdepconstr = 0;
sdpt3.CACHE_SIZE = 256;
sdpt3.LOOP_LEVEL = 8;
sdpt3.cachesize = 256;
sdpt3.linsys_options = 'raugmatsys';
sdpt3.smallblkdim = 30;
end


