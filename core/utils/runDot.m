function err = runDot(file, output, exts)
% RUNDOT Runs the dot software from command line
%
%   ERR = RUNDOT(FILE, OUTPUT, EXTS) runs the dot software from the command
%   line on the .dot file FILE, and output the files "OUTPUT.EXTS{1}",
%   "OUTPUT.EXTS{2}", etc. If everything went well, returns ERR = false.
%   Otherwise, returns ERR = true.
%   
%   Example: e = rundot('mydot.dot', 'myoutput', {'png', 'svg'});
%   creates two files: myoutput.png and myoutput.svg.
% 
%   To use this function, the "dot" software needs to be installed. One way
%   to do it is to install graphviz (http://www.graphviz.org).

err = false ;

for i = 1:numel(exts)
    ext = exts{i} ;
    try
        pathDot = loadPathDot() ;
        if ispc % Windows version
            s = dos(sprintf('"%s" -T%s %s -o %s.%s', pathDot, ext, file, output, ext)) ;
            if s == 1
                warning('Error executing dos command. Maybe wrong location ?') ;
                err = true ;
            end
        elseif isunix % Unix (mac or linux)
            s = unix(sprintf('"%s" -T%s %s -o %s.%s', pathDot, ext, file, output, ext)) ;
            if s == 1
                warning('Error executing unix command. Maybe wrong location ?') ;
                err = true ;
            end
        else
            warning('Your platform does not seem to be supported.') ;
            err = true ;
        end
    catch e
        disp(e)
        err = true ;        
        fullPath = getFullPath() ;        
        warning('The dot software does not seem to be installed or the wrong path was provided. Please delete the file located at %s', fullPath) ;
    end    
end

end