function fullPath = getFullPath()
% GETFULLPATH Path towards the pathDot file
%
%   FULLPATH = GETFULLPATH() returns the full path FULLPATH towards the 
%   pathDot file

folder = fileparts(which('loadPathDot.m')) ;
fullPath = fullfile(folder, 'pathDot') ;

end