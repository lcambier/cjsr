/** FLATTENANDCONCAT Flatten a cell array
 *
 * C = flattenAndConcat(Cc) concatenates every array on in to build one large
 *   vector
 *
 * Example 
 * C = {[1 2 3],[1 2 3]',[6 5 4 ; 3 2 1]}
 * flattenAndConcat(C)
 *   ans =
 *       1
 *       2
 *       3
 *       1
 *       2
 *       3
 *       6
 *       3
 *       5
 *       2
 *       4
 *       1
 *
 * This code is equivalent to
 * ----------------------------------------------
 * function v = flattendAndConcat(C)
 * if ~ iscell(C)
 *      error('C should be a cell of matrices') ;
 * end
 * v = [] ;
 * for i = 1:numel(C)
 *      c = C{i} ;
 *      v = [v ; c(:)] ;
 * end
 * end
 * ----------------------------------------------
 *
 * with this kind of example
 * C = cell(50, 1) ;
 * for j = 1:numel(C)
 *     C{j} = rand(randi(10), randi(10)) ;
 * end
 * we gain approximately a factor 10-15
 *
 * Compile with
 * mex flattenAndConcat.c
 *
 */

#include <math.h>
#include <matrix.h>
#include <mex.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    mwSize numel, size, sizeTemp ;
    mwIndex i, j ;
    mxArray * cell ;
    double * data ;
    double * dataIn ;
    
    
    /* Some checks */
    if (nlhs != 1 || nrhs != 1)
        mexErrMsgTxt("Needs 1 input and 1 output") ;
    
    if (! mxIsCell(prhs[0]))
        mexErrMsgTxt("Inputs needs to be cell array type") ;
    
    numel = mxGetNumberOfElements(prhs[0]) ;
    
    /* Allocate outputs */
    /* First, get the size */
    size = 0 ;
    for(i = 0 ; i < numel ; i++) {
        cell = mxGetCell(prhs[0], i) ;
        if(cell) {
            if (! mxIsDouble(cell))
                    mexErrMsgTxt("Content of cell array need to be double arrays") ;        
            size += mxGetNumberOfElements(cell) ;
        }
    }
    
    /* Create output */
    plhs[0] = mxCreateDoubleMatrix(size, 1, mxREAL) ;
    data = mxGetPr(plhs[0]) ;
   
    /* Fill output and compute factorial */
    sizeTemp = 0 ;
    for(i = 0 ; i < numel ; i++) {
        cell = mxGetCell(prhs[0], i) ;
        if (cell) {
            dataIn = mxGetPr(cell) ;
            for(j = 0 ; j < mxGetNumberOfElements(cell) ; j++) {
                data[sizeTemp++] = dataIn[j] ;
            }
        }
    }
}

