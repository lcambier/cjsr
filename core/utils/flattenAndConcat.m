function v = flattenAndConcat(C)
% FLATTENANDCONCAT Flatten a cell array
%
%   C = flattenAndConcat(CC) concatenates every array in the cell array CC to
%   build one large flat array C.

if ~ iscell(C)
    error('C should be a cell of matrices') ;
end
v = [] ;
for i = 1:numel(C)
    c = C{i} ;
    v = [v ; c(:)] ;
end

end

