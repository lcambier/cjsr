function [path] = loadPathDot()
% LOADPATHDOT Prepare dot software to be run within the toolbox
%
%   PATH = LOADPATHDOT() returns the PATH to the dot binary. It first tries 
%   to detect it automatically using some default locations. If it
%   succeeds, it stores the path in a pathDot file (next to this function).
%   If it fails, it asks the used to enter the path manually, either using
%   a dialog box (on Windows and Linux) or the command line interface (on
%   Mac OSX).

fullPath = getFullPath() ;
   
if exist(fullPath,'file') == 2 % The file exist : load the valud inside it
    fid = fopen(fullPath, 'r') ;
    if fid == -1
        error('Cannot open file storing the path to the dot program. Permission error ?') ;
    end
    path = fgetl(fid) ;
    fclose(fid) ;
else
    fprintf('It seems to be the first time you use the view() command\n') ;
    % Check for standard locations
    path = [] ;
    if ispc 
        % Installation though installer end up in C:\Program Files
        % (x86)\GraphViz ...
        results = dir('C:\Program Files (x86)\*raph*iz*') ; % handle extensions and version numbering
        for i = 1:numel(results)
            result = results(i) ;
            path = ['C:\Program Files (x86)\' result.name '\bin\dot.exe'] ;
            if exist(path,'file') == 2 % dot found
                break ;
            end
        end                        
    elseif ismac
        % A few path to try
        paths = {'/usr/bin/dot','/usr/local/bin/dot'} ;        
        for i = 1:numel(paths)
            path = paths{i} ;
            if exist(path,'file') == 2 % dot found
                break ;
            end
        end                
    elseif isunix        
        % Check if dot command works
        [~,cmdout] = unix('dot -V') ;
        found = strfind(cmdout,'graphviz') ;
        if isempty(found) % dot command does not work :'(
            path = [] ;
        else            
            path = 'dot' ; 
        end        
    else
        error('Your platform does not seem to be supported') ;
    end
        
    if (~ isempty(path) && exist(path,'file') == 2) || (strcmp(path,'dot') && isunix && ~ismac) % valid dot location
        fprintf('Dot has been automatically detected\nand set to %s.\n',path) ;
        fprintf('It should be working.\n') ;
        fprintf('No further actions are required.\n') ;
    else
        fprintf('CSSystem was not able to locate dot.\n') ;
        fprintf('You need to provided the complete path to the dot binary\n') ;
        fprintf('If you don''t have it, you can download it through graphviz (graphviz.org)\n') ;
        if ismac
            path = '' ;
            while exist(path,'file') ~= 2
                path = input('Please enter the full path to the dot binary (ex: /usr/local/bin/dot) :\n','s') ;
                if exist(path,'file') ~= 2
                    fprintf('The file does not seem to exist. Please enter a valid filename.\n') ;
                end
            end
        else
            if ispc
                [filename, pathname, ~] = uigetfile('.exe','Please select the dot.exe file, located in .../GraphViz/bin/dot.exe') ;
            elseif isunix && ~ ismac
                [filename, pathname, ~] = uigetfile('*','Please select the dot binary file') ;
            else
                error('Your platform does not seem to be supported') ;    
            end
            if ~ ischar(filename) && ~ ischar(pathname) && filename == 0 && pathname == 0
                fprintf('You should install graphviz (graphviz.org) and then select the dot binary file.\n') ;
                if isunix
                    fprintf('On Ubuntu, Graphviz can be installed and configured simply by typing\nsudo apt-get install graphviz\nin a terminal.\n') ;
                end
                fprintf('You can bypass this gui by directly creating a file pathDot (without extension) next to the loadPathDot() function containing the full path to the dot binary file (including extension if using Windows).\n') ;
                error('GrpahViz/dot not found') ;
            end    
            path = [pathname filename] ;        
        end
    end    
    fid = fopen(fullPath, 'w') ;
    if fid == -1
        error('Cannot create file containing the path to the dot program. Permission error ?') ;        
    end
    fprintf(fid, '%s', path) ;
    fclose(fid) ;
    fprintf('The dot location (%s) has been saved (at %s) and you won''t see this message again.\n', path, fullPath) ;      
end

% Check working
if exist(path,'file') ~= 2 % Path does not exist
    if isunix && ~ismac
        [~,cmdout] = unix('%s -V',path) ;
        found = strfind(cmdout,'graphviz') ;
        if isempty(found) % dot command does not work :'(
            problem = true ;
        else
            problem = false ;
        end
    else
        problem = true ;
    end
    if problem
        warning('The path stored in pathDot does not seem to be valid. This might cause errors. Please destroy the file located in %s to reset the value.',fullPath) ;    
    end
end
 
end