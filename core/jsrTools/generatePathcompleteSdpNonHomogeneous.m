function [A,Agamma,gammaCoef,b,c,K] = generatePathcompleteSdpNonHomogeneous(M,graph,nlabels)
% GENERATEPATHCOMPLETESDPNONHOMOGENEOUS Returns the SDP problem 
%
%     [A,Agamma,gammaCoef,b,c,K] = GENERATEPATHCOMPLETESDPNONHOMOGENEOUS(M,graph,nlabels)
%       returns matrices and vectors which describes the following SDP :
%
%             max_P 0
%       s.t.    gamma^(2 * nlabels(i,j)) ( Mk' Pj Mk ) - Pi<=0 forall edges i,j,k (SDP sense)
%               Pi                      >  0 forall nodes i     (SP sense)
%
%       To describe the above problem for a given gamma with the given
%       output, we have to write :
%
%             min_x c'x
%       s.t.    [ A + (gammaCoef(gamma)^2-1)Agamma ] x = b
%               x in K
%
%       To have more information about the structure K, type HELP SEDUMI.
%       M must be a cell array of matrices and graph must be a structure
%       with the following fields :
%           graph.nNodes    must contain the number of nodes in the graph.
%           graph.edges     is a nedges x 3 matrix, where the edge i,j,k
%                           describes the following inequality :
%                           gamma^2 ( Ak' Pi Ak ) - Pj <= 0 (SPD sense).
%
%       nLabels should be a vector of integer indexed by edges, of size
%       [nEdges x 1]
%
%       This function is copied from the generate_path_complete_sdp of the
%       JSR toolbox from Raphael Jungers with some minor modification to
%       handle the non-homegeneousity.
%       http://www.mathworks.com/matlabcentral/fileexchange/33202-the-jsr-toolbox



matrixSize = size(M{1},1);
nEdges = size(graph.edges,1);
nNodes = graph.nNodes;

if iscellcomplex(M)
    error('Works only with real matrix.')
end

% Index map : symmetric matrix to vector. mapIdx(i,j) gives the index in a
% vector of the (i,j)th element.
% Example : for 2x2 matrices;
%
%   map = [ 1 2 ;
%           2 3 ] ;
%
% It means that, is we have a vector v = [a,b,c], then
% A(1,1) = v(map(1,1)) = v(1) = a ;
% A(2,1) = v(map(2,1)) = v(2) = b ;
% A(1,2) = v(map(1,2)) = v(2) = b ;
% A(2,2) = v(map(2,2)) = v(3) = c ;
%
% Note that for SDP solver, the cone S is for symetric semi-definite
% matrix, that's why the matrix map is also symetric and there is only
% n(n+1)/2 elements in A and not n^2.


mapIdx = zeros(matrixSize);
counter = 0;
for i=1:matrixSize;
    counter = counter + 1;
    mapIdx(i,i) = counter;
    for j=(i+1):matrixSize; % With SDP solver, we assume that P(i,j) = P(j,i)
        counter = counter + 1;
        mapIdx(i,j) = counter;
        mapIdx(j,i) = counter;
    end
end

% Description of the dual problem -> max b'y : AY-C in K* 
% <=> max b'y : AY-C = S, S in K*.
nFreeVarPrimal =  0;
nPositiveVarPrimal = 0;
nSdpMatricesPrimal = nNodes;
nConstraintsPrimal = (nEdges + nNodes) * matrixSize^2;
nVarPrimal = nFreeVarPrimal + nPositiveVarPrimal + nSdpMatricesPrimal*matrixSize*(matrixSize+1)/2;% Symetric!


% Description of the primal problem ( min c'x : Ax = b, X in K )
nFreeVarDual =  0;
nPositiveVarDual = 0;
nSdpMatricesDual = nEdges + nNodes;

% Description of the cone K (primal), see SEDUMI
K.f = nFreeVarDual;
K.l = nPositiveVarDual;
K.q = 0;
K.r = 0;
K.s = ones(nSdpMatricesDual,1)*matrixSize;

% non-zero element in the matrix.
nnz_max = nEdges*(matrixSize^4+matrixSize^2) + nNodes*(matrixSize^2);
nnz_max_gamma = matrixSize^4*nEdges;  % Idem with Agamma

% Memory allocation
c = zeros(nConstraintsPrimal,1);
b = zeros(nVarPrimal,1);

 % We store A in a sparse structure.
sparseLine = zeros(nnz_max,1); 
sparseCol = zeros(nnz_max,1);
sparseEntry = zeros(nnz_max,1);

sparseLineGamma = zeros(nnz_max_gamma,1);
sparseColGamma = zeros(nnz_max_gamma,1);
sparseEntryGamma = zeros(nnz_max_gamma,1);


% Initialization of some indices
offsetSdpIdx = nFreeVarPrimal + nPositiveVarPrimal ;
constraintIdx = 0;
sparseIdx = 0;
sparseGammaIdx = 0;

% First loop : We describe, for a given label M, M' Pj M - Pi <= 0.
% It can be shown that it is equivalent to
% [ gamma^(2*nlabels(e=(i,j))) sum[m,n] ( M*_{l,m} M_{n,k} Pj_{m,n} ) - Pi{k,l} ] <= 0 (sdp sense)
%
% Do not forget that P is symetric.

labelsVec = ones(nConstraintsPrimal,1) ;

for edgeIdx = 1:nEdges
    i = graph.edges(edgeIdx,1);
    j = graph.edges(edgeIdx,2);
    label = graph.edges(edgeIdx,3);
    Mstar = M{label}';
    
    % for each elements in Pi
    for k=1:matrixSize
        for l=1:matrixSize % We do not need to care about symetry here,
                           % everything is done by the mapIdx matrix.
                                     
            constraintIdx = constraintIdx + 1;
            
            % Update gammaVec
            labelsVec(constraintIdx) = nlabels(edgeIdx) ;
            
            % for each elements in Pj
            for m = 1:matrixSize
                for n = 1:matrixSize % We don't need to care about symetry here,
                                     % everything is done by the mapIdx matrix.
                    
                    Acol = getACol (j,m,n,matrixSize,offsetSdpIdx); % index of variable A(i,j)
                    
                    sparseIdx = sparseIdx+1;
                    sparseLine(sparseIdx) = constraintIdx;
                    sparseCol(sparseIdx) = Acol;
                    sparseEntry(sparseIdx) = Mstar(l,m) * ...% conjugate
                        M{label}(n,k); 
                    
                    % for gamma block
                    sparseGammaIdx = sparseGammaIdx+1;
                    sparseLineGamma(sparseGammaIdx) = constraintIdx;
                    sparseColGamma(sparseGammaIdx) = Acol;
                    sparseEntryGamma(sparseGammaIdx) = sparseEntry(sparseIdx);
                end
            end
            Acol = getACol (i,k,l,matrixSize,offsetSdpIdx);
            
            sparseIdx = sparseIdx+1;
            sparseLine(sparseIdx) = constraintIdx;
            sparseCol(sparseIdx) = Acol;
            sparseEntry(sparseIdx) = -1;
        end
    end
end

% Second loop : we want to describe P > 0. However, we have only a SDP solver.
% So, we use the homogeneity property of the problem (if P is a
% solution, then alpha*P is also a solution, for any positive alpha). It 
% implies that writing P >= I is equivalent to write P > 0.
% NOTE : we have to write the constraint in the dual form - so we  
% write -P - (- I) <= 0, where C = -I.

for i = 1:nNodes
    for k = 1:n
        for l = 1:n
            constraintIdx = constraintIdx+1;
            Acol = getACol (i,k,l,matrixSize,offsetSdpIdx);
            
            sparseIdx = sparseIdx+1;
            sparseLine(sparseIdx) = constraintIdx;
            sparseCol(sparseIdx) = Acol;
            sparseEntry(sparseIdx) = -1;
            if k==l
                c(constraintIdx) = -1;
            end
        end
    end
end

A = sparse(sparseLine,sparseCol,sparseEntry,nConstraintsPrimal,nVarPrimal,nnz_max);
Agamma = sparse(sparseLineGamma,sparseColGamma,sparseEntryGamma,nConstraintsPrimal,nVarPrimal,nnz_max_gamma);

% Col and line are reversed because it is A in the primal form, not A'.
A = A';
Agamma = Agamma';

% gammaCoef is returned as a function
gammaCoef = @(gamma) spdiags(gamma.^(2*labelsVec),0,nConstraintsPrimal,nConstraintsPrimal) ;

    function out = getACol (numA,i,j,n,offset)
        nElemInSymMatrix = n*(n+1)/2;
        out = offset+(numA-1)*nElemInSymMatrix + mapIdx(i,j);
    end
end
