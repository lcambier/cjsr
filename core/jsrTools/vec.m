function x = vec(X)
% VEC Vectorizes input
%
%   x = VEC(X) returns X(:)
x = X(:) ;
end