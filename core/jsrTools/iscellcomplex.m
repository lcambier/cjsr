function out = iscellcomplex(M)
% ISCELLCOMPLEX(M) Check if cell is complex
% 
%   OUT = ISCELLCOMPLEX(M) Check if all entries of the cell matrices in M
%   are real or not. If there is at least one complex
%   number in it, then returns OUT = 1. Otherwise, returns OUT = 0.
%
%   This function has been copied from the JSR toolbox
%   http://www.mathworks.com/matlabcentral/fileexchange/33202-the-jsr-toolbox

out = 0;

[m,n] = size(M);
for l = 1:m
    for c = 1:n
        if sum(~isreal(M{l,c})) > 0
            out = 1;
            return
        end
    end
end

end