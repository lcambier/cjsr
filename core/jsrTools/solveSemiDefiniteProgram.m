function [X,Y,feas,stopFlag,objectiveVal] = solveSemiDefiniteProgram(A,b,c,K,reltol,options)
% SOLVESEMIDEFINITEPROGRAM Solve the primal SDP min c'x : Ax=b, x in K.
%
%   [X,Y,feas,stopFlag,objectiveVal] = SOLVESEMIDEFINITEPROGRAM (A,b,c,K,reltol,options)
%   Solves the primal problem
%       min c'x     s.t. Ax = b, x in K,
%   with the given relative tolerance.
%   options should be a structure with the following fields :
%   options.solver : either 'sedumi' or 'sdpt3'
%   options.solverOpts : the option structure to be passed to sedumi or
%   sdpt3. The structure is passed "as it", with some minor modifications :
%   reltol is used to set the tolerance in the solver, while verbose flags
%   are usually set to 0.
%
%   Vector X is the primal solution, and vector Y is the dual solution.
%   The output feas (binary) is true when the problem is feasible, and
%   false else.
%   The output stopFlag is equal to 0 if all is ok, 1 if the solver is sure
%   that the problem is feasible/unfeasible, but X or Y is inacurate, and
%   equal to 2 if there is a complete failure.
%   Finally, objectiveVal is the value of the objective function.
%
%   This function is copied from the JSR toolbox, with some minor
%   modifications around line 150 as well as the last argument which has been
%   modified
%   http://www.mathworks.com/matlabcentral/fileexchange/33202-the-jsr-toolbox

stopFlag = [];
feas = false; % by default, the problem is infeasible.

solver_detected = 0;


%%%%%%%%%%%%%%%%%
% SeDuMi Solver %
%%%%%%%%%%%%%%%%%
if strcmp(options.solver,'sedumi')
    
    % Check existence of SeDuMi
    s = which('sedumi.m');
    if isempty(s)
        error(['Solver sedumi could not be found. Please install SeDuMi. '...
            'See http://sedumi.ie.lehigh.edu/']);
    end
    
    % Check version of SeDuMi
    if not(check_version_sedumi)
        warning(['solve_semi_definite_program has detected that an outdated version of SeDuMi is used. ',...
            'It could cause the method to crash. We strongly advise to download the lastest ',...
            'version of SeDuMi on ', ...
            'http://perso.uclouvain.be/raphael.jungers/sites/default/files/sedumi.zip.'])
    end
    
    solver_detected = 1;
    
    % Check options of SeDuMi
    if not(isfield(options,'solverOptions'))
        options.solverOptions = [];
    end
    
    pars = options.solverOptions;
    
    if not(isfield(pars,'fid'))
        pars.fid = 0; % Silent.
    end
    
    pars.eps = reltol;
    
    % Disable rank deficient warning.
    s = warning('off','MATLAB:rankDeficientMatrix');
    % Solve SDP
    [X,Y,info] = sedumi(A,b,c,K,pars);
    %Return warning state
    warning(s);
    objectiveVal = [] ;
    stopFlag = 0;
    
    % Postprocess
    if(isfield(info,'numerr') && info.numerr >= 2 ) % Total failure
        warning('SeDuMi : Numerical errors occurred');
        stopFlag = 2;
    else
        if(isfield(info,'numerr') && info.numerr == 1) % X and Y incacurate
            stopFlag = 1;
        end
        
        % Check feasability.
        if(info.pinf == 0 && info.dinf == 0 && info.feasratio > 0) % Feasible
            objectiveVal = mean([c'*X, b'*Y]);
            feas = true;
        else
            feas = false; % Infeasible
        end
    end
end



%%%%%%%%%%%%%%%%
% SDPT3 Solver %
%%%%%%%%%%%%%%%%

if strcmp(options.solver,'sdpt3')
    solver_detected = 1;
    s = which('sdpt3.m');
    if isempty(s)
        error(['Solver SDPT3 could not be found. Please install SDPT3 properly. '...
            'See http://www.math.nus.edu.sg/~mattohkc/sdpt3.html']);
    end
    
    if not(isfield(options.solverOptions,'printLevel'))
        options.solverOptions.printlevel = 0; % silent
    end
    
    % tolerance
    options.solverOptions.inftol = reltol;
    options.solverOptions.rmdepconstr = 1;
    options.solverOptions.warning = 0;
    
    [blk,At,C,bb] = read_sedumi(A,b,c,K); % Sedumi to SDPT3
    [objectiveValVec,X,Y,~,info] = sdpt3(blk,At,C,bb,options.solverOptions);
    objectiveVal = mean(objectiveValVec) ;
    
    % resize output (cell to vec).
    primalVar = zeros(size(A,1),1);
    idx = 1;
    for i=1:length(X)
        submatrixX = X{i};
        primalVar(idx : (idx+numel(submatrixX)-1)) =  vec(submatrixX) ;
        idx = idx + numel(X{i});
    end
    
    dualVar = vec(Y) ;
    
    if(info.termcode > 0) % Primal or dual infeasible
        feas = 0;
    else
        feas = 1;
    end
    X = primalVar;
    Y = dualVar;
end

if(solver_detected == 0)
    error('Invalid solver.')
end

    function out = check_version_sedumi()
        % If version is Ok, then out = 1.
        olderCorrectVersion = 20130724;
        if exist('Version.txt','file') == 2
            VersionSedumi = load('Version.txt');
            VersionSedumi = VersionSedumi(2);
            out = VersionSedumi >= olderCorrectVersion;
        else
            warning('Version.txt (used to check Sedumi version) not found.') ;
            out = 0 ;
        end                
    end

end