%% Test CSSystem.class
clc ; close all ; clear all ;

edges = [1 1 1 ;    
         1 2 3 ;
         2 2 2 ;
         2 1 4] ;
M1 = 1/2 ;
M2 = 1/4 ;
M3 = 4 ;
M4 = 2 ;
mats = {M1, M2, M3, M4} ;     
S1 = CSSystem(edges, mats) ;    

%% Concat test

edges = [1 1 1 ;
         1 2 3 ;
         2 2 2 ] ;
M1 = 1 ;
M2 = 1/4 ;
M3 = 2 ;
S2 = CSSystem(edges, {M1, M2, M3}) ;

%%
S = CSSystem.concat(S1, S2) ;

%% View tests

%S1.view() ;
%S2.view() ;
%S.view() ;

%% T lift test

ST = Tlift(S1, 1) ;

S1.view() ;
title('Original graph (1-lift)') ;

ST = Tlift(S1, 2) ;

ST.view() ;
title('2-lift') ;

ST = Tlift(S1, 3) ;

ST.view() ;
title('3-lift') ;

