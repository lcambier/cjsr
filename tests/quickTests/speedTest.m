% SPeedtest

[edges, matrices] = benchmarkPaper() ;
S0 = CSSystem(edges, matrices) ;
tol = 1e-5 ;

for t = 1:4
    time = tic ;
    ST = S0.TLift(t) ;
    ST.cjsrQuadMultinorm(tol) ;
    toc(time) ;
end
