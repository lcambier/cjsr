clc ; close all ; clear all ;

S = danglingNode1 ;
S.view() ;
isStronglyConnected(S.getEdges)

S = benchmarkPaper ;
S.view() ;
isStronglyConnected(S.getEdges)

S = scalar1 ;
S.view() ;
isStronglyConnected(S.getEdges)

S = polytopes1 ;
S.view() ;
isStronglyConnected(S.getEdges)

S = nonDefective1 ;
S.view() ;
isStronglyConnected(S.getEdges)

S = lowRank1 ;
S.view() ;
isStronglyConnected(S.getEdges)

S = cerny1 ;
S.view() ;
isStronglyConnected(S.getEdges)

S = CSSystem([1 2 1; 2 3 1 ; 3 4 1],{2}) ;
S.view() ;
isStronglyConnected(S.getEdges)

S = CSSystem([randi(4, [10, 2]) ones(10,1)],{2}) ;
S.view() ;
isStronglyConnected(S.getEdges)

