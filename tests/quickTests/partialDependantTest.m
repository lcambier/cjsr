clc ; close all ; clear all ;
Sys = benchmarkPaper() ;

Sys.view() ;

%% Past
Spart = Sys.partialDependantLift([1 2], 'past') ;
Spart.view() ;

Sfull = Sys.MDependantLift(1, 'past') ;
Sfull.view() ;

%% Future
Spart = Sys.partialDependantLift([1 2], 'future') ;
Spart.view() ;

Sfull = Sys.MDependantLift(1, 'future') ;
Sfull.view() ;