%% Test CSSystem.class
clc ; close all ; clear all ;

edges = [1 1 1 ;    
         1 2 3 ;
         2 2 2 ;
         2 1 4] ;
M1 = 1/2 ;
M2 = 1/4 ;
M3 = 4 ;
M4 = 2 ;
mats = {M1, M2, M3, M4} ;     

%%
S1 = CSSystem(edges, mats) ;    
S1.view(true) ;
title('S1') ;

%%
S = CSSystem.concat(S1, S1) ;
S.view(true) ;
title('S1+S2') ;

%%
ST = S1.TProductLift(3) ;
ST.view(true) ;
title('3-product(S1)') ;

ST.view(2) ;
title('3-product(S1)') ;

%%
SM = S1.MDependantLift(1, 'past') ;
SM.view(true) ;
title('2-path-dep(S1)') ;

%%
Ssos = S1.sosLift(2) ;
Ssos.view(true) ;
title('2-sos(S1)') ;

%%
Sarb = S1.arbitraryLift() ;
Sarb.view(true) ;
title('arbitrary(S1)') ;

%% Bissec
[ub, Q] = S1.cjsrQuadMultinorm() ;
[ub, Q] = ST.cjsrQuadMultinorm() ;
[ub, Q] = SM.cjsrQuadMultinorm() ;
[ub, Q] = Ssos.cjsrQuadMultinorm() ;
[ub, Q] = Sarb.cjsrQuadMultinorm() ;