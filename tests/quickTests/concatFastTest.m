clc ; close all ; clear all ;

for i = 1:10000
    edges1 = randi(30, 100, 3) ;
    edges2 = randi(30, 100, 3) ;
            
    [edgesOut1, oldLabels1] = concatSlow(edges1, edges2); 
    [edgesOut2, oldLabels2] = concatFast(edges1, edges2);
    
    if any(edgesOut1(:) ~= edgesOut2(:)) || any(oldLabels1(:) ~= oldLabels2(:))
        disp('Difference !') ;
        edges1
        edges2
        edgesOut1
        edgesOut2        
    end         
end

fprintf('Ok :-)') ;