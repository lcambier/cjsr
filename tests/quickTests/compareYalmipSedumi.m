clc ; close all ; clear all ;

for t = 1:4
    S = benchmarkPaper() ;
    S = S.TProductLift(t) ;

    %% 1) Yalmip
    opts = cjsrSettings('verbose',0,'cjsrQuadMultinorm.modeling','yalmip','cjsrQuadMultinorm.yalmip',sdpsettings('solver','sedumi')) ;
    [gamma1, Q1, info1] = S.cjsrQuadMultinorm(opts) ;

    %% 2) Sedumi
    opts = cjsrSettings('verbose',0,'cjsrQuadMultinorm.modeling','sedumi','cjsrQuadMultinorm.solver','sedumi') ;
    [gamma2, Q2, info2] = S.cjsrQuadMultinorm(opts) ;

    fprintf('%f vs %f (diff %f)\n', gamma1, gamma1, gamma2 - gamma1) ;
    if gamma1 ~= gamma2
        warning('Difference !')
    end
end