clc ; close all ; clear all ;

%%
Sys = benchmarkPaper() ;
Sys = Sys.sosLift(2) ;
[~,Q] = Sys.cjsrQuadMultinorm() ;
h = Sys.viewMultinorm(Q) ;

%%
edges = [1 2 1 ; 2 1 2] ;
mats = {[1/2 0 ; 0 1/4], [1/2 0 ; 0 1/4]} ;
Sys = CSSystem(edges, mats) ;
[~,Q] = Sys.cjsrQuadMultinorm() ;
h = Sys.viewMultinorm(Q) ;

%%
Sys = benchmarkPaper() ;
[~,Q] = Sys.cjsrQuadMultinorm() ;
h = Sys.viewMultinorm(Q) ;