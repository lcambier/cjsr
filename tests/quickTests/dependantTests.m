clc ; close all ; clear all ;

edges = [1 1 1 ;    
         1 2 3 ;
         2 2 2 ;
         2 1 4] ;
M1 = 1/2 ;
M2 = 1/4 ;
M3 = 4 ;
M4 = 2 ;
mats = {M1, M2, M3, M4} ;     
S0 = CSSystem(edges, mats) ;   

S0.view() ;

%% Past
SD1 = S0.MDependantLift(1,'past') ;
SD1.view() ;
title('1 Path Dependant') ;

SD2 = S0.MDependantLift(2,'past') ;
SD2.view() ;
title('2 Path Dependant') ;

SD3 = S0.MDependantLift(3,'past') ;
SD3.view() ;
title('3 Path Dependant') ;

SD3bis = SD2.MDependantLift(1,'past') ;
SD3bis.view() ;
title('3 Path Dependant bis') ;

%% Future
SD1 = S0.MDependantLift(1,'future') ;
SD1.view() ;
title('1 Horizon Dependant') ;

SD2 = S0.MDependantLift(2,'future') ;
SD2.view() ;
title('2 Horizon Dependant') ;

SD3 = S0.MDependantLift(3,'future') ;
SD3.view() ;
title('3 Horizon Dependant') ;

SD3bis = SD2.MDependantLift(1,'future') ;
SD3bis.view() ;
title('3 Horizon Dependant bis') ;