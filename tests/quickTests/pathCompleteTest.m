%% Test path complete

clc ; close all ; clear all ;

%[edges, matrices] = benchmarkPaper() ;        
edges = [1 1 1 ;    
         1 2 3 ;
         2 2 2 ;
         2 1 4] ;
M1 = 1/2 ;
M2 = 1/4 ;
M3 = 4 ;
M4 = 2 ;
matrices = {M1, M2, M3, M4} ;     

S0 = CSSystem(edges, matrices) ;   
S0.view() ;
title('0 Path Dependant') ;
gamma = S0.cjsrQuadMultinorm(1e-3)
graph.edges = S0.edges ;
graph.nNodes = S0.getNNodes() ;
gamma = jsr_pathcomplete(S0.matrices, graph) 

%%
SD1 = S0.MDependantLift(1,'past') ;
SD1.view() ;
title('1 Path Dependant') ;
gamma = SD1.cjsrQuadMultinorm(1e-3)

SD2 = S0.MDependantLift(2,'past') ;
SD2.view() ;
title('2 Path Dependant') ;
gamma = SD2.cjsrQuadMultinorm(1e-3)

SD3 = S0.MDependantLift(3,'past') ;
SD3.view() ;
title('3 Path Dependant') ;
gamma = SD3.cjsrQuadMultinorm(1e-3)