clc ; close all ; clear all ;

% S = randomSystem(3, 3, 4, 4) ;

for i = 1:10
    S = benchmarkPaper() ;
    edges = unique(randi(S.getNEdges, [S.getNEdges 1])) ;
    S = S.partialProductLift(edges) ;

    %% 1) Yalmip
    opts = cjsrSettings('verbose',0,'cjsrQuadMultinorm.modeling','yalmip','cjsrQuadMultinorm.yalmip',sdpsettings('solver','sedumi')) ;
    [gamma1, Q1, info1] = S.cjsrQuadMultinorm(opts) ;

    %% 2) Sedumi
    opts = cjsrSettings('verbose',0,'cjsrQuadMultinorm.modeling','sedumi','cjsrQuadMultinorm.solver','sedumi') ;
    [gamma2, Q2, info2] = S.cjsrQuadMultinorm(opts) ;

    fprintf('%f vs %f (diff %f)\n', gamma1, gamma1, gamma2 - gamma1) ;
    if gamma1 ~= gamma2
        warning('Difference !')
    end    
end