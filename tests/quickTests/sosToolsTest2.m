function sosToolsTest2()
clc ; close all ; clear all ;

% 1. Check convert slow vs convert multiset
d = 8 ;
itermax = 1000 ;
allPerms = perms(1:d) ;
for i = 1:itermax
    if mod(i, itermax/10) == 0
        fprintf('%d/%d\n', i, itermax) ;
    end
    % MULTISET
    multiset = randi(20, 1, 20) ;
    % Slow
    [ref1a, ref1b] = convertSlow(multiset) ;
    % Fast
    [ref2a, ref2b] = convertMultiset(multiset) ;
    if any(ref1a ~= ref2a)
        fprintf('Error in convertMultiset - multiset') ;
        multiset
        ref1a        
        ref2a        
    end
    if abs(ref1b - ref2b) > 1e-14 * abs(ref1b)
        fprintf('Error in convertMultiset - mu') ;
        multiset
        ref1b
        ref2b
    end
    
    % PERMANENT
    A = randn(d, d) ;    
    ref1 = permanentSlow(A, allPerms) ;
    ref2 = permanent(A, allPerms) ;
    if abs(ref1 - ref2) > 1e-14 * abs(ref1)
        fprintf('Error in permanent ...') ;
        A
        ref1
        ref2
    end        
end
fprintf('All good.') ;
end