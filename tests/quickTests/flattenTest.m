clc ; close all ; clear all ;

for i = 1:1000
    
    m = randi(5) ;
    n = randi(5) ;
    C = cell(m, n) ;
    for j = 1:m
        for k = 1:n
            C{j, k} = rand(randi(5), randi(5)) ;
        end
    end
    
    F1 = flattendAndConcatSlow(C) ;
        
    F2 = flattendAndConcat(C) ;
    
    if any(F1 ~= F2)
        C        
        error('Error !') ;
    end                
end