clc ; close all ; clear all ;

A = [0.94 0.56 ; 0.14 0.46] ;
B = [0 ; 1] ;
K1 = [-0.49 0.27] ;
K2 = [0 K1(2)] ;
K3 = [K1(1) 0] ;
K4 = [0 0] ;
mat = {A+B*K1 ; A+B*K2 ; A+B*K3 ; A+B*K4} ;

edges = [1 3 1 ;
         1 2 3 ;
         2 1 2 ;
         2 3 1 ;
         3 1 2 ;
         3 2 3 ;
         3 4 4 ;
         3 3 1 ;
         4 3 1 ] ;
         

S = CSSystem(edges, mat) ;

S.view() ;

for t = 1:5
    ST = S.TProductLift(t) ;
    timer = tic ;
    gamma(t) = ST.cjsrQuadMultinorm(1e-5) ;        
    time(t) = toc(timer) ;
end

figure ;
subplot(1, 2, 1) ; plot(gamma) ;
subplot(1, 2, 2) ; plot(time) ;