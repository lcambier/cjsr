clc ; close all ; clear all ;

edges = [1 1 1 ;    
         1 2 3 ;
         2 2 2 ;
         2 1 4] ;
M1 = 1/2 ;
M2 = 1/4 ;
M3 = 4 ;
M4 = 2 ;
mats = {M1, M2, M3, M4} ;     
S1 = CSSystem(edges, mats) ;   

S1.view() ;

Sout = S1.arbitraryLift() ;

Sout.view() ;

%% Compute CJSR with CSJR
gamma = S1.cjsrQuadMultinorm(1e-3) 


%% Compute CJSR with JSR
gamma = jsr_pathcomplete(Sout.matrices) 

%% Compute CJSR with grip
gamma2 = jsr_prod_Gripenberg(Sout.matrices) 

%% Checking for system without self loops
close all ; clear all ;
edges = [1 2 1 ;
         2 1 2] ;
mats = {2, 1/3} ;
S1 = CSSystem(edges, mats) ;
S1.view() ;

Sout = S1.arbitraryLift() ;

gamma = S1.cjsrQuadMultinorm(1e-3) 

gamma2 = jsr_prod_Gripenberg(Sout.matrices) 