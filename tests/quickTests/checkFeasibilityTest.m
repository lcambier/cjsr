clc ; close all ; clear all ;

edges = [1 1 1 ;
         1 1 2] ;

mat = {diag([1/3 1/2]), diag([0.6 1.2])} ;

S = CSSystem(edges, mat) ;

[gamma, Q] = cjsr_bissec(S, 1e-5, [0.1 2]) ;

gamma
Q