% Runs all benchmarks
clc ; close all ; clear all ;

names = {'scalar1','polytopes1','nonDefective1','lowRank1','cerny1'} ;
opts = cjsrSettings('cpoly.solver','sedumi','verbose',0) ;
for n = 1:numel(names)
    disp('------------------------------------------') ;
    eval(sprintf('S = %s();',names{n})) ;
    [P,bnds] = S.cjsrCplxPolytopeMultinorm(opts) ;
    bnds
end
%%
opts = cjsrSettings('cpoly.solver','sdpt3','verbose',0) ;
for n = 1:numel(names)
    disp('------------------------------------------') ;
    eval(sprintf('S = %s();',names{n})) ;
    [P,bnds] = S.cjsrCplxPolytopeMultinorm(opts) ;
    bnds
end