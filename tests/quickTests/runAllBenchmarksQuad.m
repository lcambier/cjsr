% Runs all benchmarks
close all ; clear all ;

names = {'scalar1','polytopes1','nonDefective1','lowRank1','cerny1'} ;

%%
opts = cjsrSettings('quad.solver','sedumi','verbose',0) ;
for n = 1:numel(names)    
    eval(sprintf('S = %s();',names{n})) ;
    rho = S.cjsrQuadMultinorm(opts) ;
    rho
end
disp('------------------------------------------') ;
%%
opts = cjsrSettings('quad.solver','sdpt3','verbose',0) ;
for n = 1:numel(names)
    eval(sprintf('S = %s();',names{n})) ;
    rho = S.cjsrQuadMultinorm(opts) ;
    rho
end
disp('------------------------------------------') ;
%%
opts = cjsrSettings('quad.modeling','yalmip','verbose',0,'quad.yalmip',sdpsettings('solver','sedumi')) ;
for n = 1:numel(names)    
    eval(sprintf('S = %s();',names{n})) ;
    rho = S.cjsrQuadMultinorm(opts) ;
    rho
end
disp('------------------------------------------') ;
%%
opts = cjsrSettings('quad.modeling','yalmip','verbose',0,'quad.yalmip',sdpsettings('solver','sdpt3')) ;
for n = 1:numel(names)    
    eval(sprintf('S = %s();',names{n})) ;
    rho = S.cjsrQuadMultinorm(opts) ;
    rho
end