% Test polyNorm
clc ; close all ; clear all ;

w1 = [1 0]' ;
w2 = [0 1]' ;
w3 = [1/2 3/4]' ;
v = rand(2, 1) ;
W = [w1 w2 w3] ;


tic ; n1 = polyNorm(W, v) 
toc ;
opts = cjsrSettings() ;
tic ; [~,n2] = checkBelonging(W, v, opts, 'norm')
toc ;