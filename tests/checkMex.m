%% Check mex
% Run the mex and compare the results to the plain matlab version
% It also shows the speed gain
clc ; close all ; clear all ;

%% FlattendAndConcat
for i = 1:1000
    C = cell(50, 1) ;
    for j = 1:numel(C)
        C{j} = rand(randi(10), randi(10)) ;
    end
    init = tic ; Cplain = flattenAndConcatPlain(C) ; timePlain(i) = toc(init) ;
    init = tic ; Cmex   = flattenAndConcat(C) ;      timeMex(i) = toc(init) ;
    
    if any(Cplain ~= Cmex)
        error('Error in mex file ! Wrong result') ;
    end
end
fprintf('Flatten And Concat\n') ;
fprintf('Plain : %f s\n', mean(timePlain)) ;
fprintf('Mex   : %f s\n', mean(timeMex)) ;

%% Permanent
clear all ;
d = 10 ;
tic ; allPerms = perms(1:d) ; toc ;
for i = 1:100            
    A = rand(d, d) ;
    init = tic ; Pplain = permanentPlain(A, allPerms) ; timePlain(i) = toc(init) ;
    init = tic ; Pmex   = permanent(A, allPerms) ;      timeMex(i) = toc(init) ;
    
    if Pplain ~= Pmex
        error('Error in mex file ! Wrong result') ;
    end
end
fprintf('Permanent\n') ;
fprintf('Plain : %f s\n', mean(timePlain)) ;
fprintf('Mex   : %f s\n', mean(timeMex)) ;

%% Convert Multisets
clear all ;
for i = 1:100            
    multiset = randi(4, [1 20]) ;
    init = tic ; [multisetConvMex,   muMex]   = convertMultiset(multiset) ;      timeMex(i) = toc(init) ;
    init = tic ; [multisetConvPlain, muPlain] = convertMultisetPlain(multiset) ; timePlain(i) = toc(init) ;
    
    if muMex ~= muPlain || any(multisetConvMex ~= multisetConvPlain)
        error('Error in mex file ! Wrong result') ;
    end
end
fprintf('Convert Multisets\n') ;
fprintf('Plain : %f s\n', mean(timePlain)) ;
fprintf('Mex   : %f s\n', mean(timeMex)) ;

%% Concat
clear all ;
for i = 1:100
    edges1 = randi(100, [100 3]) ;
    edges2 = randi(100, [100 3]) ;    
    init = tic ; [eMex,   lMex]   = concat(edges1, edges2) ; timeMex(i)   = toc(init) ;
    init = tic ; [ePlain, lPlain] = concatPlain(edges1, edges2) ; timePlain(i) = toc(init) ;
    if any(eMex(:) ~= ePlain(:)) || any(lMex(:) ~= lPlain(:))
        error('Error in mex file ! Wrong result') ;
    end
end
fprintf('Concat\n') ;
fprintf('Plain : %f s\n', mean(timePlain)) ;
fprintf('Mex   : %f s\n', mean(timeMex)) ;