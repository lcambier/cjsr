classdef SystemTest < matlab.unittest.TestCase
    
    methods (Test)
        
        function testSystem(testCase)       
            
            % First method
            edges = [1 2 1 ;
                     2 2 2 ;
                     1 1 3 ] ;
            mats = {1.5, 0.5, 2} ;
            S1 = CSSystem(edges, mats) ;
            
            testCase.verifyEqual(S1.getNEdges(), 3) ;
            testCase.verifyEqual(S1.getNNodes(), 2) ;
            for e = 1:3
                testCase.verifyEqual(S1.getLabels(e), edges(e, 3)) ;
            end
            testCase.verifyEqual(S1.getMatSize(), 1) ;
            for l = 1:3
                testCase.verifyEqual(S1.getMat(l), mats{l}) ;
            end
            for l = 1:3
                testCase.verifyEqual(S1.getMatProdLabel(l), mats{l}) ;
            end
            for e = 1:3
                testCase.verifyEqual(S1.getMatProdEdge(e), mats{edges(e,3)});
            end
            testCase.verifyEqual(S1.getNMats(), 3) ;            
            testCase.verifyEqual(S1.getMatrices(), mats') ;
            for e = 1:3
                testCase.verifyEqual(S1.getEdges(e), edges(e, 1:2)) ;
            end
            for l = 1:3
                testCase.verifyEqual(S1.getNLabels(l), 1) ;
            end
                                   
        end 
        
        function testSystem2(testCase)
           
            % Second method
            edges = [1 2 1;
                     2 1 4;
                     2 2 2;
                     1 1 3] ;
            mats = {1.5, 0.5, 2, 1} ;
            S1 = CSSystem(edges(:,1:2), mats, num2cell([1 4 2 3])') ;
            
            testCase.verifyEqual(S1.getNEdges(), 4) ;
            testCase.verifyEqual(S1.getNNodes(), 2) ;
            for e = 1:4
                testCase.verifyEqual(S1.getLabels(e), edges(e, 3)) ;
            end
            testCase.verifyEqual(S1.getMatSize(), 1) ;
            for l = 1:4
                testCase.verifyEqual(S1.getMat(l), mats{l}) ;
            end
            for l = 1:4
                testCase.verifyEqual(S1.getMatProdLabel(l), mats{l}) ;
            end
            for e = 1:4
                testCase.verifyEqual(S1.getMatProdEdge(e), mats{edges(e,3)});
            end
            testCase.verifyEqual(S1.getNMats(), 4) ;            
            testCase.verifyEqual(S1.getMatrices(), mats') ;
            for e = 1:4
                testCase.verifyEqual(S1.getEdges(e), edges(e, 1:2)) ;
            end
            for l = 1:4
                testCase.verifyEqual(S1.getNLabels(l), 1) ;
            end
            
        end
        
        function testProdLabelsGraph(testCase)
            
            edges = [1 2 ;
                     2 1 ;
                     2 2 ;
                     1 1 ] ;
            A = rand(2, 2) ;
            B = rand(2, 2) ;
            mats = {A, B} ;
            labels = {[1 2], [1], [1 1 2], [2 1 2]} ;
            
            S1 = CSSystem(edges, mats, labels) ;
            
            testCase.verifyEqual(S1.getMatProdEdge(1) , B*A, 'reltol', 1e-14) ;
            testCase.verifyEqual(S1.getMatProdEdge(2) , A, 'reltol', 1e-14) ;
            testCase.verifyEqual(S1.getMatProdEdge(3) , B*A*A, 'reltol', 1e-14) ;
            testCase.verifyEqual(S1.getMatProdEdge(4) , B*A*B, 'reltol', 1e-14) ;
           
            testCase.verifyEqual(S1.getMatProdLabel([1 2]), B*A, 'reltol', 1e-14) ;
            testCase.verifyEqual(S1.getMatProdLabel([1 2 1 2 2 2 1]), A*B*B*B*A*B*A, 'reltol', 1e-14) ;
              
            
            [graph, mats] = S1.getGraph() ;           
            edgesCheck = [1 2 ;
                          2 1 ;
                          2 2 ;
                          1 1] ;
            matsCheck = {B*A, A, B*A*A, B*A*B}' ;
            testCase.verifyEqual(graph.edges(:, 1:2), edgesCheck) ;
            for e = 1:4
                testCase.verifyEqual(mats{graph.edges(e,3)}, matsCheck{e}, 'reltol', 1e-14) ;                
            end
                                   
        end
        
        function testTProductLift(testCase)            
            
            mats = {rand(2), rand(2), rand(2)}' ;
            edges = [1 2 ; 2 1 ; 1 1] ;
            labs = {1, 3, [1 2]}' ;
            S = CSSystem(edges, mats, labs) ;
            ST = S.TProductLift(2) ;
            
            edgesT = ST.edges ;
            labsT = ST.labels ;
            matsT = ST.matrices ;
            
            edgesTcheck = [1 1 ; 2 2 ; 2 1 ; 1 2 ; 1 1] ;
            labsTcheck = {[1 3], [3 1], [3 1 2], [1 2 1], [1 2 1 2]}' ;
            matsTcheck = mats ;
            
            testCase.verifyEqual(matsT, matsTcheck) ;
            testCase.verifyEqual(edgesT, edgesTcheck) ;
            testCase.verifyEqual(labsT, labsTcheck) ;
            
        end
        
        function testMDependantLift(testCase)            
            mats = {rand(2), rand(2), rand(2)}' ;
            edges = [1 2 ; 2 1 ; 1 1] ;
            labs = {1, 3, [1 2]}' ;
            S = CSSystem(edges, mats, labs) ;
            
            ST = S.MDependantLift(1, 'past') ;
            
            edgesT = ST.edges ;
            labsT = ST.labels ;
            matsT = ST.matrices ;
            
            edgesTcheck = [2 1 ; 3 1 ; 1 2 ; 2 3 ; 3 3] ;
            labsTcheck = {[1], [1], [3], [1 2], [1 2]}' ;
            matsTcheck = mats ;
            
            testCase.verifyEqual(matsT, matsTcheck) ;
            testCase.verifyEqual(edgesT, edgesTcheck) ;
            testCase.verifyEqual(labsT, labsTcheck) ;            
            
            ST = S.MDependantLift(1, 'future') ;
            
            edgesT = ST.edges ;
            labsT = ST.labels ;
            matsT = ST.matrices ;           
            
            edgesTcheck = [1 2 ; 2 1 ; 2 3 ; 3 1 ; 3 3] ;
            labsTcheck = {[1], [3], [3], [1 2], [1 2]}' ;
            matsTcheck = mats ;
            
            testCase.verifyEqual(matsT, matsTcheck) ;
            testCase.verifyEqual(edgesT, edgesTcheck) ;
            testCase.verifyEqual(labsT, labsTcheck) ;     
        end
        
        function testSosLift(testCase)            
            A = [1 0 ; 0 0] ;
            Ad = sosLiftMatrix(A, 2) ;
            AdExpected = [0 0 0 ; 0 0 0 ; 0 0 1] ;
            testCase.verifyEqual(Ad, AdExpected) ;
            
            A = [0 0 ; 0 1] ;
            Ad = sosLiftMatrix(A, 2) ;
            AdExpected = [1 0 0 ; 0 0 0 ; 0 0 0] ;
            testCase.verifyEqual(Ad, AdExpected) ;
            
            A = [1 0 ; 0 1] ;
            Ad = sosLiftMatrix(A, 2) ;
            AdExpected = eye(3) ;
            testCase.verifyEqual(Ad, AdExpected) ;    
            
            Sys = randomSystem(3, 2, 4, 3) ;
            Ssos2 = Sys.sosLift(2) ;
            
            matricesBefore = Sys.matrices ;
            matricesAfter = matricesBefore ;
            for i = 1:numel(matricesBefore)
                matricesAfter{i} = sosLiftMatrix(matricesBefore{i},2) ;
            end
            testCase.verifyEqual(Ssos2.matrices, matricesAfter) ;               
        end
        
        function testArbitraryLift(testCase)            
            edges = [1 2 ; 2 1 ; 1 1] ;
            A = rand(3) ; B = rand(3) ;
            mats = {A, B}' ;
            labels = {1, 2, [1 2]} ;
            
            Sys = CSSystem(edges, mats, labels) ;
            Sarb = Sys.arbitraryLift() ;
            
            matArb = Sarb.matrices ;             
            testCase.verifyEqual(numel(matArb), 3) ;
                      
            A1 = [B*A zeros(3) ; zeros(3) zeros(3)] ;
            A2 = [zeros(3) B ; zeros(3) zeros(3)] ; % label sigma on edge (i -> j) give a non-zero entry in A(j, i) (block notation)
            A3 = [zeros(3) zeros(3) ; A zeros(3)] ;
            
            testCase.verifyEqual(matArb, {sparse(A3), sparse(A2), sparse(A1)}') ;
            testCase.verifyEqual(Sarb.edges, [1 1; 1 1;1 1]) ;
        end
        

                        
    end
end