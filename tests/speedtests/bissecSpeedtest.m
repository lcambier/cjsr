%% Create quite large random graph
clc ; close all ; clear all ;

matSize = 5 ;
nNodes = 10 ;
nNeighbours = 5 ;
nLabels = 10 ;

Sys = randomSystem(matSize, nNeighbours, nNodes, nLabels) ;

yalmipOpts = sdpsettings('verbose',1,'solver','sdpt3') ;
opts = cjsrSettings('yalmip', yalmipOpts) ;

tic ;
Sys.cjsrQuadMultinorm(opts) ;
toc ;
