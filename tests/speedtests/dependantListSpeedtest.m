%% Create quite large random graph
clc ; close all ; clear all ;

matSize = 20 ;
nNodes = 20 ;
nNeighbours = 8 ;
nLabels = 10 ;

Sys = randomSystem(matSize, nNeighbours, nNodes, nLabels) ;

%% Do Prod Lift and time
tic ;
for m = 1:100
    fprintf('Lift %d\n', m) ;
    St{m+1} = Sys.MDependantLift(m, 'past');
end
toc ;