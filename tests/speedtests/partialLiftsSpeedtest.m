%% Create quite large random graph
clc ; close all ; clear all ;

matSize = 20 ;
nNodes = 20 ;
nNeighbours = 8 ;
nLabels = 10 ;

Sys = randomSystem(matSize, nNeighbours, nNodes, nLabels) ;

%% Do Prod Lift and time
tic ;
for m = 1:100
    Sys.partialProductLift(1:10);
    Sys.partialDependantLift(1:10, 'past') ;
end
toc ;