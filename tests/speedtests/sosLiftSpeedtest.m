%% Create quite large random graph
clc ; close all ; clear all ;

matSize = 20 ;
nNodes = 30 ;
nNeighbours = 10 ;
nLabels = 10 ;

Sys = randomSystem(matSize, nNeighbours, nNodes, nLabels) ;

%% Do Prod Lift and time
tic ;
for t = 1:2
    fprintf('Lift %d\n', t) ;
    St{t} = Sys.sosLift(t) ;
end
toc ;