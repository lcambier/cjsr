%% Create quite large random graph
clc ; close all ; clear all ;

matSize = 30 ;
nNodes = 100 ;
nNeighbours = 100 ;
nLabels = 30 ;

Sys = randomSystem(matSize, nNeighbours, nNodes, nLabels) ;

tic ;
Sab = Sys.arbitraryLift() ;
toc ;
