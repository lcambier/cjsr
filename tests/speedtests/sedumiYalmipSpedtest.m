% Speedtest
close all ; clear all ; clc ;
Sys = randomSystem(10, 5, 5, 5) ;

T = 1 ;
repeat = 1 ;
optsRef = cjsrSettings('cjsrQuadMultinorm.tol', 1e-5,'verbose',0) ;
St = Sys.TProductLift(T) ;

% Sedumi
opts = cjsrSettings(optsRef, 'cjsrQuadMultinorm.modeling','sedumi') ;
for i = 1:repeat
    fprintf('Sedumi %d/%d\n', i, repeat) ;
    [ubSedumi,Q,info] = St.cjsrQuadMultinorm(opts) ;
    infoSedumi(i) = info ; 
end

% Yalmip
yalmipOpts = sdpsettings('verbose',0,'solver','sedumi') ;
opts = cjsrSettings(optsRef, 'cjsrQuadMultinorm.modeling','yalmip','cjsrQuadMultinorm.yalmip',yalmipOpts) ;
for i = 1:repeat
    fprintf('Yalmip %d/%d\n', i, repeat) ;
    [ubYalmip,Q,info] = St.cjsrQuadMultinorm(opts) ;
    infoYalmip(i) = info ;
end

fprintf('Solve -- Yalmip : %f vs %f : Sedumi\n', mean([infoYalmip.timeSolve]), mean([infoSedumi.timeSolve])) ;
fprintf('Build -- Yalmip : %f vs %f : Sedumi\n', mean([infoYalmip.timeModeling]), mean([infoSedumi.timeModeling])) ;