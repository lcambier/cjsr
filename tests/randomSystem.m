function Sys = randomSystem(matSize, nNeighbours, nNodes, nLabels)
% RANDOMSYSTEM Generates random systems
%
% Sys = randomSystem(matSize, nNeighbours, nNodes, nLabels) generates a
% random system with *at most* nLabels matrices matSize x matSize, nNodes
% and nNeighbours
%
% This is intended for benchmarking, so to keep things simple, each node is
% connected to the nNeighbours following nodes (to have a connected graph)
%
% Each matrix is a random uniform [0,1] matrix (i.e. each entry is
% uniformly distributed on [0,1] and independant from the others).

matrices = cell(nLabels, 1) ;
for l = 1:nLabels
    matrices{l} = rand(matSize, matSize) ;
end

edges = [] ;
nLabUsed = 1 ;
for n = 1:nNodes
    for nn = 1:nNeighbours
        neigh = mod(n+nn-1,nNodes)+1 ;
        lab = randi(nLabUsed) ;        
        if lab == nLabUsed && nLabUsed < nLabels
            nLabUsed = nLabUsed + 1 ;
        end
        edge = [n neigh lab] ;
        edges = [edges ; edge] ;
    end
end

Sys = CSSystem(edges, matrices(1:max(edges(:,3)),:)) ;

end