function [ cjsr, X ] = cjsr0( CSS )
%% CJSR0 Checks if system has a 0 CJSR or not.
V = CSS.getNNodes;
E = CSS.getNEdges;
n = CSS.getMatSize;

X = zeros(n,n,V);
for v = 1 : V
    X(:,:,v) = eye(n);   
end
edges = CSS.getEdges(1:E);
for k = 1 : n*V    
     X_next = zeros(n,n,V);
     for e = 1 : E
         matrix = CSS.getMatProdEdge(e);
         X_next(:,:,edges(e,2)) = X_next(:,:,edges(e,2)) + matrix' * X(:,:,edges(e,1)) * matrix;
     end
     X = X_next;
end
if X == 0
    cjsr = 0;
else
    cjsr = 1;
end
end

