clc ; close all ; clear all ;

w1 = [1;0] ;
w2 = [0;1] ;
W = [w1 w2] ;
v = [1/2;1/4]*1.5 ;

feas = checkBelongingFast(W, v);

figure ;
plot(W(1,:), W(2,:),'or') ; hold on ;
plot(v(1),v(2),'bo') ;
plot(0,0,'wo') ;

if feas
    fprintf('The point is in the absco(W)\n') ;
else
    fprintf('The point is not in the absco(W)\n') ;
end