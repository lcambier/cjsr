clc ; close all ; clear all ;

%%
% Sys = benchmarkPaper() ;
% P = cjsrCplxPolytopeMultinorm(Sys, [8 5 1 5 2 4 8 8]) ;

edges = [1 1 1; 1 1 2] ;
b = 0.78 ;
A = [1 1 ; 0 1] ;
B = b*[1 0 ; 1 1] ;
mats = {A, B} ;
Sys = CSSystem(edges, mats) ;
[P, rho] = cjsrCplxPolytopeMultinorm(Sys, [2 1 1 2 1]) ;


Sys.viewMultinorm('poly', P) ;

%%
figure(1) ;
for n = 1:Sys.getNNodes()
    Pp = P{n} ;
    subplot(2, 2, n) ; hold on 
    plot(Pp(1,:), Pp(2,:), 'o', 'color', 'red') ;
    plot(-Pp(1,:), -Pp(2,:), 'o', 'color', 'red') ;
end
title('Input') ;
figure(2) ;
for n = 1:Sys.getNNodes()
    Pp = P{n} ;
    subplot(2, 2, n) ; hold on 
    plot(Pp(1,:), Pp(2,:), 'o', 'color', 'red') ;
    plot(-Pp(1,:), -Pp(2,:), 'o', 'color', 'red') ;
end
title('Output') ;


%%
for e = 1:Sys.getNEdges()
    edge = Sys.getEdges(e) ;
    Pbeginning = [P{edge(1)}, -P{edge(1)}] ;
    for i = 1:100
        ijrand = randi(size(Pbeginning, 2), 2) ;
        l = rand() ;
        s = (l > 0.5)*(-1) + (l <= 0.5)*(1) ;
        point = s*(l*Pbeginning(:, ijrand(1))+(1-l)*Pbeginning(:, ijrand(2))) ;
        Mpoint = Sys.getMatProdEdge(e) * point / rho ;
        figure(1) ;
        subplot(2, 2, edge(1)) ;
        plot(point(1), point(2), 'o', 'color','blue') ;
        figure(2) ;
        subplot(2, 2, edge(2)) ; 
        plot(Mpoint(1), Mpoint(2), 'o', 'color','cyan') ;
    end
end