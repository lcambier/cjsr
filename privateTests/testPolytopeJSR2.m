clc ; close all ; clear all ;


X = [0.4712    0.9544 ;
     0.7444    0.3503 ] ;

v = [0.8489 ; 0.5198] ;

figure ;
hold on ;
plot(X(1, :), X(2, :), 'or') ;
plot(v(1), v(2), 'xb') ;

lambdaQuad = [0.3645 ;
              0.7095 ] ;
          
xlim([0 1]) ;
ylim([0 1]) ;          
          
% Plot the points for which lambda is ok   