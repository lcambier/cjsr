% Test Yalmip SOS

n = 3 ;
d = 2 ;

x = sdpvar(n, 1) ;

xMono = monolist(x, d, d) ;
N = length(xMono) ;

nNodes = 2 ;
for node = 1:nNodes
    Q{node} = sdpvar(N, N) ;
    p{node} = xMono' * Q{node} * xMono ;
end

% Constraints
cntr = [] ;
for node = 1:nNodes
    cntr = [cntr ; ...
            sos(p{node})] ;       
end

