close all ; clear all ; clc ;
opts = sdpsettings('verbose',0,'solver','sdpt3') ;
opts2 = cjsrSettings('cpoly.solver','sdpt3') ;
iMax = 100 ;
for i = 1:iMax
    n = randi(10) ;
    d = randi(10) ;
    
    X = randn(d,n)+1i*randn(d,n) ;
    y = randn(d,1)+1i*randn(d,1) ;
    
    lambda = sdpvar(n,1,'full','complex') ;
    rho = sdpvar(1,1) ;
    cntr1 = X*lambda == rho*y ;
    cntr2 = sum(abs(lambda)) <= 1 ;
    info = optimize([cntr1 cntr2],rho,opts) ;
    normv1 = -1/double(rho) ;    
    normv2 = polyNormRobust(X, y, opts2) ;
    
    if isempty(normv2)
        normv2 = inf ;
    end
    fprintf('%d/%d - Ok (%f vs %f)\n', i, iMax, normv1, normv2) ;
    
end