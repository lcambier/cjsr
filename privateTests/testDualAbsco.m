%%
clc ; close all ; clear all ;
N = 200 ;
n = 100 ;

x = rand(n, N) + 1i*rand(n, N) ;
%y = rand(n, 1) + 1i*rand(n, 1) ;
y = x(:, randi(N))*1 ;

for i = 1:N
    X(:,i*2-1:i*2) = [real(x(:,i)) -imag(x(:,i)) ; imag(x(:,i)) real(x(:,i))] ;
end
Y = [real(y) ; imag(y)] ;

%% Primal formulation
lambda = sdpvar(2*N, 1, 'full') ;
v = sdpvar(N, 1, 'full') ;
s = sdpvar(1, 1, 'full') ;

cntrP1 = X * lambda == Y ;
cntrP2 = sum(v) + s == 1 ;
cntrP3 = [] ;
for i = 1:N
    cntrP3 = [cntrP3 ; v(i) >= norm([lambda(2*i-1) ; lambda(2*i)])] ;
end
cntrP4 = s >= 0 ;

solP = optimize([cntrP1 ; cntrP2 ; cntrP3 ; cntrP4], 0) ;

%% Dual formulation
w0 = sdpvar(1, 1, 'full') ;
wR = sdpvar(n, 1, 'full') ;
wI = sdpvar(n, 1, 'full') ;
alpha = sdpvar(N, 1, 'full') ;
betaR = sdpvar(N, 1, 'full') ;
betaI = sdpvar(N, 1, 'full') ;
rho = sdpvar(1, 1, 'full') ;

objD = - w0 - Y'*[wR ; wI] ;
cntrD1 = w0 + rho == 0 ;
cntrD2 = [] ;
cntrD3 = [] ;
cntrD4 = [] ;
for i = 1:N
    cntrD2 = [cntrD2 ; w0 + alpha(i) == 0] ;
    cntrD3 = [cntrD3 ; alpha(i) >= norm([betaR(i) ; betaI(i)])] ;    
    xxloc = [real(x(:,i))' imag(x(:,i))' ; imag(x(:,i))' real(x(:,i))'] ;
    cntrD4 = [cntrD4 ; xxloc * [wR ; wI] + [betaR(i) ; betaI(i)] == zeros(2, 1) ] ;
end

solD = optimize([cntrD1 ; cntrD2 ; cntrD3 ; cntrD4], objD) ;

%% Dual formulation (short)
w  = sdpvar(n, 1, 'full', 'complex') ;

objDD = 0 ;
cntrDD1 = [] ;
for i = 1:N
    cntrDD1 = [cntrDD1 ; real(y'*w) >= 1 + abs(x(:,i)'*w)] ;    
end
solDD = optimize(cntrDD1, objDD) ;


%% Results
solP
solD
solDD