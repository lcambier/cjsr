% testBalancedComplexPolytope
clc ; close all ; clear all ;

%% Exemple 1
A = [1 1 ; 0 1] ;
b = 0.78 ;
B = b*[1 0 ; 1 1] ;

M = {A, B} ;
C = A*B*A*A*B ;
k = 5 ;
[bounds, V, info] = jsr_norm_balancedComplexPolytope(M, C, k) ;
figure ;
plot(V(:, 1), V(:, 2), 'o') ;

%% Exemple 2
A = [-3 -2 1 2 ; -2 0 -2 1 ; 1 3 -1 -5 ; -3 -3 -2 -1] ;
B = [1 0 -3 -1 ; -4 -2 -1 -4 ; -1 0 -1 2 ; -1 -2 -1 2] ;

M = {A, B} ;
C = A*B ;
k = 2 ;

[bounds, V, info] = jsr_norm_balancedComplexPolytope(M, C, k) ;
figure ;
plot(V(:, 1), V(:, 2), 'o') ;

%% Exemple 3
A = [75407947696/105973492625   -1375188679296/2649337315625   2264216426496/13246686578125 ;
    1 0 0 ;
    0 1 0 ] ;

B = [1371520404/1048676375   -26436927264/26216909375   24625176576/131084546875 ;
    1 0 0 ;
    0 1 0 ] ;

C = [23699556/17272375   -516800736/431809375  917070336/2159046875 ;
    1 0 0 ;
    0 1 0] ;

M = {A, B, C} ;
C = A*B*C ;
k = 3 ;

[bounds, V, info] = jsr_norm_balancedComplexPolytope(M, C, k) ;
