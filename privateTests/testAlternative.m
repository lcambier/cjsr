close all ; clear all ; clc ;

X = [1 0 ; 0 1/2] ;
y = 1.001*[1/2 ; 1/4] ;

X1 = [ -0.300000000000000 + 1.228820572744451i -0.300000000000000 - 1.228820572744451i ;
      1.000000000000000 + 0.000000000000000i  1.000000000000000 + 0.000000000000000i   ] ;


y1 = [ 0.295979252613731 + 1.229795219547636i ;
      0.889002431411746 - 0.457902475363481i ] ;

X2 = [ -0.3000 + 1.2288i , -0.3000 - 1.2288i ;
       1.0000 + 0.0000i ,  1.0000 + 0.0000i ] ;
y2 = [ 0.2960 + 1.2298i ; 
       0.8890 - 0.4579i] ;
   
X3 = [1+2i , 3+4i ; 5+6i, 7+8i] ;
y3 = [9+10i ; 11+12i] ;

X4 = [1+2i ; 5+6i] ;
y4 = [9+10i ; 11+12i] ;
   
X = X ;
y = y ;



% %% 1)
% lambda = sdpvar(size(X, 2),1,'full','complex') ;
% v = sdpvar(size(X, 2),1,'full') ;
% cntr1 = X*lambda == sum(v)*y ;
% cntr2 = [] ;
% for i = 1:size(X, 2)
%     cntr2 = [cntr2 v(i)>=abs(lambda(i))] ;
% end
% info = optimize([cntr1 cntr2],-sum(v)) ;
% info
% double(-sum(v))
% %% 2)
% opts = cjsrSettings ;
% type = 'feas' ;
% [feas] = checkBelongingRobust2(X, y, opts, type)

%% 3)
lambda = sdpvar(size(X, 2),1,'full','complex') ;
rho = sdpvar(1,1) ;
cntr1 = X*lambda == rho*y ;
cntr2 = sum(abs(lambda)) <= 1 ;
info = optimize([cntr1 cntr2],rho) ;


normv = polyNormRobust(X, y) ;

fprintf('Norm = %f vs %f\n', -1/double(rho), normv) ;