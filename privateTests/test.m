clc ;  clear all ; close all ; 

%% Exemple 1
Sys = benchmarkPaper() ;
opts = cjsrSettings('cpoly.plot', true, 'cpoly.solver', 'sdpt3', 'cpoly.maxPathsLength', 4, 'cpoly.checkNewCycle', true, 'cpoly.maxiter', 10) ;
profile on ; P = cjsrCplxPolytopeMultinorm(Sys, [8 5 1 5 2 4 8 8], opts) ; profile off ; profile viewer ;
% profile on ; P = cjsrCplxPolytopeMultinorm(Sys, [8 5 1], opts) ; profile off ; profile viewer ;

%%
[~,Qq] = Sys.cjsrQuadMultinorm() ;
Ssos2 = Sys.sosLift(2) ;
[~,Qs2] = Ssos2.cjsrQuadMultinorm() ;
Ssos3 = Sys.sosLift(3) ;
[~,Qs3] = Ssos3.cjsrQuadMultinorm() ;
Ssos5 = Sys.sosLift(5) ;
[~,Qs5] = Ssos5.cjsrQuadMultinorm() ;
Ssos7 = Sys.sosLift(7) ;
[~,Qs7] = Ssos7.cjsrQuadMultinorm() ;

viewMultinorm(Sys, 'quad', Qq) ;
viewMultinorm(Sys, 'sos', Qs2, 2) ;
viewMultinorm(Sys, 'sos', Qs3, 3) ;
viewMultinorm(Sys, 'sos', Qs5, 5) ;
viewMultinorm(Sys, 'sos', Qs7, 7) ;
viewMultinorm(Sys, 'poly', P) ;

%% Exemple 2
edges = [1 1 1; 1 1 2] ;
b = 0.78 ;
A = [1 1 ; 0 1] ;
B = b*[1 0 ; 1 1] ;
mats = {A, B} ;
Sys = CSSystem(edges, mats) ;

P = cjsrCplxPolytopeMultinorm(Sys, [2 1 1 2 1]) ;

[~,Qq] = Sys.cjsrQuadMultinorm() ;
Ssos2 = Sys.sosLift(2) ;
[~,Qs2] = Ssos2.cjsrQuadMultinorm() ;
Ssos3 = Sys.sosLift(3) ;
[~,Qs3] = Ssos3.cjsrQuadMultinorm() ;
Ssos5 = Sys.sosLift(5) ;
[~,Qs5] = Ssos5.cjsrQuadMultinorm() ;
Ssos7 = Sys.sosLift(7) ;
[~,Qs7] = Ssos7.cjsrQuadMultinorm() ;

viewMultinorm(Sys, 'quad', Qq) ;
viewMultinorm(Sys, 'sos', Qs2, 2) ;
viewMultinorm(Sys, 'sos', Qs3, 3) ;
viewMultinorm(Sys, 'sos', Qs5, 5) ;
viewMultinorm(Sys, 'sos', Qs7, 7) ;
viewMultinorm(Sys, 'poly', P) ;


%% Exemple 3
A = [75407947696/105973492625   -1375188679296/2649337315625   2264216426496/13246686578125 ;
    1 0 0 ;
    0 1 0 ] ;
B = [1371520404/1048676375   -26436927264/26216909375   24625176576/131084546875 ;
    1 0 0 ;
    0 1 0 ] ;
C = [23699556/17272375   -516800736/431809375  917070336/2159046875 ;
    1 0 0 ;
    0 1 0] ;
M = {A, B, C} ;

edges = [1 1 1 ; 1 1 2 ; 1 1 3] ;
Sys = CSSystem(edges, M) ;
cjsrCplxPolytopeMultinorm(Sys, [3 2 1]) ;