function [feas, normv] = checkBelonging(W, v, opts, type)
% CHECKBELONGING Absolute convex hul belonging or norm
%
% feas = checkBelonging(W, v, opts, 'feas') returns true if v belongs to the convex hull
% of W, false otherwise.
% v should be a column vector. W is a matrix where each column is a vertex
% of the complex polytope absco(W).
% opts can contains
%   - opts.cpoly.solver ('sedumi') : the solver name, either 'sedumi' or
%     'sdpt3'.
%
% [feas, norm] = checkBelonging(W, v, opts, 'norm') returns (true,norm) if
% the norm of v can be computed using W. Returns (false,[]) otherwise.
%
% A complex polytope P, represented by a set of vectors w1 ... wn is the 
% set of x such that there exist lambda1...lambdan such that
%       sum_i lambdai wi = x
%       sum_i |lambdai| <= 1
%       lambdai is complex

    if ~ (strcmp(type, 'norm') || strcmp(type, 'feas'))
        error('type should be norm or feas') ;
    end
    if strcmp(type,'feas') && nargout ~= 1
        error('with type feas, only one argout authorized') ;
    end    
    if strcmp(type,'norm') && nargout ~= 2
        error('with type norm, 2 argouts required') ;
    end
    if size(v, 2) ~= 1        
        error('v should be a vector') ;
    end
    if size(v, 1) ~= size(W, 1)
        error('W and v should have the same (1st) dimension') ;
    end    
    
    %% Tolerance for preprocess
    tol = opts.cpoly.reltol ;

    %% Modeling
    d = size(W, 1) ;
    n = size(W, 2) ;
    
    nVar = 1 + 3 * n ;
    Aeq1 = [1 ones(1, n) zeros(1, 2*n)] ;
    Aeq2 = [ zeros(d, 1) zeros(d, n) real(W) -imag(W)  ;
             zeros(d, 1) zeros(d, n) imag(W)  real(W) ] ;
    A = [Aeq1 ; Aeq2] ;
    c = zeros(nVar, 1) ;
    % For the conic expression, we need to flip stuff
    newIdx = 1 ;
    for i = 1:n
        newIdx = [newIdx 1+[i i+n i+2*n]] ;
    end
    A = A(:, newIdx) ;
    % Preprocess if too much constraints
    preprocess = false ;
    if nVar < size(A, 1)
        preprocess = true ;
        [U1, S, U2] = svd(A) ; % A x = (U1 S U2') x = b
        % Get rank
        dimS = sum(diag(S) > tol * norm(S,'fro')) ; % Basically, returns the rank of A
        A = S(1:dimS,1:dimS) * U2(:,1:dimS)' ;
    end
    % The cone
    K.f = 0 ;
    K.l = 1 ;
    K.q = 3*ones(n, 1) ;
    K.r = 0 ;
    K.s = 0 ;
    % The RHS    
    b = [1 ; real(v) ; imag(v)] ; 
    solveOrNot = true ;
    if preprocess % Preprocess 
        b = U1' * b ;
        % Now let's check if the problem can be solve
        % Ax = b <=> U1 S U2' x = b 
        % Then after the "trimmed" SVD, removing the zeros singular values
        % S U2' = U1' * b
        % Since we only keep the dimS first rows (which are zeros on the left), the last rows of U1' * b
        % should be zero in order to be solvable
        if norm(b(dimS+1:end,1)) <= tol * norm(b,'fro') 
            % Can then be solved
            b = b(1:dimS,1) ;
        else
            % Cannot be solved since the last element should have been zero
            feas = false ;
            solveOrNot = false ;
            if nargout == 2
                normv = [] ;
            end            
        end
    end    
    % Solving
    if solveOrNot
        % Call solver
        if strcmp(opts.cpoly.solver,'sedumi')
            solOpts = opts.sedumi ;            
        elseif strcmp(opts.cpoly.solver,'sdpt3')
            solOpts = opts.sdpt3 ;
        end
        options.solver = opts.cpoly.solver ;
        options.solverOptions = solOpts ;
        reltol = tol ;
        if nargout == 1 % type = feas
            [X,Y,feas,stopFlag,objectiveVal] = solveSemiDefiniteProgram(A,b,c,K,reltol,options) ;            
        elseif nargout == 2 % type = norm            
            a = [0 ; - real(v) ; - imag(v)] ; % Add the "rhoinv" variables at the top
            A = [a A] ;
            b = [1 ; zeros(2*d,1)] ;    
            c = [-1 ; zeros(nVar,1)] ;
            K.l = 2 ;
            [X,Y,feas,stopFlag,minusrhoinv] = solveSemiDefiniteProgram(A,b,c,K,reltol,options) ;
            normv = (-minusrhoinv)^(-1) ;
        else
            error('One or two argouts required') ;
        end
    end
end
