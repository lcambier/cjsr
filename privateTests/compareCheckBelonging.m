for i = 1:100
    
    n = randi(10) ;
    d = randi(8) ;
    
    W = 2*(rand(d,n)-1/2) + 1i * 2*(rand(d,n)-1/2) ;
    v = 5*(rand()-1/2)*(rand(d,1) + 1i * rand(d,1)) ;
    opts = cjsrSettings ;
    type = 'feas';
    
    feas1 = checkBelongingOld(W, v, opts, type)   ; 
    feas2 = checkBelongingRobust(W, v, opts)  ;
    
    lambda = sdpvar(n,1,'full','complex') ;
    cntr = [W*lambda == v, sum(abs(lambda))<=1] ;
    opts = sdpsettings('verbose',0) ;
    diag = optimize(cntr,0,opts) ;
    if strcmp(diag.info,'Infeasible problem (SeDuMi-1.3)')
        feas3 = 0 ;
    elseif strcmp(diag.info,'Successfully solved (SeDuMi-1.3)')
        feas3 = 1 ;
    else
        warning('Error yalmip') ;
    end
    
    fprintf('%d vs %d vs %d\n',feas1,feas2,feas3) ;
    
end