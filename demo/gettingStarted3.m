%% Getting started 3
% Simply run this script
% Make sure to have the full toolbox on the matlab path, as well as the 
% SEDUMI toolbox.
clc ; close all ; clear all ;
disp('-----------------------------------------------------------------------------') ;
disp('Welcome to the CJSR Toolbox - Getting Started 3 (Complex polytope multinorms)') ;
disp('-----------------------------------------------------------------------------') ;
disp('The tutorial is based on the example from')
disp('[1] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015).') ;
disp('    Stability of discrete-time switching systems with constrained switching sequences.')
disp('    arXiv preprint arXiv:1503.06984.') ;
disp('The system can be loaded using the following command:')
strToRun = 'S = benchmarkPaper()' ; runCmdPause ;

disp('-------------------------------------------------------------------') ;
disp('Again, we wish the compute the CJSR of the system S. If CJSR < 1, we are stable.') ;
disp('----- Complex Polytope Multinorms - Guglielmi al. 2008 [2] ---') ;
disp('This method is inspired from the paper [2], and adapted') ;
disp('to the constrained switching case.') ; 
disp('[2] Guglielmi, Nicola, and Marino Zennaro. "An algorithm for finding extremal');
disp('    polytope norms of matrix families." Linear Algebra and its Applications') ;
disp('    428.10 (2008): 2265-2282.') ;
pauseCmd() ;
disp('The method aims at building an invariant balanced complex polytope');
disp('per node of the graph. Together they form a polytopic multinorm.');
disp('By invariant, we mean if CJSR = 1, for an edge (i,j,s),')
disp('the image of polytope(i) by the matrix (s) intersects polytope (j) on its border.')
pauseCmd() ;
disp('This method requires an initial candidate extremal cycle, i.e. a cycle') ;
disp('with an averaged spectral radius hopefully close to the CJSR of S.') ;
disp('This cycle should be given as a vector of edges-index') ;
disp('Remember the set of edges') ;
strToRun = 'S.getEdges()' ; runCmdPause() ;
disp('A candidate SMP can be encoded as') ;
strToRun = 'smp_candidate = [8 5 1]' ; runCmdPause() ;
disp('This candidate corresponds to the cycle given by')
strToRun = 'S.getEdges(smp_candidate)'; runCmdPause();
disp('The sequence of labels here is ')
strToRun = 'S.getLabels(smp_candidate)'; runCmdPause();
disp('The method will test if this candidate is indeed an smp.') ;
disp('If not, it uses the candidate as an initial guess and refines it during iterations.') ;
pauseCmd();
disp('We can now call the cjsrCplxPolytopeMultinorm function') ;
disp('It will take about 1 minute to terminate.') ;
strToRun = '[P, bounds, smp] = S.cjsrCplxPolytopeMultinorm(smp_candidate) ;' ; runCmdPause() ;
disp('The bounds variable contains lower and upper bounds on the CJSR:') ;
strToRun = 'bounds' ; runCmdPause() ;
disp('Since the 2 bounds are very close, we can conclude the algorithm successfully') ;
disp('terminated.') ;
disp('The smp output contains the best cycle found by the algorithm') ;
strToRun = 'smp' ; runCmdPause() ;
disp('The algorithm was able to improve upon smp_candidate.')

disp('The P output is a cell array containing the vertices of ');
disp('the extremal polytopes obtained at each node at')
disp('the end of the algorithm.') ;
pauseCmd() ;
disp('The resulting extremal norm can be plotted using the output P:') ;
strToRun = 'S.viewMultinorm(''poly'',P);' ; runCmdPause ;
disp('Note that this function only plots the real part in the two-dimensional case.') ;


disp('You can learn more about this function by typing')
disp('help CSSystem.cjsrCplxPolytopeMultinorm') ;

disp('This demonstration is now terminated.') ;
disp('-------------------------------------------------------------------') ;
