%% Illustrates the use of partials lifts to improve the upperbound on the CJSR
%
% This script can be used to see the effect of the partials lifts.
% Again, it uses sdpt3. You can change it to 'sedumi' if needed.
%
% This is an experimental feature, which serves more as an illustration of
% the kind of operations one can perform on a CSSystem object.
%
% The most direct method to refine the upper bound obtained by using
% cjsrQuadMultinorm is to lift the system.
%
% The idea behind partial lifts is to only lift one (or a few) node(s), to
% avoid lifting all of them if it is not necessary.
% Then, we can try to analyse the evolution of the upper-bounds (by lifting
% the first node/edge, the first 2, etc) to see at which point it decreases
% the most. This node/edge at which the change is the greatest should
% probably be the most important one.
% 
% This function takes about 1 minute to run completely
clc ; close all ; clear all ;

% Again, let us use our benchmark example
S = benchmarkPaper() ;
opts = cjsrSettings('verbose',0,'quad.solver','sdpt3') ;

% Let us make increasingly higher partial product lifts
ub = [] ;
for e = 1:S.getNEdges()   
    % The first argument of partialProductLift is a vector of *edge* index
    % that will be lifted
    % 'past' means we lift then according to the length-2 path entering the
    % edges from the past
    % 'future' could be used instead
    % All the edges in the vector will be lifted. So in this case, Sp is
    % lifted with an increasingly high number of edges (1, 1 and 2, 1, 2
    % and 3, etc)
    Sp = S.partialProductLift(1:e, 'past') ;
    ub(e) = Sp.cjsrQuadMultinorm(opts) ;
end
figure ;
plot(ub) ;
title('Evolution of the UB for the partials product lifts') ;
% We can clearly see that lifting the edge 8 seems to be the most useful.

% Let us now make increasingly higher partial dependant lifts
ub = [] ;
for n = 1:S.getNNodes()
    % The first argument of partialDependantLift is a vector of *node*
    % index
    % 'past' mean we split the nodes according to the entering edges
    % 'future' could be used instead
    Sp = S.partialDependantLift(1:n, 'past') ;
    ub(n) = Sp.cjsrQuadMultinorm(opts) ;
end
figure ;
plot(ub) ;
title('Evolution of the UB for the partials dependant lifts') ;
% We can clearly observe that lifting the node 3 seems the most useful
% operations.
