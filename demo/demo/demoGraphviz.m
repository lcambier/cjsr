%% Demonstration of the use of the GraphViz/dot external software to visualize and export systems
% To run this script step by step, use the "cell" command to only run the 
% highlighted block of code and to automatically jump to the next one at
% the end (ctrl+shift+enter on windows, cmd+shift+enter on Mac OSX)
clc ; close all ; clear all ;

%% 1) Installation of dot
% The first step is to properly install dot
% One way to do it is to install graphviz from http://www.graphviz.org

% This toolbox was tested with GraphViz 2.36 on Mac and 2.38 on Windows

% On windows, go to Download -> Windows - Stable and development Windows Install packages
% -> graphviz-x.xx.msi
% Download, double click, and follow instructions.
% Keep track of the installation directory, we'll use it later.
% It will probably be C:\Program File (x86)\graphviz-x.xx
% and dot will then be located in
% C:\Program File (x86)\graphviz-x.xx\bin\dot.exe

% On Mac OSX, the easiest way to install it is from homebrew
% (http://brew.sh). 
% 1. First, install Homebrew by typing in a terminal 
%   ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
% 2. Once Homebrew is installed, you can install graphviz by typing in terminal
%   brew install graphviz
% Using this method, dot (the part of Graphviz we will use) can be run and
% is located in
% /usr/local/bin/dot

% On linux, if you use ubuntu you can install graphviz by typing in a
% terminal
%   sudo apt-get install graphviz

%% 2) Build a CSSystem
% Once GraphViz is installed, we can load our "benchmark" CSS using
S = benchmarkPaper() ;

%% 3) Then, we can visualise it thanks to dot.
% At the first use of the view() or export() command, Matlab will ask for
% the location of the dot binary *if* it can't figure it out by itself.

% If you use Windows or Linux, the toolbox will open a dialog box so that
% you can easily point to the dot binary (dot.exe on windows, dos on Linux)
% On Mac OS X, you will have to manually type the directory in the Matlab
% prompt (this is because using the Mac OS X finder, it is not that easy to
% find locations like /usr/local ...)

% Note that once that path to dot has been set (either automatically or by
% hand), it will be saved in a file in the core/utils folder.

% Once this is done, you will be able to visualize your system by typing
S.view() ;

% We can also export the graph to different formats.
% The first argument is the name of the resulting file, and the list of
% extensions is the formats we want.
% All the possible formats are listed here : http://www.graphviz.org/doc/info/output.html
S.export('graph', {'dot','png','svg','eps'}) ;