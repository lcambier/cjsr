%% Illustrates the use of the add methods to modify system
% This shows how to add edges to a system, 
% and how to use the data to create visualizations 
% in multiple formats (png, dot, eps, etc).
clc ; close all ; clear all ;

% As always, let us load a standard system and let us visualize it
S = benchmarkPaper() ;
S.view() ;

% We can add edges to the system
% Be carefull : the edge can create new nodes.
% Please check help addEdge for more informations.
% The label should be between 1 and S.getNMats()
% (Don't forget matlab works by-value : you *should* write S =
% S.addEdge(...), not just S.addEdge(...))
S = S.addEdge([4 5 2]) ; % Add the edge (i, j, s) = (4, 5, 2)
S.view() ;

% Let us add a 5th matrix
S = S.addMatrix(rand(2)) ; % The new matrix can be accessed with S.getMat(5)
% We can now use it when adding edges to our system.
% Let us add an edge with a multilabel
S = S.addEdge([5 4],[5,2,1]) ; % Edge corresponding to the product M1*M2*M5.
% Finally, we can view and export the .dot file
S.view() ;
S.export('graph_export',{'png','dot','eps'}) ;
% As you can see in the 'Current Folder' window, multiple files with the
% corresponding extensions has been created.

