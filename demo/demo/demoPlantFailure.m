%% Plant with Controller Failure Example
% This script is an example of an engineering problem that can be adressed
% thanks to the CJSR toolbox
clc ; close all ; clear all ;

% Open-loop plant
A = [0.94 0.56 ; 
     0.14 0.46] ;
% Input-to-state 
B = [0 ; 1] ;
% Controller gains
K = [-0.49 0.27] ;
% Our complete system will then be A + B*K

% We will study how the number of failures impact our stability
maxFails = 5 ;


% Given that, we have this kind of graph
%
%   (1)
%   \ v
%    [1] -(2)-> [2] -(2)-> [3] - ... -> [maxFail+1]
%     |          |          |                 |
%     |___(1)____v          v                 v
%     |____________(1)______|                 |
%     |____________________________(1)________|
%
%
% The [.] are the nodes while the (.) are the labels of the edges
% We have maxFail+1 nodes : one for each possible failure, and one for the
% success state
% We move forward from node i to node i+1 if there is a failure, while we
% move back from node i to node 1 if there is no failure.
% The constraint "at most maxFail failure" is encoded in the limited size of the
% graph

% Keeping that in mind, we can build the graph
% The label 1 is success : no failure of the controller ; the system and
% the plant work together
M1 = A+B*K ;
% The label 2 is a failule : no controller
M2 = A ;
matrices = {M1, M2} ;


failAmount = 0:maxFails ;
for i = 1:numel(failAmount)
    maxFail = failAmount(i) ;
    edges = [1 1 1] ;
    for j = 1:maxFail
        edges = [edges ; ...
                 j j+1 2 ;   % Failure
                 j+1 1 1 ] ; % Success
    end
    % Thanks to that, we build the system
    S = CSSystem(edges, matrices(1:min(i, 2))) ;
    % That we can visualize
    S.view() ;
    % Let us now check wheter this system is unstable or not with at most n
    % failure
    % To improve accuracy, let us lift it 3 times before
    ub(i) = (S.sosLift(2).MDependantLift(3,'past').cjsrQuadMultinorm())^(1/2) ;
    % If The upperbound on the JSR is clearly below 1 : our system will be stable
end

plot(failAmount, ub, 'marker', '.', 'markersize', 10) ;
hold on
plot(failAmount(ub < 1), ub(ub < 1), '.', 'markersize', 10) ;
plot(failAmount(ub >= 1), ub(ub >= 1), '*', 'markersize', 10) ;
