%% Illustrates the use of the cjsrCplxPolytopeMultinorm method
% on the example of [1, page 2277]
%
% This script takes a few minutes to terminates.
% During the execution, it plots the evolution of the complex polytope norm
% As we can see, it is very close to the one in [1].
% This example requires sedumi. If you want to use sdpt3, simply modify the
% 'cpoly.solver' option of cjsrSettings from 'sedumi' to 'sdpt3'
%
% References :
% [1] Guglielmi, Nicola, and Marino Zennaro. "An algorithm for finding extremal
%     polytope norms of matrix families." Linear Algebra and its Applications
%     428.10 (2008): 2265-2282.
clc ; close all ; clear all ;

%% Build the simple system
[S, cjsr, smp] = polytopes1() ;
S.view() ;

%% Define the options
opts = cjsrSettings('verbose',1, 'cpoly.plot', 1, ...              % Plot or not the evolution of the algo (will only plot the real parts in 2D)
                                 'cpoly.solver','sedumi', ...      % Define the solver ('sedumi' or 'sdpt3')
                                 'cpoly.maxiter',30, ...           % The max # of iterations
                                 'cpoly.checkNewCycle',true, ...   % If the algo should look for better SMP (spectrum maximising product - see below)
                                 'cpoly.maxPathsLength',8);        % The maximum length of the SMP (see below)

%% Define the candidate SMP                            
[P, bounds, smp] = S.cjsrCplxPolytopeMultinorm(smp, opts) ;

%% What is the cycle is not extremal ?
% Now what if we don't give the algorithm the right cycle ?
smp = [2 1] ;
[P, bounds, smp] = S.cjsrCplxPolytopeMultinorm(smp, opts) ;
% The algorithm eventually recover the right cycle and the right CJSR
bounds
smp

%% Visualize the multinorm
S.viewMultinorm('poly', P) ;