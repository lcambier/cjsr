%% Runs the complex polytope method on the example of [1]
%
% This script runs the complex polytope method on the example of [1].
% It takes several minutes.
% This example requires sedumi. If you want to use sdpt3, simply modify the
% 'cpoly.solver' option of cjsrSettings from 'sedumi' to 'sdpt3'
%
% Reference : 
% [1] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015). 
%     Stability of discrete-time switching systems with constrained switching 
%     sequences. arXiv preprint arXiv:1503.06984.
clc ; close all ; clear all ;

%% Load a simple system
S = benchmarkPaper() ; % Return a system with 4 nodes and 4 labels
S.view() ;

%% Define the options
opts = cjsrSettings('verbose',1, 'cpoly.plot', 1, ...            % Plot or not the evolution of the algo (will only plot the real parts in 2D)
                                 'cpoly.solver','sedumi', ...    % Define the solver ('sedumi' or 'sdpt3')
                                 'cpoly.maxiter',25, ...         % The max # of iterations
                                 'cpoly.checkNewCycle',true, ... % If the algo should look for better SMP (spectrum maximising product - see below)
                                 'cpoly.maxPathsLength',8);      % The maximum length of the SMP (see below)

%% Define the candidate SMP
% Here we need to give a candidate SMP. Basically, it is a path that should
% be close to reaching the joint spectral radius
% It should be given as a succession of **edges** index and it should be a
% cycle : The last and first edges should have the same first and second
% component (i.e. the last node of the last edge = the first node of the first edge).
smp = [8 5 1 5 2 4 8 8] ;
[P, bounds, smp] = S.cjsrCplxPolytopeMultinorm(smp, opts) ;
% We can see that the algorithm terminates with this cycle, which is then
% extremal.
bounds
smp

%% What if the cycle is not extremal ?
% Now what if we don't give the algorithm the right cycle ?
% It will look for better ones while the algorithm runs ...
smp = [8 5 1] ;
[P, bounds, smp] = S.cjsrCplxPolytopeMultinorm(smp, opts) ;
bounds
smp
% We can see that the obtained smp is equal to the one provided in the
% previous example (up to a cyclic permutation).

%% We can eventually plot the resulting complex polytopic norm using the 'P' 
% output of cjsrCplxPolytopeMultinorm
S.viewMultinorm('poly', P) ;