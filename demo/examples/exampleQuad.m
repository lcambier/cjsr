%% Runs the tests of [1]
% This script can be used to reproduce the experiments of [1], using
% different kinds of lifts and the quadratic norm approximation.
%
% It will take several minutes to terminates, and will output a plot at
% the end.
%
% Reference : 
% [1] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015). 
%     Stability of discrete-time switching systems with constrained switching 
%     sequences. arXiv preprint arXiv:1503.06984.
clc ; close all ; clear all ;

[Sys, cjsr] = benchmarkPaper() ;

% 1. A few params
% ------------------
% Using Yalmip modeling
% yalmipOpts = sdpsettings('verbose',0,'solver','sdpt3') ;
% opts = cjsrSettings('verbose',1,'cjsrQuadMultinorm.modeling','yalmip','cjsrQuadMultinorm.yalmip', yalmipOpts) ;
% ------------------
% Using Sedumi-matrix modeling
opts = cjsrSettings('verbose',1,'quad.modeling','sedumi','quad.solver','sedumi') ;
% ------------------

K = 5 ;
legs = {} ;

% 2. Visualization
Sys.view() ;

% 3. Compute some CJSR upper bounds
%% 3a. Vanilla bissection
ub = Sys.cjsrQuadMultinorm(opts) ;
fprintf('Bissection on S : %f\n', ub) ;

%% 3b. Product Lift
for t = 1:K
    St = Sys.TProductLift(t) ;
    ubt(t) = St.cjsrQuadMultinorm(opts) ;    
    fprintf('Bissection on S^%d : %f\n', t, ubt(t)) ;
end
figure ;
hold on ;
plot(ubt,'-r') ;
legs{length(legs)+1} = 'T product' ;

%% 3ci. Dependant Lift (path <=> past)
ubm(1) = ub ;
for m = 1:K-1
    Sm = Sys.MDependantLift(m, 'past') ;
    ubm(m+1) = Sm.cjsrQuadMultinorm(opts) ;
    fprintf('Bissection on S_%d : %f\n', m, ubm(m+1))
end
plot(ubm,'-b') ;
legs{length(legs)+1} = 'M dependant (path)' ;

%% 3d. SOS (2) + T product lift 
SysSos2 = Sys.sosLift(2) ;
for t = 1:K
    SysSos2lifted = SysSos2.TProductLift(t) ;
    ubt(t) = SysSos2lifted.cjsrQuadMultinorm(opts)^(1/2) ; % 1/2 for Sos Lift
    fprintf('Bissection on S[2]^%d : %f\n', t, ubt(t)) ;
end
plot(ubt,'-k') ;
legs{length(legs)+1} = 'SOS 2 - T product' ;

%% 3e. SOS (2) + Dependant Lift (path <=> past)
ubm(1) = ubt(1) ;
for m = 1:K-1
    SysSos2lifted = SysSos2.MDependantLift(m, 'past') ;
    ubm(m+1) = SysSos2lifted.cjsrQuadMultinorm(opts)^(1/2) ; % 1/2 for Sos Lift
    fprintf('Bissection on S[2]_%d (past) : %f\n', m, ubm(m+1)) ;
end
plot(ubm,'-g') ;
legs{length(legs)+1} = 'SOS 2 - M dependant (path)' ;

% Can be done as well, but it is very slow
% Uncomment the following lines to do the SOS-3 lift.
%
% %% 3f. SOS (3) + T product lift 
% SysSos3 = Sys.sosLift(3) ;
% for t = 1:K
%     SysSos3lifted = SysSos3.TProductLift(t) ;
%     ubt(t) = SysSos3lifted.cjsrQuadMultinorm(opts)^(1/3) ; 
%     fprintf('Bissection on S[3]^%d : %f\n', t, ubt(t)) ;
% end
% plot(ubt,'--k') ;
% legs{length(legs)+1} = 'SOS 3 - T product' ;
% 
% %% 3g. SOS (3) + Dependant Lift (path <-> past)
% ubm(1) = ubt(1) ;
% for m = 1:K-1
%     SysSos3lifted = SysSos3.MDependantLift(m, 'past') ;
%     ubm(m+1) = SysSos3lifted.cjsrQuadMultinorm(opts)^(1/3) ;
%     fprintf('Bissection on S[3]_%d (past) : %f\n', m, ubm(m+1)) ;
% end
% plot(ubm,'--g') ;
% legs{length(legs)+1} = 'SOS 3 - M dependant (path)' ;
% 
% plot([1 K], [cjsr, cjsr], '--k') ;
% legs{length(legs)+1} = 'CJSR' ;

xlabel('K') ;
ylabel('CJSR Upper Bound') ;
legend(legs) ;


