%% Getting started 4
% Simply run this script
% Make sure to have the full toolbox on the matlab path, as well as the
% SEDUMI toolbox.
clc ; close all ; clear all ;
disp('----------------------------------------------------------------------------------') ;
disp('Welcome to the CJSR Toolbox - Getting Started 4 (Generating unstable trajectories)') ;
disp('----------------------------------------------------------------------------------') ;
disp('The tutorial is based on the running example of')
disp('[1] Legat, B., Jungers, R. M., Parrilo, P. A. (2015).') ;
disp('    Generating unstable trajectories for Switched Systems')
disp('    via Dual Sum-Of-Squares techniques,')
disp('which continues section 4 of ')
disp('[2] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015).') ;
disp('    Stability of discrete-time switching systems with constrained switching sequences.')
disp('    arXiv preprint arXiv:1503.06984.') ;
disp('The system can be loaded using the following command:')
strToRun = 'S = benchmarkPaper()' ; runCmdPause ;
disp('-------------------------------------------------------------------') ;
disp('This time the objective is twofold.') ;
disp('A solution of the LMIs of cjsrQuadMultinorm provides an upper bound on the CJSR.') ;
disp('Using the guarantees we have on the quality of this upper bound,') ;
disp('we also have a lower bound.') ;
disp('However, this lower bound is not very good, so we seek to improve it.') ;
disp('Second, we would like to generate an unstable trajectory') ;
disp('and try to extract a candidate s.m.p. from this trajectory.') ;
disp('The spectral radius of this candidate will provide the') ;
disp('improved lower bound.') ;
disp('This technique is presented in [1].') ;
pauseCmd() ;
disp('The system has 9 edges:') ;
strToRun = 'S.getEdges()' ; runCmdPause() ;
disp('The first step of the method is to obtain a good CJSR approximation using')
disp('the cjsrQuadMultinorm method ([2], see also gettingStarted2.m).')
disp('A convex optimization problem is solved, and we exploit the optimal dual variables of this problem.')
strToRun = '[ub1, Qprimal2, Qdual2, ~] = S.cjsrQuadMultinorm();' ; runCmdPause() ;
disp('There is one dual variable per edge.') ;
disp('The dual variable associated with the first edge') ;
strToRun = 'Qdual2{1}' ; runCmdPause() ;
disp('The dual variables are matrices (they correspond to "pseudo-expectations"; see [1]).') ;
disp('The variables for the other edges are') ;
strToRun = 'Qdual2{2}' ; runCmdPause() ;
strToRun = 'Qdual2{3}' ; runCmdPause() ;
strToRun = 'Qdual2{4}' ; runCmdPause() ;
strToRun = 'Qdual2{5}' ; runCmdPause() ;
strToRun = 'Qdual2{6}' ; runCmdPause() ;
strToRun = 'Qdual2{7}' ; runCmdPause() ;
strToRun = 'Qdual2{8}' ; runCmdPause() ;
strToRun = 'Qdual2{9}' ; runCmdPause() ;
disp('Except for the edges 1, 5, 8, the dual variable are close to 0.') ;
disp('They are not exactly 0 because the dual variables are computed using an approximate value of the') ;
disp('always produce solutions in the relative interior of the cone of the corresponding convex set.') ;
disp('Note that we solve the SDP program by bisection on gamma, the upper bound on the CJSR') ;
disp('The dual variable are computed on the highest lower bound on gamma and') ;
disp('the primal variable are computed on the lowest upper bound.') ;
disp('This explains why the dual variables are not exactly 0.') ;
disp('To visualize where those edges are, we can use the "view" method of the CSSystem class:') ;
strToRun = 'S.view(cjsrSettings(''dot.edgeNumbering'',1));' ; runCmdPause() ;
disp('One might probably guess that the s.m.p., if it exists, only contains the edges 1, 5 and 8.') ;
disp('You will see that this does not appear to be true.') ;
disp('') ;
disp('We can compute a lower bound using the guarantee on upper bound.') ;
disp('We know that the CJSR is in the following interval:') ;
strToRun = '[lb, ub] = S.getbounds(ub1); [lb, ub]' ; runCmdPause() ;
disp('We now try the method to see if we can improve this lower bound') ;
strToRun = '[lb, smp, seq, ~] = S.cjsrBuildSequence(cjsrSettings(), Qdual2);' ; runCmdPause() ;
disp('We have considerably improved the lower bound.') ;
disp('We now know that the CJSR is in the interval') ;
strToRun = '[lb, ub]' ; runCmdPause() ;
disp('Can we do better ?') ;
disp('Let us increase the degree dramatically 2d=12.') ;
strToRun = 'S12 = S.sosLift(6); [ub6, Qprimal12, Qdual12, ~] = S12.cjsrQuadMultinorm();' ; runCmdPause() ;
disp('What are the dual variables now ?') ;
strToRun = 'Qdual12{1}' ; runCmdPause() ;
strToRun = 'Qdual12{2}' ; runCmdPause() ;
strToRun = 'Qdual12{3}' ; runCmdPause() ;
strToRun = 'Qdual12{4}' ; runCmdPause() ;
strToRun = 'Qdual12{5}' ; runCmdPause() ;
strToRun = 'Qdual12{6}' ; runCmdPause() ;
strToRun = 'Qdual12{7}' ; runCmdPause() ;
strToRun = 'Qdual12{8}' ; runCmdPause() ;
strToRun = 'Qdual12{9}' ; runCmdPause() ;
disp('We see that 1, 5 and 8 are still nonzero but now 2 and 4 are also nonzero.') ;
disp('Again using the guarantee on upper bound,') ;
disp('we know that the CJSR is in the following interval:') ;
strToRun = '[lb, ub] = S12.getbounds(ub6); [lb, ub]' ; runCmdPause() ;
disp('And using the method,') ;
strToRun = '[lb, smp, seq, ~] = S12.cjsrBuildSequence(cjsrSettings(), Qdual12);' ; runCmdPause() ;
disp('we know that it is in the following interval') ;
strToRun = '[lb, ub]' ; runCmdPause() ;
disp('As we have seen in gettingStarted3, we have found an s.m.p.') ;
disp('We see that while the dual variables were zero for the edges 2 and 4 for 2d = 2,') ;
disp('they are part of an s.m.p.') ;
disp('Actually, we can find this s.m.p. already using 2d = 10 but using an horizon of 2:') ;
strToRun = '[lb, smp, seq, ~] = S.sosLift(5).cjsrBuildSequence(cjsrSettings(''seq.l'', 2)); lb' ; runCmdPause() ;
disp('or even 2d = 2 but using an horizon of 3:') ;
strToRun = '[lb, smp, seq, ~] = S.cjsrBuildSequence(cjsrSettings(''seq.l'', 3), Qdual2);' ; runCmdPause() ;

disp('You can learn more about this function by typing')
disp('help CSSystem.cjsrBuildSequence') ;

disp('This demonstration is now terminated.') ;
disp('-------------------------------------------------------------------') ;
