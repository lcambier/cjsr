%% Getting started 2
% Simply run this script
% Make sure to have the full toolbox on the matlab path, as well as the 
% SEDUMI toolbox.
clc ; close all ; clear all ;
disp('----------------------------------------------------------------------') ;
disp('Welcome to the CSSystem Toolbox - Getting Started 2 (Quadratic multinorms)') ;
disp('----------------------------------------------------------------------') ;
disp('The tutorial is based on the example from')
disp('[1] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015).') ;
disp('    Stability of discrete-time switching systems with constrained switching sequences.')
disp('    arXiv preprint arXiv:1503.06984.') ;
disp('The system can be loaded using the following command:')
strToRun = 'S = benchmarkPaper()' ; runCmdPause ;

disp('-------------------------------------------------------------------') ;
disp('The question we ask is wheter this CSS') ;
disp('is uniformly assymptotically stable (and thus exponentially stable) or not.') ;
disp('This concept is related to the *(Constrained) Join Spectral Radius* (CJSR in short)')
disp('of the system, which is in general equal to its exponential growth rate.') ;
disp('(It generalizes the Joint Spectral Radius). For reference, see') 
disp('[2] X. Dai, �A Gel�fand-type spectral radius formula and');
disp('    stability of linear constrained switching systems,� Linear');
disp('    Algebra and its Applications, vol. 436, no. 5, pp. 1099�1113, 2012.');
disp('A system is stable, and exponentially stable, if and only if CJSR < 1.') ;
disp('Computing this CJSR is known to be hard. *The toolbox is devoted to this task.*') ;
pauseCmd() ;
disp('We now present a few algorithms aiming at estimating/approximating/computing the CJSR');
disp('Detailled references will be given later on.');
pauseCmd() ;
disp('For our system, the CJSR is around 0.975!') ;

disp('----- Quadratic Multinorms - Philippe et al. 2015 [1] -----') ;
disp('The first method (cjsrQuadMultinorm) computes an ellipsoidal approximation') ;
disp('of a set of norms defining the CJSR. It solves a set of LMIs within a bisection procedure.') ;
disp('This method will return an upperbound on the CJSR.') ;
disp('The following command will run the method with the default settings.') ;
disp('-- !!  Make sure you have SEDUMI installed and that it is on the matlab path !! ---') ;
disp('Note : while the method runs, several things will be displayed in the terminal') ;
disp('you can ignore them for now') ;
strToRun = 'ub = S.cjsrQuadMultinorm() ;' ; runCmdPause ;
strToRun = 'ub' ; runCmdPause ;
disp('As we can see, the upper bound is slightly above 1,') ;
disp('so we cannot guarantee stability so far...')
disp('To improve on the bound, we can use what we call lifts and transform the system.') ;
pauseCmd() ;
disp('Lifts are ways to build CSSs with equivalent stability properties,')
disp('who are bigger (in the size of the graph or matrices),')
disp('but from which we obtain provably better approximations.') ;
pauseCmd() ;
disp('We begin with the ''T-product'' lift. ') ;
disp('It is computed using the following:') ;
strToRun = 'Slifted = S.TProductLift(2)' ; runCmdPause ;
disp(' ')
disp('Slifted is a CSSystem object.');
disp('Every edge in Slifted corresponds to a path of length 2 in S.')
disp('It represents the dynamics x(t+2) = A(s(t+1))A(s(t))x(t).')
disp('The toolbox comes with tools to visualize systems.') ;
disp('It relies on the **GRAPHVIZ** package and the dot software.') ;
disp('The package is available (for free) here: http://www.graphviz.org') ;
disp('For more information you can check the demoViewExport.m script in the');
disp('demo/examples folder') ;
disp('Is graphviz already installed ? (If no, we will skip this step)') ;
pressedARandomButton = 0;
while pressedARandomButton < 5;
    str = input('Please enter yes or no : \n','s') ;
    if strcmp(str,'yes') || strcmp(str,'no')
        break;
    end
    pressedARandomButton = pressedARandomButton +1;
    if pressedARandomButton >= 5
        disp('We''ll take this as a no.');
        str = 'no';
    end
end
if strcmp(str,'yes')
    disp('At the first call of the view() or export() function,') ;
    disp('the cjsr toolbox asks for the location of GraphViz (if it cannot') ;
    disp('figure it out by itself...).') ;    
    strToRun = 'Slifted.view() ;' ; runCmdPause ;
    disp('We can see on this figure that on all edges, the labels are *multi-labels*,') ;
    disp('meaning they correspond to matrices products.') ;
end

pauseCmd() ;
disp('We can now call the cjsrQuadMultinorm method on this lifted system') ;
strToRun = 'ub = Slifted.cjsrQuadMultinorm();' ; runCmdPause ;
strToRun = 'ub' ; runCmdPause ;
disp('The result is already better.') ;
disp('By lifting the system one more time, we will get an upper bound which') ;
disp('is below 1, meaning our system is stable.') ;
strToRun = '[ub, Q] = S.TProductLift(3).cjsrQuadMultinorm();' ; runCmdPause ;
strToRun = 'ub' ; runCmdPause ;
disp('Now the upper-bound clearly is below 1: our system is stable!') ;
pauseCmd() ;

disp('We can plot the unit-ball of the resulting quadratic multinorm:') ;
strToRun = 'S.viewMultinorm(''quad'',Q);' ; runCmdPause ;

disp('The toolbox proposes other lifts.') ;
pauseCmd() ;
disp('The path-dependant and horizon-dependant lifts allow to use the tools developped in') ;
disp('[3] J.-W. Lee and G. E. Dullerud, �Uniform stabilization of');
disp('    discrete-time switched and markovian jump linear systems,�');
disp('    Automatica, 42(2), 205-218, 2006.');
disp('[4] R. Essick, J.-W. Lee, and G. E. Dullerud, �Control of linear');
disp('    switched systems with receding horizon modal information,�');
disp('    IEEE Transactions on Automatic Control, vol. 59, no. 9, pp.2340,2352, Sept. 2014.');
disp('A quadratic multinorm stability certificate obtained using a path-dependant lift')
disp('is known as a **path-dependant Lyapunov function**.')
disp('The lift allows to use them in an approximation setting.') ;
disp('An example of use is as follows, for the path-dependant lift with memory 1.')
strToRun = 'ub = S.MDependantLift(1,''path'').cjsrQuadMultinorm() ;' ; runCmdPause ;
strToRun = 'ub' ; runCmdPause ;
disp('See help CSSystem.MDependantLift for more information.')
pauseCmd() ;
disp('Other lifts are CSSystem.sosLift and CSSystem.arbitraryLift.')
disp('The first lifts the matrix set in and higher dimensionnal space,')
disp('so that a quadratic norm in that space is a sum-of-square polynomial');
disp('of a given degree in R^n. See e.g.:');
disp('[5] P. A. Parrilo and A. Jadbabaie, Approximation of the joint');
disp('    spectral radius using sum of squares, Linear Algebra and its');
disp('    Applications, vol. 428, no. 10, pp. 2385-2402, 2008.');
disp('The second allows to use classical JSR tools for CJSR estimations.');
disp('See e.g.:')
disp('[6] V. Kozyakin, �The Berger-Wang formula for the markovian');
disp('    joint spectral radius�, Linear Algebra and its Applications,');
disp('    vol. 448, pp. 315-328, 2014,');
disp('[7] Y. Wang, N. Roohi, G. E. Dullerud, and M. Viswanathan,');
disp('    Stability of linear autonomous systems under regular');
disp('    switching sequences, in Proceedings of the 53rd Conference');
disp('    on Decision and Control. IEEE, 2014, pp. 5445-5450.');
disp('See help CSSystem.sosLift and help CSSystem.arbitraryLift');
disp('for references and more information.') ;
pauseCmd() ;

disp('-------------------------------------------------------------------') ;
disp('Let us now explore the different options of the cjsrQuadMultinorm method.') ;
disp('To pass options to cjsrQuadMultinorm, we can pass an optional structure containing options') ;
disp('For instance, let us decrease the verbose level (i.e. the quantity of'); 
disp('information printed out in the terminal during the execution) and the tolerance')
disp('of the bissection algorithm used in cjsrQuadMultinorm.') ;
disp('To do so, we use the cjsrSettings function as follows:') ;
strToRun = 'opts = cjsrSettings(''verbose'',0,''quad.tol'',1e-4) ;' ; runCmdPause ;
strToRun = 'ub = S.cjsrQuadMultinorm(opts); ub' ; runCmdPause() ;
disp('More informations on the parameters can be found by typing either') ;
disp('help cjsrSettings') ;
disp('or') ;
disp('help CSSystem.cjsrQuadMultinorm') ;
pauseCmd() ;

disp('-------------------------------------------------------------------') ;
disp('This second introduction is now over.') ;
disp('-------------------------------------------------------------------') ;
disp('In the next tutorial, we present a method using a set of polytopic norms')
disp('to approximate the CJSR. Type gettingStarted3 and hit enter to run this') ;
disp('last tutorial.') ;