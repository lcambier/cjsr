%% Getting started
% Simply run this script
% Make sure to have the full toolbox on the matlab path
clc ; close all ; clear all ;
disp('-------------------------------------------------------------------') ;
disp('Welcome to the CSSystem Toolbox - Getting Started 1 (Basics)') ;
disp('-------------------------------------------------------------------') ;
disp('This toolbox has been developped for the stability analysis of') ;
disp('**Discrete-time Linear Switching Systems with Constrained Switching Sequences**.') ;
disp('In short, we call them Constrained Switching Systems (CSS).');
pauseCmd() ;
disp('In this first tutorial, we show how to build and work with CSS') ;
disp('through the **CSSystem Matlab(R) class**.') ;
disp('The next tutorials show how to apply the toolbox for stability analysis.') ;
disp('As the tutorial runs, commands are executed and variables stored to workspace.') ;
pauseCmd() ;

disp('--------------------- Using the CSSystem Class ---------------------') ;
disp('We build the example system presented in: .')
disp('[1] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015).') ;
disp('    Stability of discrete-time switching systems with constrained switching sequences.')
disp('    arXiv preprint arXiv:1503.06984.') ;

disp('We have a constrained switching system described by a set')
disp('of 4 matrices M1, M2, M3 and M4, along with a set of switching rules.') ;

%pauseCmd();
A = [0.94 0.56 ; 0.14 0.46] ;
B = [0 ; 1] ;
K = [-0.49 0.27] ;
M1 = A+B*K;
M2 = A+B*[0 K(2)];
M3 = A+B*[K(1) 0];
M4 = A;
strToRun = 'M1, M2, M3, M4' ; runCmdPause ;

disp('-------------------')
disp('The 4 matrices are stored in a cell array:') ;
strToRun = 'matrices = {M1, M2, M3, M4} ;' ; runCmd ;
disp('-------------------') ;
pauseCmd() ;


disp('The following graph (automaton) encodes the switching rules.');
disp('The main concept is that every edge corresponds to a time transition,');
disp('and every path corresponds to a valid switching sequence.')
disp('An edge in the graph is a triplet (i, j, s). ');
disp('The two first entries (i and j) are the source and destination nodes.'); 
disp('The last entry (s) corresponds to the mode (matrix) of the transition.') ;
imshow('gettingStarted.png') ;
pauseCmd() ;

disp('The "authorized" products of matrices are such that there exists') ;
disp('a path in the graph with matching labels.') ;
disp('For instance, M1*M1*M3 is authorized because there exists')
disp('at least one path with labels (3, 1, 1) in succession.');
disp('The product M2*M2 is not authorized, there are no path with labels (2,2).') ;
pauseCmd() ;

disp('This graph can be described by the following set of edges.') ;
disp('>> edges = [1 3 1 ;') ;
disp('            1 2 3 ;') ;
disp('            2 1 2 ;') ;
disp('            2 3 1 ;') ;
disp('            3 1 2 ;') ;
disp('            3 2 3 ;') ;
disp('            3 4 4 ;') ;
disp('            3 3 1 ;') ;
disp('            4 3 1 ; ]') ;
edges = [1 3 1 ;
    1 2 3 ;
    2 1 2 ;
    2 3 1 ;
    3 1 2 ;
    3 2 3 ;
    3 4 4 ;
    3 3 1 ;
    4 3 1 ] ;
pauseCmd() ;

disp('We can now encode our Constrained Switching System') ;
disp('using the CSSystem class:') ;
strToRun = 'S = CSSystem(edges, matrices)' ; runCmdPause ;
disp(' ')
disp('The object gives access to different operations (check help or doc CSSystem.)') ;
disp('The number of nodes is given by') ;
strToRun = 'S.getNNodes()' ; runCmdPause ;
disp('We can also access the first and fourth edge by typing') ;
strToRun = 'S.getEdges([1 4])' ; runCmdPause ;
disp('This command returns only the (i, j) pair for each edge') ;
disp('To access the labels on edges 1 and 4, we can use') ;
strToRun = 'S.getLabels([1 4])' ; runCmdPause ;
disp('The matrix at label 3 is accessed through') ;
strToRun = 'S.getMat(3)' ; runCmdPause ;
disp('The matrix at the third edge can be accessed through') ;
strToRun = 'S.getMatProdEdge(3)' ; runCmdPause ;
disp('The reason behind the name of this method is that labels on edges can have different lenghts.') ;
disp('For an edge (i,j,[1,2,4]), the method returns M4*M2*M1.') ;
disp('The help for this function is accessed by typing') ;
strToRun = 'help CSSystem.getMatProdEdge' ; runCmdPause ;


%disp('Many other commands can be run to accessed values from a CSSystem object') ;
disp('A complete list can be found in the documentation of CSSystem (type help CSSystem).') ;
pauseCmd() ;

disp('-------------------------------------------------------------------') ;
disp('This first introduction is now over.') ;
disp('---------- Spoilers for the gettingStarted2 tutorial --------------');
disp('In this toolbox, our focus is on computing the');
disp('**Constrained Joint Spectral Radius** of a CSS.');
disp('The notion, presented in: ')
disp('[2] X. Dai, A Gel''fand-type spectral radius formula and');
disp('    stability of linear constrained switching systems, Linear');
disp('    Algebra and its Applications, vol. 436, no. 5, pp. 1099-1113, 2012,');
disp('is tightly link to the stability of CSSs and is hard to compute.')
disp('The toolbox proposes methods for its approximation.');
disp('To see them in action, begin by typing gettingStarted2.') ;
