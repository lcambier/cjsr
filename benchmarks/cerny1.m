function [S_cerny, cjsr, smp] = cerny1(n, m)
% CERNY1 CSSsystem with cjsr = 1 and non-trivial invariant subspaces.
% 
%   [S, CJSR, SMP] = CERNY1() returns a system S with cjsr = CJSR, 
%   and CJSR is attained by the cycle described by the sequence of edges
%   in the array SMP. System on 2 nodes and matrices of dimension 2.
%
%   [S, CJSR, SMP] = CERNY1(N,M) returns a system S with cjsr = CJSR, 
%   and CJSR is attained by the cycle described by the sequence of edges
%   in the array SMP. Parameters N, M are scalars integers >= 2.
%
%   The graph of S is a cerny automaton ([1], [2]-Prop. 2.4) on M >= 2 nodes:
%            (1)       (1)       (1)           (1)  
%            \ v       \ v       \ v           \ v     
%   [1] -(2)->[2] -(2)->[3] -(2)->[4] ... -(2)->[m]
%    ^\ \     /^                                 /
%      \ \(1)/                                  /
%       \______________ (2) ___________________/
%   The two matrices in dimension >= 2 are
%   > M{1} = [zeros(n-1, 1), eye(n - 1); zeros(1, n)]; % cycles
%   > M{2} = [zeros(n-1, n); [1, zeros(1, n-1)]];      % forward
%   The smp is of length  N*(M-1)
%
%   References:
%   [1] M. V. Volkov. Synchronizing automata and the Cerny conjecture. In Language
%       and automata theory and applications, pages 11{27. Springer, 2008.
%   [2] M. Philippe and R. Jungers. A sufficient condition for the boundedness of
%       matrix product accepted by an automaton. In Proceedings of
%       Hybrid Systems: Computation and Control, 2015.

if nargin == 0
    n = 2; % dimension of the system
    m = 2; % number of nodes in the cerny automaton.
end
n = max(n,2);
m = max(m,2);
M = cell(2,1);

M{1} = [zeros(n-1, 1), eye(n - 1); zeros(1, n)]; % cycles!
M{2} = [zeros(n-1, n); [1, zeros(1, n-1)]]; % forward

edges = zeros(2*m, 3);

edges(1:2,:) = [1,2,1; 1 2 2];
smp = [1]; % cycle

for v = 2 : m
    edges(2*v-1 : 2*v, :) = [v, v, 1;...
                            v, mod(v, m)+1, 2];
    smp = [smp,(2*v-1)*ones(1, n-1), 2*v]; % cycles until x = [1,0,0,...]', then forward
end
smp(2) = [];
S_cerny = CSSystem(edges, M);
cjsr = 1; 

end

