function [S, cjsr, smp] = benchmarkPaper()
% BENCHMARKPAPER Returns a constrainted system
%
%   [S, CJSR, SMP] = benchmarkPaper() returns the example of system S described in
%   [1]. This system is a four nodes systems with the following edges
%   (i, j, sigma)
%    1  3  1
%    1  2  3
%    2  1  2
%    2  3  1
%    3  1  2
%    3  2  3
%    3  4  4
%    3  3  1
%    4  3  1
%   and the following matrices
%   M1 =
%      0.9400    0.5600
%     -0.3500    0.7300
%   M2 =
%      0.9400    0.5600
%      0.1400    0.7300
%   M3 = 
%      0.9400    0.5600
%     -0.3500    0.4600
%   M4 = 
%      0.9400    0.5600
%      0.1400    0.4600
%   CJSR is the value of the CJSR : 0.9748
%   The SMP is [8 5 1 5 2 4 8 8].
%
%   Reference: 
%   [1] Philippe, Matthew, et al. "Stability of discrete-time 
%       switching systems with constrained switching sequences." arXiv preprint 
%       arXiv:1503.06984 (2015).


A = [0.94 0.56 ; 0.14 0.46] ;
B = [0 ; 1] ;
K1 = [-0.49 0.27] ;
K2 = [0 K1(2)] ;
K3 = [K1(1) 0] ;
K4 = [0 0] ;
matrices = {A+B*K1 ; A+B*K2 ; A+B*K3 ; A+B*K4} ;
edges = [1 3 1 ;
         1 2 3 ;
         2 1 2 ;
         2 3 1 ;
         3 1 2 ;
         3 2 3 ;
         3 4 4 ;
         3 3 1 ;
         4 3 1 ] ;

S = CSSystem(edges, matrices) ;
smp = [8 5 1 5 2 4 8 8];
sr = abs(eigs(S.getMatProdEdge(smp), 1)) ;
cjsr = sr^(1/8) ;


end