function [S, cjsr, smp] = polytopes1()
% POLYTOPES1 Example from Guglielmi and Zennaro, presented in [1].
%
%    [S, CJSR, SMP] = POLYTOPES1() returns an arbitrary switching system 
%    (special case of CSS). All numerical data (matrices, value of cjsr CJSR and 
%    spectral maximizing products SMP) can be found in [1], with b = 0.78
%    (see [1]).
%
%    References :
%    [1] Guglielmi, Nicola, and Marino Zennaro. "An algorithm for finding extremal
%        polytope norms of matrix families." Linear Algebra and its Applications
%        428.10 (2008): 2265-2282.

% The two matrices
A = [1 1 ; 0 1] ;
b = 0.78 ;
B = b*[1 0 ; 1 1] ;  
matrices = {A, B} ;

S = CSSystem(matrices) ;
smp = [2 1 1 2 1] ;   
cjsr = 1.43204 ;

end

