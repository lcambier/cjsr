function [S, cjsr, smp] = scalar1()
% SCALAR1 Example from Bliman and Ferrari-Trecate, presented in [1].
%
%   [S, CJSR, SMP] = SCALAR1() returns a scalar constrained switching system S. 
%   whose cjsr CJSR is attained by the cycle described by the array SMP.
%
%   References:
%   [1] Bliman, P. A., & Ferrari-Trecate, G. (2003). Stability analysis of 
%       discrete-time switched systems through Lyapunov functions with 
%       nonminimal state. In Proceedings of IFAC Conference on the Analysis
%       and Design of Hybrid Systems (pp. 325-330).


edges = [1,2,2; ...
         2,1,1; ...
         2,3,3; ...
         3,4,2; ...
         4,1,1];
   
matrices = {1/10, 3, 1/2};

S = CSSystem( edges, matrices);

smp = [1,3,4,5];
cjsr = abs(eigs(S.getMatProdEdge(smp), 1));
cjsr = cjsr^(1/length(smp));

end

