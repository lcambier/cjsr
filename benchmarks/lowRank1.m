function [S, cjsr, smp] = lowRank1()
% LOWRANK1 CSSsystem with a node with a non-trivial invariant subspace.
% 
%   [S, CJSR, SMP] = LOWRANK1() returns a system S with cjsr = CJSR, 
%   and CJSR is attained by the cycle described by the sequence of edges
%   in the array SMP.
%
%   The benchmark is a modification of [1]-Section 4.
%   Adds a path of length 2 between failure node 4 and node 1.  
%   Both edges carry the same label, which is node associated to the matrix
%   [1,1;0,0];
%
%   Reference : 
%   [1] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015). 
%       Stability of discrete-time switching systems with constrained switching 
%       sequences. arXiv preprint arXiv:1503.06984.

S = benchmarkPaper();
[S, lab] = S.addMatrix([1,1;0,0]);
newNode = S.getNNodes + 1;
S = S.addEdge( [4, newNode, lab] );
S = S.addEdge( [ newNode, 1 , lab] );
smp = [5     1     7    10    11     2     4     8     8     8];
% smp found using cjsrCplxPolytopeMultinorm.
cjsr = abs(eigs(S.getMatProdEdge(smp), 1));
cjsr = cjsr^(1/length(smp));
% cjsr value verified using cjsrQuadMultinorm
end

