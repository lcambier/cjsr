function [S, cjsr, smp] = disconnected1()
% DISCONNECTED1 CSSsystem with two disconnected components
% 
%   [S, CJSR, SMP] = DISCONNECTED1() returns a system S whose cjsr is CJSR, 
%   and CJSR is attained by the cycle described by the sequence of edges
%   in the array SMP. This system is made of two disconnected components.
%
%   The benchmark is a modification of [1]-Section 4.
%   
%   Reference: 
%   [1] Philippe, M., Essick, R., Dullerud, G., & Jungers, R. M. (2015). 
%       Stability of discrete-time switching systems with constrained switching 
%       sequences. arXiv preprint arXiv:1503.06984.

[S, cjsr, smp] = benchmarkPaper();
mats = S.getMatrices() ;
edges = S.getEdges() ;
labels = S.getLabels() ;

edges2 = [edges ; edges+4] ;
labels2 = cell(numel(labels)*2,1) ;
labels2(1:numel(labels)) = labels ;
labels2(numel(labels)+1:2*numel(labels)) = labels ;

S = CSSystem(edges2, mats, labels2) ;



end

