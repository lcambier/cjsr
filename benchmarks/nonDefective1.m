function [S, cjsr, smp] = nonDefective1()
% NONDEFECTIVE1 An unbounded system with cjsr = 1.
%
%   [S, CJSR, SMP] = nonDefective1() returns a system S  with cjsr = CJSR, 
%   and CJSR is attained by the cycle described by the sequence of edges
%   in the array SMP.
%
%   Generates system with cjsr = 1 and irreducible set of matrices, but
%   having unbounded trajectories (w. polynomial growth).

matrices = {[1,1;0,1], [0,1;1,0]}; % The matrix set in itself is irreducible.
edges = [1,1,1;...
         1 2 2;...
         2 1 2]; % Minimul dwell-time of 2 steps on second matrix
S = CSSystem(edges, matrices) ; % System object.
cjsr = 1;
smp = 1;
end

