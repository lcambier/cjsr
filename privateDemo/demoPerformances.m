%% Illustrates the performances of both the bissection and the complex polytope method
% This script takes a few minutes to terminates
clc ; close all ; clear all ;

S = benchmarkPaper() ;
opts = cjsrSettings('cpoly.maxiter', 30, 'cpoly.plot', true, 'cpoly.checkNewCycle', true) ; 

% Poly
[~,~,~,infoP] = S.cjsrCplxPolytopeMultinorm([5 1], opts) ;
figure ;
poly = plot(infoP.time, infoP.rho, '-k') ; hold on ;

% T Product Lift
oldTime = 0 ;
for t = 1:5
    St = S.TProductLift(t) ;
    [ubT,~,infoT] = St.cjsrQuadMultinorm() ;
    tprod = plot(infoT.time+oldTime, infoT.ub, '-b') ;
    oldTime = oldTime + infoT.time(end) ;
end

% M Dependant Lift
oldTime = 0 ;
for m = 0:4
    Sm = S.MDependantLift(m,'past') ;
    [ubM,~,infoM] = Sm.cjsrQuadMultinorm() ;
    mdep = plot(infoM.time+oldTime, infoM.ub, '-r') ;
    oldTime = oldTime + infoM.time(end) ;    
end
legend([poly tprod mdep],{'CplxPolytope','T-Product','M-Dependant'}) ;