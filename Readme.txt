**********************************
 Welcome to the CSSystem Toolbox! 
**********************************
by L�opold Cambier, Matthew Philippe, Rapha�l Jungers
Universit� catholique de Louvain, BELGIUM.

Constrained Switching Systems (CSSystems) are discrete-time linear switching systems
where the transition matrix belongs to a finite set of matrices and where only some
matrix products are allowed.

The Constrained Joint Spectral Radius (CJSR) characterizes the maximal asymptotic rate 
of growth of a product of matrices that belongs to this set. It is known to be
very hard to compute.

This toolbox gives a few tools to approximate the CJSR, which allows to characterize
the (un)stability of a given CSSystem.

************************
INSTALLATION
************************

No installation is required regarding the toolbox' code.
You simply need to download and to add the *full* toolbox to the Matlab path.

[Required] For SDP optimization, one of the following solver is required:
- Sedumi (http://perso.uclouvain.be/raphael.jungers/sites/default/files/sedumi.zip or http://sedumi.ie.lehigh.edu)
- SDPT3  (http://www.math.nus.edu.sg/~mattohkc/sdpt3.html)
Sedumi is the default solver that will be chosen.
You will need to add at least one of these solvers to the Matlab path.

[Optional] If you want, the toolbox also allows to solve some of the problems using Yalmip.
This could be helpful in extending the functionalities of the toolbox.
To do so, you will need to download and install Yalmip (http://users.isy.liu.se/johanl/yalmip/)

[Optional] Finally, in order to visualize systems, it is useful to download
and install Graphviz. More informations at http://www.graphviz.org and in the demo/demo/demoGraphviz.m
file.
Once it is downloaded and installed, the toolbox will search for Graphviz at the
first run of the visualisation functions. If it can't find it, you will receive a 
message asking fot the location of Graphviz.

************************
GETTINGSTARTED
************************

The easiest way to learn how to use the toolbox if to follow the tutorial. To
do so, type
gettingStarted1
in the terminal.

************************
VERSION
************************

The current version is v1.0.

*************************
LICENSE & CREDENTIALS
*************************

See License.txt for the terms and conditions on the use and spread of this toolbox.

*************************
AUTHORS
*************************

v1.0 implemented by L�opold Cambier, Matthew Philippe and Rapha�l Jungers.

*************************
BUG REPORT AND CONTACT
*************************

Please send all bug reports or questions to 
matthew.philippe@uclouvain.be

